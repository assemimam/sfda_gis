//
//  FavouriteViewController.h
//  sfda_gis
//
//  Created by Assem Imam on 9/18/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"
#import "SWTableViewCell.h"
@interface FavouriteViewController : MasterViewController<UITableViewDataSource,UITableViewDelegate,SWTableViewCellDelegate>
{
    __weak IBOutlet UIView *NoDataView;
    __weak IBOutlet UILabel *NoDataLabel;
    __weak IBOutlet UITableView *FavouritesTableView;
    __weak IBOutlet UIButton *BackButton;
    __weak IBOutlet UILabel *TitleLabel;
}
- (IBAction)CloseAction:(UIButton *)sender;
- (IBAction)BackAction:(UIButton *)sender;
@end
