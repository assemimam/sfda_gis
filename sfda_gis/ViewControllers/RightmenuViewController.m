//
//  RightmenuViewController.m
//  SFDA_GIS
//
//  Created by Assem Imam on 8/5/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import  "RightmenuViewController.h"
#import  "LibrariesHeader.h"
#import  "MenuCell.h"
#import "OfficesViewController.h"
#import "SectorsViewController.h"
#import "AboutViewController.h"
#import "FavouriteViewController.h"
#define DEGREES_RADIANS(angle) ((angle) / 180.0 * M_PI)
@interface RightmenuViewController ()
{
    NSArray *SfdaOficesList;
    NSArray *SfdaSectorsList;
    NSArray *MoreList;
    NSArray *MenuHeadersList;
}
@end

@implementation RightmenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    @try {
//        UIScreen *screen = [UIScreen mainScreen];
//        self.view.bounds = CGRectMake(0, 0, screen.bounds.size.height, screen.bounds.size.width - 20);
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
           // self.view.transform = CGAffineTransformConcat(self.view.transform, CGAffineTransformMakeRotation(DEGREES_RADIANS(-180)));

        //self.view.transform= CGAffineTransformMakeRotation(1.5*M_PI);
//            self.view.frame =  CGRectMake(0, 0, 1024, 768);
        }
        SfdaOficesList=[[Language getObject]getArrayWithKey:@"SfdaOfficesMenuOptions"];
        SfdaSectorsList=[[Language getObject]getArrayWithKey:@"SfdaSectorsMenuOptions"];
        MoreList=[[Language getObject]getArrayWithKey:@"MoreMenuOptions"];
        MenuHeadersList=[[Language getObject]getArrayWithKey:@"MenuHeaderOptions"];
    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}
#pragma -mark uitableview datasource methods


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCell *cell;
    @try {
        cell=(MenuCell*)[tableView dequeueReusableCellWithIdentifier:@"cleMenu" forIndexPath:indexPath];
        NSString*OptionName=@"";
        NSString*Imagename=@"";
        switch (indexPath.section) {
            case 0:
            {
                OptionName = [[SfdaOficesList objectAtIndex:indexPath.row]objectForKey:@"option"];
                Imagename = [[SfdaOficesList objectAtIndex:indexPath.row]objectForKey:@"image"];
            }
                break;
            case 1:
            {
                OptionName = [[SfdaSectorsList objectAtIndex:indexPath.row]objectForKey:@"option"];
                Imagename = [[SfdaSectorsList objectAtIndex:indexPath.row]objectForKey:@"image"];
            }
                break;
            case 2:
            {
                OptionName = [[MoreList objectAtIndex:indexPath.row]objectForKey:@"option"];
                Imagename = [[MoreList objectAtIndex:indexPath.row]objectForKey:@"image"];
            }
                break;
        }
        cell.MenuOptionLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:cell.MenuOptionLabel.font.pointSize];
        cell.MenuOptionLabel.text = OptionName;
        cell.MenuOptionImageView.image = [UIImage imageNamed:Imagename];
        cell.MenuOptionLabel.text = [textConverter convertArabic:cell.MenuOptionLabel.text];
    }
    @catch (NSException *exception) {
        
    }
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger noOfRows=0;
    
    @try {
        switch (section) {
            case 0:
                noOfRows = [SfdaOficesList count];
                break;
            case 1:
                noOfRows = [SfdaSectorsList count];
                break;
            case 2:
                noOfRows = [MoreList count];
                break;
        }
    }
    @catch (NSException *exception) {
        
    }
    
    return noOfRows;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
#pragma -mark uitableview delegate methods
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 35;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView;
    @try {
        if(MenuHeadersList!=nil && [MenuHeadersList count] > 0){
            
            NSString *headerText =[textConverter convertArabic: [MenuHeadersList objectAtIndex:section]];
            float headerWidth = 320.0f;
            float padding = 10.0f;
            headerView = [[UIView alloc] initWithFrame:CGRectMake(300, 0.0f, headerWidth, 35)];
            headerView.autoresizesSubviews = YES;
            headerView.backgroundColor = [UIColor colorWithRed:242/255.0f  green:242/255.0f blue:242/255.0f alpha:1];
            
            // create the label centered in the container, then set the appropriate autoresize mask
            UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, headerWidth, 35)];
            headerLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
            headerLabel.textAlignment = NSTextAlignmentRight;
            headerLabel.text = headerText;
            headerLabel.textColor = [UIColor colorWithRed:176/255.0f  green:176/255.0f blue:176/255.0f alpha:1];
            headerLabel.backgroundColor = [UIColor clearColor];
            headerLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:12];
            CGSize textSize = [headerText sizeWithFont:headerLabel.font constrainedToSize:headerLabel.frame.size lineBreakMode:NSLineBreakByWordWrapping];
            headerLabel.frame = CGRectMake(headerWidth - (textSize.width + padding), headerLabel.frame.origin.y, textSize.width, headerLabel.frame.size.height);
            [headerView addSubview:headerLabel];
            headerLabel.text = [textConverter convertArabic:  headerLabel.text];
            
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
    
    return headerView;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        UIStoryboard *countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone"
                                                                    bundle:nil];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad"
                                                          bundle:nil];
        }
        
        if (indexPath.section==0) {
            OfficesViewController *vc_offices =[countryStoryboard  instantiateViewControllerWithIdentifier:@"OfficesViewController"];
            switch (indexPath.row) {
                case 0:
                {
                    
                    vc_offices.BranchType = BRANCH;
                }
                    break;
                    
                case 1:
                {
                    vc_offices.BranchType = LABORATORY;
                }
                    break;
                case 2:
                {
                    vc_offices.BranchType = OUTLET;
                }
                    break;
            }
            int viewControllerIndex= [CommonMethods GetViewController:vc_offices FromNavigationController:[SlideNavigationController sharedInstance]];
            NSMutableArray *viewControllersList = [[NSMutableArray alloc]init];
            if (viewControllerIndex!=-1) {
                for (UIViewController *item in [SlideNavigationController sharedInstance].viewControllers) {
                    [viewControllersList addObject:item];
                }
                [viewControllersList removeObjectAtIndex:viewControllerIndex];
                [SlideNavigationController sharedInstance].viewControllers = viewControllersList;
            }
            
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
                [[SlideNavigationController sharedInstance] pushViewController:vc_offices animated:YES];
            }
            else{
                vc_offices.modalPresentationStyle=UIModalPresentationFormSheet;
                vc_offices.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
                
                [[SlideNavigationController sharedInstance]righttMenuSelected:[[UIButton alloc]init]];
                [[SlideNavigationController sharedInstance]  presentViewController:vc_offices animated:YES completion:nil];
            }
            
        }
        
        if (indexPath.section==1) {
            
            SectorsViewController *vc_sectors =[countryStoryboard  instantiateViewControllerWithIdentifier:@"SectorsViewController"];
            
            switch (indexPath.row) {
                case 0:
                {
                    
                    vc_sectors.SectorType = FOODS_SECTOR;
                }
                    break;
                    
                case 1:
                {
                    vc_sectors.SectorType = DRUGS_SECTOR;
                }
                    break;
                case 2:
                {
                    vc_sectors.SectorType = MEDICAL_SECTOR;
                }
                    break;
            }
            int viewControllerIndex= [CommonMethods GetViewController:vc_sectors FromNavigationController:[SlideNavigationController sharedInstance]];
            NSMutableArray *viewControllersList = [[NSMutableArray alloc]init];
            if (viewControllerIndex!=-1) {
                for (UIViewController *item in [SlideNavigationController sharedInstance].viewControllers) {
                    [viewControllersList addObject:item];
                }
                [viewControllersList removeObjectAtIndex:viewControllerIndex];
                [SlideNavigationController sharedInstance].viewControllers= viewControllersList;
                
            }
            
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
                [[SlideNavigationController sharedInstance] pushViewController:vc_sectors animated:YES];
            }
            else{
                vc_sectors.modalPresentationStyle=UIModalPresentationFormSheet;
                vc_sectors.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
                [[SlideNavigationController sharedInstance]righttMenuSelected:[[UIButton alloc]init]];
                [[SlideNavigationController sharedInstance]  presentViewController:vc_sectors animated:YES completion:nil];
            }
            
        }
        if (indexPath.section==2) {
            
            switch (indexPath.row) {
                case 0:
                {
                    FavouriteViewController *vc_favourites =[countryStoryboard  instantiateViewControllerWithIdentifier:@"FavouriteViewController"];
                    int viewControllerIndex= [CommonMethods GetViewController:vc_favourites FromNavigationController:[SlideNavigationController sharedInstance]];
                    NSMutableArray *viewControllersList = [[NSMutableArray alloc]init];
                    if (viewControllerIndex!=-1) {
                        for (UIViewController *item in [SlideNavigationController sharedInstance].viewControllers) {
                            [viewControllersList addObject:item];
                        }
                        [viewControllersList removeObjectAtIndex:viewControllerIndex];
                        [SlideNavigationController sharedInstance].viewControllers= viewControllersList;
                    }
                    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
                        [[SlideNavigationController sharedInstance] pushViewController:vc_favourites animated:YES];
                    }
                    else{
                      //  vc_favourites.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
                       
                        
                        vc_favourites.modalPresentationStyle=UIModalPresentationFormSheet;
                        vc_favourites.modalTransitionStyle = UIModalTransitionStyleCoverVertical;


                        [[SlideNavigationController sharedInstance]righttMenuSelected:[[UIButton alloc]init]];
                        [[SlideNavigationController sharedInstance]  presentViewController:vc_favourites animated:YES completion:nil];
                        
                    }
                }
                    break;
                    
                case 1:
                {
                    AboutViewController *vc_about =[countryStoryboard  instantiateViewControllerWithIdentifier:@"AboutViewController"];
                    int viewControllerIndex= [CommonMethods GetViewController:vc_about FromNavigationController:[SlideNavigationController sharedInstance]];
                    NSMutableArray *viewControllersList = [[NSMutableArray alloc]init];
                    if (viewControllerIndex!=-1) {
                        for (UIViewController *item in [SlideNavigationController sharedInstance].viewControllers) {
                            [viewControllersList addObject:item];
                        }
                        [viewControllersList removeObjectAtIndex:viewControllerIndex];
                        [SlideNavigationController sharedInstance].viewControllers= viewControllersList;
                    }
                    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
                        [[SlideNavigationController sharedInstance] pushViewController:vc_about animated:YES];
                    }
                    else{
                        vc_about.modalPresentationStyle=UIModalPresentationFormSheet;
                        vc_about.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                        [[SlideNavigationController sharedInstance]righttMenuSelected:[[UIButton alloc]init]];
                        [[SlideNavigationController sharedInstance]  presentViewController:vc_about animated:YES completion:nil];
                    }
                }
                    break;
            }
        }
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
    }
    @catch (NSException *exception) {
        
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
//- (NSUInteger)supportedInterfaceOrientations
//{
//    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
//         return UIInterfaceOrientationMaskLandscapeLeft|UIInterfaceOrientationMaskLandscapeRight;
//    }
//    else{
//    return UIInterfaceOrientationMaskPortrait ;
//    }
//}


//-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
//{
//    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
//    {
//        return  UIInterfaceOrientationLandscapeLeft| UIInterfaceOrientationLandscapeRight;
//        
//    }
//    else
//    {
//        return UIInterfaceOrientationMaskPortrait;
//    }
//    return  UIInterfaceOrientationLandscapeRight;
//}
//
//
//-(BOOL)shouldAutorotate
//{
//    return YES;
//}
//
//- (NSUInteger)supportedInterfaceOrientations{
//    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
//    {
//        return UIInterfaceOrientationMaskLandscape;
//        
//    }
//    return 0;
//}
@end
