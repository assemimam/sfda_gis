//
//  SectorsViewController.m
//  SFDA_GIS
//
//  Created by assem on 9/4/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "SectorsViewController.h"
#import "TableHeaderView.h"
#import  "BranchCell.h"
#import "BranchDetailsMapViewController.h"

@interface SectorsViewController ()
{
    NSMutableArray *groupedBranchesList;
    NSUInteger _selectedIndex;
}
@end

@implementation SectorsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    TitleLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:TitleLabel.font.pointSize];
    NoResultLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:NoResultLabel.font.pointSize];
    BackButton.titleLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:BackButton.titleLabel.font.pointSize];
    BackButton.titleLabel.text =[textConverter convertArabic:  BackButton.titleLabel.text];
    NoResultLabel.text =[textConverter convertArabic:NoResultLabel.text];
 
    
    groupedBranchesList = [[NSMutableArray alloc]init];
    NSString *iconTitleImageName;
    id typeFilteredArray;
    switch (self.SectorType) {
        case FOODS_SECTOR:
        {
            NSString *filterCondition =@"HasFood == '1'";
           
            NSPredicate *predicate =[NSPredicate predicateWithFormat:filterCondition];
            typeFilteredArray = [BranchesList filteredArrayUsingPredicate:predicate];
            TitleLabel.text=@"قطاع الغذاء";
            TitleLabel.textColor = [UIColor colorWithRed:121/255.0f green:185/255.0f blue:48/255.0f alpha:1];
            iconTitleImageName =@"foodic";
        }
            break;
            
        case DRUGS_SECTOR:
        {
            NSString *filterCondition =@"HasDrug == '1'";
          
            NSPredicate *predicate =[NSPredicate predicateWithFormat:filterCondition];
            typeFilteredArray = [BranchesList filteredArrayUsingPredicate:predicate];
            TitleLabel.text=@"قطاع الدواء";
            TitleLabel.textColor = [UIColor colorWithRed:52/255.0f green:117/255.0f blue:159/255.0f alpha:1];
            iconTitleImageName =@"drugic";
        }
            break;
            
        case MEDICAL_SECTOR:
        {
            NSString *filterCondition =@"HasMedical == '1'";
           
            NSPredicate *predicate =[NSPredicate predicateWithFormat:filterCondition];
            typeFilteredArray = [BranchesList filteredArrayUsingPredicate:predicate];
            TitleLabel.text=@"قطاع الأجهزة الطبية";
            TitleLabel.textColor = [UIColor colorWithRed:205/255.0f green:80/255.0f blue:69/255.0f alpha:1];
            iconTitleImageName =@"medicalic";
        }
            break;
    }
    
       TitleLabel.text =[textConverter convertArabic:TitleLabel.text];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        [IconTitleButton setImage:[UIImage imageNamed:iconTitleImageName] forState:UIControlStateNormal];
        [IconTitleButton setImage:[UIImage imageNamed:iconTitleImageName] forState:UIControlStateHighlighted];
        [IconTitleButton setImage:[UIImage imageNamed:iconTitleImageName] forState:UIControlStateSelected];
    }
    if (typeFilteredArray ) {
        if ([typeFilteredArray count]>0) {
            NSPredicate *predicate ;
            NSMutableDictionary *outlets = [[NSMutableDictionary alloc]init];
            [outlets setObject:@"0" forKey:@"Id"];
            [outlets setObject:@"المنافذ" forKey:@"Name"];
            NSString *filterCondition =@"BranchTypeId == '1'";
          
            
            predicate=[NSPredicate predicateWithFormat:filterCondition];
            id outletsFilteredArray = [typeFilteredArray filteredArrayUsingPredicate:predicate];
            [outlets setObject:outletsFilteredArray forKey:@"Branches"];
            [groupedBranchesList addObject:outlets];
            
            NSMutableDictionary *laboratories = [[NSMutableDictionary alloc]init];
            [laboratories setObject:@"0" forKey:@"Id"];
            [laboratories setObject:@"المختبرات" forKey:@"Name"];
            filterCondition =@"BranchTypeId == '3'";
            predicate =[NSPredicate predicateWithFormat:filterCondition];
            id laboratoriesFilteredArray = [typeFilteredArray filteredArrayUsingPredicate:predicate];
            [laboratories setObject:laboratoriesFilteredArray forKey:@"Branches"];
            [groupedBranchesList addObject:laboratories];
            
            NSMutableDictionary *branches = [[NSMutableDictionary alloc]init];
            [branches setObject:@"0" forKey:@"Id"];
            [branches setObject:@"الفروع" forKey:@"Name"];
            filterCondition =@"BranchTypeId == '2'";
    
            predicate =[NSPredicate predicateWithFormat:filterCondition];
            id branchesFilteredArray = [typeFilteredArray filteredArrayUsingPredicate:predicate];
            [branches setObject:branchesFilteredArray forKey:@"Branches"];
            [groupedBranchesList addObject:branches];
            
            
       }
        else{
            [BranchesTableView setHidden:YES];
            [NoResultView setHidden:NO];
            [self.view bringSubviewToFront:NoResultView];
        }
    }
    else
    {
        [BranchesTableView setHidden:YES];
        [NoResultView setHidden:NO];
        [self.view bringSubviewToFront:NoResultView];
    }

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BranchCell *cell;
    @try {
        
        cell=(BranchCell*)[tableView dequeueReusableCellWithIdentifier:@"cleBranch" forIndexPath:indexPath];
        cell.BranchNameLabel.text =[textConverter convertArabic:[[[[groupedBranchesList objectAtIndex:indexPath.section]objectForKey:@"Branches"]objectAtIndex:indexPath.row]objectForKey:@"NameAr"]];
        cell.BranchNameLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:cell.BranchNameLabel.font.pointSize];
    }
    @catch (NSException *exception) {
        
    }
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger noOfRows=0;
    
    @try {
        noOfRows = [[[groupedBranchesList objectAtIndex:section]objectForKey:@"Branches"]count];
    }
    @catch (NSException *exception) {
        
    }
    
    return noOfRows;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
#pragma -mark uitableview delegate methods
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *Title= [textConverter convertArabic:[[[[[groupedBranchesList objectAtIndex:indexPath.section]objectForKey:@"Branches"]objectAtIndex:indexPath.row]objectForKey:@"NameAr"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    

    CGSize QuestionSize = [Title sizeWithFont:[UIFont fontWithName:@"GESSTwoBold-Bold" size:13] constrainedToSize:CGSizeMake(tableView.frame.size.width , 2000000000000.0f)];
    
    return  MAX( QuestionSize.height,50);
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    CGSize HeaderSize;
    @try {
        NSString *headerText =[NSNull null]!=[[groupedBranchesList objectAtIndex:section]objectForKey:@"Name"]?[textConverter convertArabic:[[groupedBranchesList objectAtIndex:section]objectForKey:@"Name"]]:@"";
        
        HeaderSize = [headerText sizeWithFont:[UIFont fontWithName:@"GESSTwoBold-Bold" size:13] constrainedToSize:CGSizeMake(tableView.frame.size.width , 2000000000000.0f)lineBreakMode:NSLineBreakByWordWrapping];
    }
    @catch (NSException *exception) {
        
    }
    
    return  MAX( HeaderSize.height,35);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    TableHeaderView *headerView;
    @try {
        if(groupedBranchesList!=nil && [groupedBranchesList count] > 0){
            NSString *headerText =[textConverter convertArabic: [[groupedBranchesList objectAtIndex:section]objectForKey:@"Name"]];
            float headerWidth =tableView.frame.size.width;
            headerView = (TableHeaderView*)[[[NSBundle mainBundle]loadNibNamed:@"TableHeaderView" owner:nil options:nil]objectAtIndex:0];
        
            headerView.autoresizesSubviews = YES;
            headerView.HeaderTitleLabel.text = headerText;
            headerView.HeaderTitleLabel.backgroundColor = [UIColor clearColor];
            headerView.HeaderTitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:headerView.HeaderTitleLabel.font.pointSize];
            headerView.RowsCountLabel.text = [NSString stringWithFormat:@"%i",[[[groupedBranchesList objectAtIndex:section]objectForKey:@"Branches"]count]];
            headerView.autoresizingMask= UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight ;
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
    headerView.superview.backgroundColor = [UIColor redColor];
    return headerView;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
               [self  dismissViewControllerAnimated:YES completion:^{
                UIStoryboard *countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone"
                                                                            bundle:nil];
                if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
                    countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad"
                                                                  bundle:nil];
                }
                
                BranchDetailsMapViewController *vc_branchDetails =(BranchDetailsMapViewController*)[countryStoryboard  instantiateViewControllerWithIdentifier:@"BranchDetailsMapViewController"];
                [BranchesTableView deselectRowAtIndexPath:indexPath animated:YES];
                id selectedBranch =[[[groupedBranchesList objectAtIndex:indexPath.section]objectForKey:@"Branches"]objectAtIndex:indexPath.row];
                vc_branchDetails.SelectedBranch = selectedBranch;
                [[SlideNavigationController sharedInstance]pushViewController:vc_branchDetails animated:YES];
                NSLog(@"selected branch %@",selectedBranch);
            }];
            
        }
    }
    @catch (NSException *exception) {
        
    }

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"DetailsSegue"])
    {
        BranchDetailsMapViewController *vc_branchDetails =(BranchDetailsMapViewController*)[segue destinationViewController];
        NSIndexPath *indexPath =  [BranchesTableView indexPathForSelectedRow];
        [BranchesTableView deselectRowAtIndexPath:indexPath animated:YES];
        id selectedBranch =[[[groupedBranchesList objectAtIndex:indexPath.section]objectForKey:@"Branches"]objectAtIndex:indexPath.row];
        vc_branchDetails.SelectedBranch = selectedBranch;
        vc_branchDetails.ComeFromSearchResults=NO;
        NSLog(@"selected branch %@",selectedBranch);
    }
    
    
}


- (IBAction)CloseAction:(UIButton *)sender {
    @try {
        [[SlideNavigationController  sharedInstance]dismissViewControllerAnimated:YES completion:nil];
    }
    @catch (NSException *exception) {
        
    }
}
@end
