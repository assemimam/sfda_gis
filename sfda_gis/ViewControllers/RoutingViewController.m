//
//  RoutingViewController.m
//  SFDA_GIS
//
//  Created by assem on 9/2/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "RoutingViewController.h"
#import "LibrariesHeader.h"
#import "OCMapViewSampleHelpAnnotation.h"
#import "CustomAnnotationView.h"
#import "BranchDetailsView.h"
#import "MDDirectionService.h"
#import "RoutingViewController.h"
#import "RouteSuggestionView.h"
#import "RoutingOptionPopoverView.h"
#import "DWBubbleMenuButton.h"

#define APPLEMAPS   0
#define GOOGLEMAPS  1

@interface RoutingViewController ()<SlideMenuDelegate ,RoutingOptionDelegate, UIAlertViewDelegate , UIPopoverControllerDelegate>
{
    CustomAnnotationView*SelectedAnnotation;
    YLActivityIndicatorView* LoadingIndicator;
    NSMutableArray *RouteMarkers;
    NSMutableArray *RouteMarkersStrings;
    MDDirectionService *routingService;
    NSMutableArray *RouteSuggestionsList;
    RouteSuggestionView *v_routeSuggestion;
    BOOL ReverseRouting;
    CLLocationCoordinate2D destinationCoordinates;
    CLLocationCoordinate2D currentLocationCoordinates;
    BOOL showRouteSuggestion;
    NSString *TotalDistance;
    UIAlertView *promptAlert;
}

@property (nonatomic, strong) UIPopoverController *userDataPopover;

@end

@implementation RoutingViewController

@synthesize SelectedBranch;
#pragma -mark slide delegate
-(void)didSlideMenuForOpenState:(BOOL)open_state{
    if (open_state) {
        [MapView setUserInteractionEnabled:NO];
    }
    else{
        [MapView setUserInteractionEnabled:YES];
    }
}

-(void)BackAction:(UIButton *)sender
{
    @try {
        [self.navigationController popViewControllerAnimated:YES];
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)GoToNavigatorAction:(UIButton *)sender
{
    @try {
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            RoutingOptionPopoverView *vc_routingOption = [[RoutingOptionPopoverView alloc] initWithNibName:@"RoutingOptionPopoverView" bundle:nil];
            
            vc_routingOption.delegate = self;
            self.userDataPopover = [[UIPopoverController alloc] initWithContentViewController:vc_routingOption];
            self.userDataPopover.delegate = self;
            
            self.userDataPopover.popoverContentSize = CGSizeMake(320.0, 150.0);
            [self.userDataPopover presentPopoverFromRect:[sender frame]
                                                  inView:self.view
                                permittedArrowDirections:UIPopoverArrowDirectionAny
                                                animated:YES];
            
        }
        else
        {

        }
        
    }
    @catch (NSException *exception) {
        
    }
    
    
    
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)ShowLoadingView
{
    @try {
        [LoadingView setHidden:NO];
        [LoadingIndicator startAnimating];
        [UIView animateWithDuration:0.5 animations:^{
            [LoadingView setFrame:CGRectMake(0, HeaderView.frame.size.height , LoadingView.frame.size.width,LoadingView.frame.size.height)];
        }];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    [SlideNavigationController sharedInstance].delegate=nil;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    @try {
        
        [CommonMethods FormatView:OpenAppleButton WithShadowRadius:0.5 CornerRadius:2 ShadowOpacity:0.5 BorderWidth:0 BorderColor:[UIColor blackColor] ShadowColor:[UIColor blackColor] andXPosition:0 ];
         [CommonMethods FormatView:OpenGoogleButton WithShadowRadius:0.5 CornerRadius:2 ShadowOpacity:0.5 BorderWidth:0 BorderColor:[UIColor blackColor] ShadowColor:[UIColor blackColor] andXPosition:0 ];
        [SlideNavigationController sharedInstance].delegate = self;
        ReverseRouting =NO;
        showRouteSuggestion=YES;
        RouteMarkers = [[NSMutableArray alloc]init];
        RouteMarkersStrings = [[NSMutableArray alloc]init];
        RouteSuggestionsList = [[NSMutableArray alloc]init];
        [[UIApplication sharedApplication]setStatusBarHidden:NO];
        
        LoadingIndicator = [[YLActivityIndicatorView alloc] initWithFrame:LoadingActivityControl.frame];
        LoadingIndicator.duration = .8f;
        LoadingIndicator.tag =10;
        [LoadingView addSubview:LoadingIndicator];
        
        [self performSelector:@selector(ShowLoadingView) withObject:nil afterDelay:0.2];
        
        v_routeSuggestion =(RouteSuggestionView*)[[[NSBundle mainBundle]loadNibNamed:@"RouteSuggestionView" owner:nil options:nil]objectAtIndex:0];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
            [v_routeSuggestion setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, v_routeSuggestion.frame.size.height)];
        }
        else{
            [v_routeSuggestion setFrame:CGRectMake((self.view.frame.size.width - v_routeSuggestion.frame.size.width)/2, self.view.frame.size.height, v_routeSuggestion.frame.size.width, v_routeSuggestion.frame.size.height)];
        }
        
        [self.view addSubview:v_routeSuggestion];
        [v_routeSuggestion.HideButton addTarget:self action:@selector(HideButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [v_routeSuggestion.ReverseDirectionsButton addTarget:self action:@selector(ReverseDirectionsButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        TitleLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:TitleLabel.font.pointSize];
        LoadingLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:LoadingLabel.font.pointSize];
        BackButton.titleLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:BackButton.titleLabel.font.pointSize];
        BackButton.titleLabel.text =[textConverter convertArabic:BackButton.titleLabel.text];
        //        TitleLabel.text =[textConverter convertArabic: [SelectedBranch objectForKey:@"NameAr"]];
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[[NSUserDefaults standardUserDefaults]objectForKey:@"lat" ] floatValue ] longitude:[[[NSUserDefaults standardUserDefaults]objectForKey:@"long"] floatValue ] zoom:6];
        // GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:21.5608816 longitude:39.135299 zoom:6];
        MapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, MapViewContainer.frame.size.width, MapViewContainer.frame.size.height)  camera:camera];
        
        MapView.delegate=self;
        MapView.myLocationEnabled = NO;
        MapView.settings.myLocationButton=NO;
        MapView.settings.indoorPicker=NO;
        MapView.settings.compassButton=NO;
        [MapViewContainer addSubview: MapView];
        
        destinationCoordinates.latitude = [[SelectedBranch valueForKey:@"Latitude"] doubleValue];
        destinationCoordinates.longitude = [[SelectedBranch valueForKey:@"Longitude"] doubleValue];
        
        
        
        currentLocationCoordinates.latitude =/*21.5608816;*/ [[[NSUserDefaults standardUserDefaults]objectForKey:@"lat" ] floatValue ];
        currentLocationCoordinates.longitude =/* 39.135299;*/[[[NSUserDefaults standardUserDefaults]objectForKey:@"long"] floatValue ];
        UIImage *iconImage ;
        
        
        GMSMarker *destinationMarker = [GMSMarker markerWithPosition:destinationCoordinates];
        destinationMarker.icon=iconImage;
        destinationMarker.map = MapView;
        
        GMSMarker *currentLocationMarker = [GMSMarker markerWithPosition:currentLocationCoordinates];
        currentLocationMarker.icon=[UIImage imageNamed:@"mylocationic"];
        currentLocationMarker.map = MapView;
        [RouteMarkers addObject:destinationMarker];
        [RouteMarkers addObject:currentLocationMarker];
        
        [self performSelector:@selector(initialLoading) withObject:nil afterDelay:1];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)PerformRouting
{
    @try {
        
        [RouteSuggestionsList removeAllObjects];
        [RouteMarkersStrings removeAllObjects];
        
        NSString *currentLocationString = [[NSString alloc] initWithFormat:@"%f,%f",
                                           currentLocationCoordinates.latitude,currentLocationCoordinates.longitude];
        NSString *destinationString = [[NSString alloc] initWithFormat:@"%f,%f",
                                       destinationCoordinates.latitude,destinationCoordinates.longitude];
        if (ReverseRouting) {
            [RouteMarkersStrings addObject:destinationString];
            [RouteMarkersStrings addObject:currentLocationString];
        }
        else{
            [RouteMarkersStrings addObject:currentLocationString];
            [RouteMarkersStrings addObject:destinationString];
        }
        
        if([RouteMarkers count]>1){
            NSString *sensor = @"false";
            NSArray *parameters = [NSArray arrayWithObjects:sensor, RouteMarkersStrings,
                                   nil];
            NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
            NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                              forKeys:keys];
            routingService=[[MDDirectionService alloc] init];
            SEL selector = @selector(addDirections:);
            [routingService setDirectionsQuery:query
                                  withSelector:selector
                                  withDelegate:self];
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)initialLoading
{
    @try {
        NavigatiorButton.userInteractionEnabled =  NO;
        OpenDropMenuButton.userInteractionEnabled = NO;
        [NSThread detachNewThreadSelector:@selector(PerformRouting) toTarget:self withObject:nil];
    }
    @catch (NSException *exception) {
        
    }
    
}
- (void)HideLoading
{
    NavigatiorButton.userInteractionEnabled =  YES;
    OpenDropMenuButton.userInteractionEnabled = YES;
    [LoadingIndicator stopAnimating];
    [LoadingView setFrame:CGRectMake(0, HeaderView.frame.size.height - LoadingView.frame.size.height, LoadingView.frame.size.width,LoadingView.frame.size.height)];
}

- (void)handleSuggestionView {
    
    [UIView animateWithDuration:0.3 animations:^{
        [self HideLoading];
        
    }completion:^(BOOL finished) {
        if (finished) {
            [UIView animateWithDuration:0.3 animations:^{
                [LoadingView setHidden:YES];
                if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
                    [MapView setFrame:CGRectMake(0, MapView.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height -v_routeSuggestion.frame.size.height-HeaderView.frame.size.height)];
                    [v_routeSuggestion setFrame:CGRectMake(0, self.view.frame.size.height -v_routeSuggestion.frame.size.height, self.view.frame.size.width, v_routeSuggestion.frame.size.height)];
                }
                else{
                    [v_routeSuggestion setFrame:CGRectMake((self.view.frame.size.width - v_routeSuggestion.frame.size.width)/2, self.view.frame.size.height -v_routeSuggestion.frame.size.height, v_routeSuggestion.frame.size.width, v_routeSuggestion.frame.size.height)];
                    
                }
                
                
            }];
        }
    }];
}

- (void)addDirections:(NSDictionary *)json{
    @try {
        
        if ([json objectForKey:@"routes"]) {
            if ([[json objectForKey:@"routes"] count]>0) {
                
                GMSMutablePath *path = [GMSMutablePath path];
                NSMutableArray *polyLinesArray = [[NSMutableArray alloc] init];
                int zero=0;
                
                TotalDistance = [[[[[[json objectForKey:@"routes"] objectAtIndex:zero] objectForKey:@"legs"] objectAtIndex:0] objectForKey:@"distance"] objectForKey:@"text"];
                
                NSLog(@"%@", TotalDistance);
                
                [v_routeSuggestion.TotalDistanceLabel setText:TotalDistance];
                
                
                NSArray *steps = [[[[[json objectForKey:@"routes"] objectAtIndex:zero] objectForKey:@"legs"] objectAtIndex:0] objectForKey:@"steps"];
                for (int i=0; i<[[[[[[json objectForKey:@"routes"] objectAtIndex:zero] objectForKey:@"legs"] objectAtIndex:0] objectForKey:@"steps"]count]; i++) {
                    
                    //adding suggestions
                    NSMutableDictionary *suggestion = [[NSMutableDictionary alloc]init];
                    [suggestion setObject:[[steps objectAtIndex:i]objectForKey:@"html_instructions"] forKey:@"suggestion"];
                    [suggestion setObject:[[[steps objectAtIndex:i]objectForKey:@"distance"]objectForKey:@"text"] forKey:@"distance"];
                    if ([[steps objectAtIndex:i]objectForKey:@"maneuver"]) {
                        [suggestion setObject:[[steps objectAtIndex:i]objectForKey:@"maneuver"] forKey:@"maneuver"];
                    }
                    [RouteSuggestionsList addObject:suggestion];
                    
                    NSString* encodedPoints =[[[steps objectAtIndex:i]objectForKey:@"polyline"]valueForKey:@"points"];
                    polyLinesArray = [routingService decodePolyLine:encodedPoints];
                    NSUInteger numberOfCC=[polyLinesArray count];
                    for (NSUInteger index = 0; index < numberOfCC; index++) {
                        CLLocation *location = [polyLinesArray objectAtIndex:index];
                        CLLocationCoordinate2D coordinate = location.coordinate;
                        [path addLatitude:coordinate.latitude longitude:coordinate.longitude];
                        if (index==0) {
                            //[self.coordinates addObject:location];
                        }
                    }
                }
                
                GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
                polyline.strokeWidth =4;
                polyline.strokeColor = [UIColor colorWithRed:230/255.0f green:83/255.0f blue:93/255.0f alpha:1];
                polyline.map = MapView;
                
                v_routeSuggestion.SuggestionList = RouteSuggestionsList;
                v_routeSuggestion.EndLocation= [textConverter convertArabic: [SelectedBranch objectForKey:@"NameAr"]];
                
                if (ReverseRouting) {
                    v_routeSuggestion.StartLocation=[[[[[json objectForKey:@"routes"] objectAtIndex:zero] objectForKey:@"legs"] objectAtIndex:0]objectForKey:@"end_address"];
                    [v_routeSuggestion RefreshData:YES];
                }
                else{
                    v_routeSuggestion.StartLocation=[[[[[json objectForKey:@"routes"] objectAtIndex:zero] objectForKey:@"legs"] objectAtIndex:0]objectForKey:@"start_address"];
                    [v_routeSuggestion RefreshData:NO];
                    
                }
                
                
                [self  performSelectorOnMainThread:@selector(handleSuggestionView) withObject:nil waitUntilDone:NO  ];
            }
            else{
                [UIView animateWithDuration:0.3 animations:^{
                    [self HideLoading];
                }];
                [self performSelectorOnMainThread:@selector(ShowNoRouteAlert) withObject:nil waitUntilDone:NO];
            }
        }
        [v_routeSuggestion.ReverseDirectionsButton setUserInteractionEnabled:YES];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)ShowNoRouteAlert
{
    [[[UIAlertView alloc]initWithTitle:nil message:@"تعذر الوصول الى طريق" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil, nil]show];
    
}
-(void)HideButtonAction:(UIButton*)sender
{
    @try {
        if (showRouteSuggestion) {
            [UIView animateWithDuration:0.3 animations:^{
                [LoadingView setHidden:YES];
                if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
                    [MapView setFrame:CGRectMake(0, MapView.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height -HeaderView.frame.size.height-v_routeSuggestion.HeaderView.frame.size.height)];
                    [v_routeSuggestion setFrame:CGRectMake(0, self.view.frame.size.height -v_routeSuggestion.HeaderView.frame.size.height, self.view.frame.size.width, v_routeSuggestion.frame.size.height)];
                }
                else{
                    
                    [v_routeSuggestion setFrame:CGRectMake((self.view.frame.size.width - v_routeSuggestion.frame.size.width)/2, self.view.frame.size.height -v_routeSuggestion.HeaderView.frame.size.height, v_routeSuggestion.frame.size.width, v_routeSuggestion.frame.size.height)];
                }
                
                v_routeSuggestion.HideArrowButton.transform=CGAffineTransformMakeRotation(M_PI);
                
            }];
            
            showRouteSuggestion=NO;
            
        }
        else{
            [UIView animateWithDuration:0.3 animations:^{
                [LoadingView setHidden:YES];
                if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
                    [MapView setFrame:CGRectMake(0, MapView.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height -v_routeSuggestion.frame.size.height-HeaderView.frame.size.height)];
                    [v_routeSuggestion setFrame:CGRectMake(0, self.view.frame.size.height -v_routeSuggestion.frame.size.height, self.view.frame.size.width, v_routeSuggestion.frame.size.height)];
                }
                else{
                    
                    [v_routeSuggestion setFrame:CGRectMake((self.view.frame.size.width - v_routeSuggestion.frame.size.width)/2, self.view.frame.size.height -v_routeSuggestion.frame.size.height, v_routeSuggestion.frame.size.width, v_routeSuggestion.frame.size.height)];
                }
                
                v_routeSuggestion.HideArrowButton.transform=CGAffineTransformMakeRotation(2*M_PI);
                
            }];
            
            showRouteSuggestion=YES;
            
        }
        
    }
    @catch (NSException *exception) {
        
    }
}

-(void)ReverseDirectionsButtonAction:(UIButton*)sender
{
    @try {
        [v_routeSuggestion.ReverseDirectionsButton setUserInteractionEnabled:NO];
        ReverseRouting =!ReverseRouting;
        [self  ShowLoadingView];
        [NSThread detachNewThreadSelector:@selector(PerformRouting) toTarget:self withObject:nil];
        
    }
    @catch (NSException *exception) {
        
    }
}




#pragma mark - Routing Option delegate

-(void)routingOptionSeletedIndex:(NSUInteger)index
{
    @try {
        
        float destinationLat = destinationCoordinates.latitude;
        float destinationLong = destinationCoordinates.longitude;
        if (index == APPLEMAPS)
        {
            
            NSString* url = [NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=%f,%f&daddr=%f,%f&directionsmode=driving",currentLocationCoordinates.longitude,currentLocationCoordinates.latitude,destinationLong,destinationLat];
            
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
            
        }
        else if (index == GOOGLEMAPS)
            
        {
            
            NSURL *url =  [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f&directionsmode=driving"
                                                ,currentLocationCoordinates.latitude,currentLocationCoordinates.longitude,destinationLat, destinationLong]];
            BOOL isAppInstalled=[[UIApplication sharedApplication] canOpenURL:url];
            if(isAppInstalled)
            {
                
                [[UIApplication sharedApplication] openURL:url];
                
            }else {
                
                
                promptAlert = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"برجاء تحميل تطبيق خرائط جوجل"
                                                        delegate:self
                                               cancelButtonTitle:@"تحميل"
                                               otherButtonTitles:@"إلغاء", nil];
                [promptAlert show];
            }
            
        }
    }
    @catch (NSException *exception) {
        
    }
    
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        NSString *stringURL = @"https://itunes.apple.com/en/app/google-maps/id585027354?mt=8";
        NSURL *url = [NSURL URLWithString:stringURL];
        [[UIApplication sharedApplication] openURL:url];
        
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)animateButtons
{
    [UIView animateWithDuration:0.2 animations:^
    {
        [ButtonsContainerView setFrame:CGRectMake(ButtonsContainerView.frame.origin.x, ButtonsContainerView.frame.origin.y, ButtonsContainerView.frame.size.width, 80)];
        
    }];
    
}

- (IBAction)AnimateDropMenu:(id)sender {
    showRouteSuggestion =YES;
    [self HideButtonAction:nil];
    [UIView animateWithDuration:0.2 animations:^{
        [self.view bringSubviewToFront:ModalView];
        [self.view bringSubviewToFront:ButtonsContainerView];
        [ModalView setBackgroundColor:[UIColor lightGrayColor]];
        

        
    }];
    [self performSelector:@selector(animateButtons) withObject:nil afterDelay:0.1];
}

-(IBAction)dismissPopUpMenuAction:(id)sender
{
    
    [UIView animateWithDuration:0.3 animations:^
     {
          [ButtonsContainerView setFrame:CGRectMake(ButtonsContainerView.frame.origin.x, ButtonsContainerView.frame.origin.y, ButtonsContainerView.frame.size.width, 0)];
         [self.view sendSubviewToBack:ModalView];
         [self.view sendSubviewToBack:ButtonsContainerView];
         
     }];
   
    
}


-(IBAction)openAppleMapButtonAction:(id)sender
{
    @try {
        
        float destinationLat = destinationCoordinates.latitude;
        float destinationLong = destinationCoordinates.longitude;
        
        NSString* url = [NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=%f,%f&daddr=%f,%f&directionsmode=driving",currentLocationCoordinates.longitude,currentLocationCoordinates.latitude,destinationLong,destinationLat];
        
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
    }
    @catch (NSException *exception) {
        
    }

    
}

-(IBAction)openGoogleMapButtonAction:(id)sender
{
    @try {
        
        float destinationLat = destinationCoordinates.latitude;
        float destinationLong = destinationCoordinates.longitude;
        NSURL *url =  [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f&directionsmode=driving"
                                            ,currentLocationCoordinates.latitude,currentLocationCoordinates.longitude,destinationLat, destinationLong]];
        BOOL isAppInstalled=[[UIApplication sharedApplication] canOpenURL:url];
        if(isAppInstalled)
        {
            
            [[UIApplication sharedApplication] openURL:url];
            
        }else {
            
            
            promptAlert = [[UIAlertView alloc] initWithTitle:@""
                                                     message:@"برجاء تحميل تطبيق خرائط جوجل"
                                                    delegate:self
                                           cancelButtonTitle:@"تحميل"
                                           otherButtonTitles:@"إلغاء", nil];
            [promptAlert show];
        }

    }
    @catch (NSException *exception) {
        
    }
    


}
@end
