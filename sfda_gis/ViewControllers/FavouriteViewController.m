//
//  FavouriteViewController.m
//  sfda_gis
//
//  Created by Assem Imam on 9/18/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "FavouriteViewController.h"
#import "LibrariesHeader.h"
#import "BranchDetailsMapViewController.h"
#import "FavouriteCell.h"

@interface FavouriteViewController ()
{
    NSUInteger _selectedIndex;
    NSMutableArray *favouriteList;
}
@end

@implementation FavouriteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)GetFavouriteBranches
{
    @try {
        
        [[Cashing getObject] setDatabase:@"SFDA_GIS"];
        
        DB_Field  *fldID = [[DB_Field alloc]init];
        fldID.FieldName=@"id";
        fldID.FieldAlias=@"Id";
        
        DB_Field  *fldNameAr = [[DB_Field alloc]init];
        fldNameAr.FieldName=@"NameAr";
        
        DB_Field  *fldNameEn = [[DB_Field alloc]init];
        fldNameEn.FieldName=@"NameEn";
        
        DB_Field  *fldManagerId = [[DB_Field alloc]init];
        fldManagerId.FieldName=@"ManagerId";
        
        DB_Field  *fldManagerName = [[DB_Field alloc]init];
        fldManagerName.FieldName=@"ManagerName";
        
        DB_Field  *fldSectionId = [[DB_Field alloc]init];
        fldSectionId.FieldName=@"SectionId";
        
        DB_Field  *fldSectionName = [[DB_Field alloc]init];
        fldSectionName.FieldName=@"SectionName";
        
        DB_Field  *fldMainBranchId = [[DB_Field alloc]init];
        fldMainBranchId.FieldName=@"MainBranchId";
        
        DB_Field  *fldMainBranchName = [[DB_Field alloc]init];
        fldMainBranchName.FieldName=@"MainBranchName";
        
        DB_Field  *fldBranchTypeId = [[DB_Field alloc]init];
        fldBranchTypeId.FieldName=@"BranchTypeId";
        
        DB_Field  *fldBranchTypeName = [[DB_Field alloc]init];
        fldBranchTypeName.FieldName=@"BranchTypeName";
        
        DB_Field  *fldLatitude = [[DB_Field alloc]init];
        fldLatitude.FieldName=@"Latitude";
        
        DB_Field  *fldLongitude= [[DB_Field alloc]init];
        fldLongitude.FieldName=@"Longitude";
        
        DB_Field  *fldEmail = [[DB_Field alloc]init];
        fldEmail.FieldName=@"Email";
        
        DB_Field  *fldAddress = [[DB_Field alloc]init];
        fldAddress.FieldName=@"Address";
        
 
        
        id result = [[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldID,fldNameAr,fldNameEn,fldSectionId,fldMainBranchId,fldBranchTypeId,fldLatitude,fldLongitude,fldEmail,fldAddress, nil] Tables:[NSArray arrayWithObject:@"favourite_branches"] Where:nil FromIndex:nil ToIndex:nil OrderByField:@"RecentDate" AscendingOrder:NO];
        
        return  result;
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    @try {
        [super viewWillAppear:animated];
        favouriteList = [self GetFavouriteBranches];
        [FavouritesTableView reloadData];
        if ([favouriteList count]==0) {
            [FavouritesTableView setHidden:YES];
            [NoDataView setHidden:NO];
            
            [self.view bringSubviewToFront:NoDataView];
        }

    }
    @catch (NSException *exception) {
        
    }
   
}
- (void)viewDidLoad
{
    [super viewDidLoad];
  
    TitleLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:TitleLabel.font.pointSize];
    NoDataLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:NoDataLabel.font.pointSize];
    BackButton.titleLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:BackButton.titleLabel.font.pointSize];
    BackButton.titleLabel.text =[textConverter convertArabic:BackButton.titleLabel.text];
    NoDataLabel.text =[textConverter convertArabic:NoDataLabel.text];
    TitleLabel.text =[textConverter convertArabic:TitleLabel.text];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SWTableViewDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    @try {

        if ([favouriteList count]==0) {
            [FavouritesTableView setHidden:YES];
            [NoDataView setHidden:NO];
            [self.view bringSubviewToFront:NoDataView];
        }
        else{
            NSIndexPath *cellIndexPath = [FavouritesTableView indexPathForCell:cell];
            [[Cashing getObject] setDatabase:@"SFDA_GIS"];
            [[Cashing getObject]DeleteDataFromTable:@"favourite_branches" WhereCondition:[NSString stringWithFormat:@"id=%i",[[[favouriteList objectAtIndex:cellIndexPath.row]objectForKey:@"Id"]intValue]]];
            [favouriteList removeObjectAtIndex:cellIndexPath.row];
            [FavouritesTableView deleteRowsAtIndexPaths:@[cellIndexPath] withRowAnimation:UITableViewRowAnimationLeft];

        }
    }
    @catch (NSException *exception) {
        
    }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    // allow just one cell's utility button to be open at once
    return YES;
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    switch (state) {
        case 1:
            // set to NO to disable all left utility buttons appearing
            return YES;
            break;
        case 2:
            // set to NO to disable all right utility buttons appearing
            return YES;
            break;
        default:
            break;
    }
    
    return YES;
}



#pragma -mark uit  ableview datasource methods

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FavouriteCell *cell;
    @try {
        cell=(FavouriteCell*)[tableView dequeueReusableCellWithIdentifier:@"cleFavourite" forIndexPath:indexPath];
        cell.BranchNameLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:cell.BranchNameLabel.font.pointSize];
        cell.BranchNameLabel.text =[textConverter convertArabic:[[favouriteList objectAtIndex:indexPath.row]objectForKey:@"NameAr"]];
      
        if ([[[favouriteList objectAtIndex:indexPath.row]objectForKey:@"BranchTypeId"]intValue] == [BRANCHES intValue]) {
             cell.BranchImageView.image=[UIImage imageNamed:@"filterstoreic.png"];
        }
        else if ([[[favouriteList objectAtIndex:indexPath.row]objectForKey:@"BranchTypeId"]intValue] == [OUTLETES intValue])
        {
             cell.BranchImageView.image=[UIImage imageNamed:@"filterbranchic.png"];
        }
        else if ([[[favouriteList objectAtIndex:indexPath.row]objectForKey:@"BranchTypeId"]intValue] == [LABORATORIES intValue])
        {
            cell.BranchImageView.image=[UIImage imageNamed:@"filterlabic.png"];
        }
        cell.rightUtilityButtons = [self rightButtons];
         cell.delegate = self;
    }
    @catch (NSException *exception) {
        
    }
    return cell;
}
- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                title:@"حذف"];
    
    return rightUtilityButtons;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger noOfRows=0;
    
    @try {
        noOfRows = [favouriteList count];
    }
    @catch (NSException *exception) {
        
    }
    
    return noOfRows;
}
#pragma -mark uitableview delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        _selectedIndex = indexPath.row;
        UIStoryboard *countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone"
                                                                    bundle:nil];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad"
                                                          bundle:nil];
        }
        
        BranchDetailsMapViewController *vc_branchDetails =(BranchDetailsMapViewController*)[countryStoryboard  instantiateViewControllerWithIdentifier:@"BranchDetailsMapViewController"];
        NSIndexPath *indexPath =  [FavouritesTableView indexPathForSelectedRow];
        [FavouritesTableView deselectRowAtIndexPath:indexPath animated:YES];
        id selectedBranch =[favouriteList objectAtIndex:indexPath.row];
        vc_branchDetails.SelectedBranch = selectedBranch;
        NSLog(@"selected branch %@",selectedBranch);
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            [self  dismissViewControllerAnimated:YES completion:^{
                [[SlideNavigationController sharedInstance] pushViewController:vc_branchDetails animated:YES ] ;
            }];
        }
        else{
            [[SlideNavigationController sharedInstance] pushViewController:vc_branchDetails animated:YES ] ;
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize QuestionSize;
    @try {
        NSString *Title=[textConverter convertArabic: [[[favouriteList objectAtIndex:indexPath.row] objectForKey:@"NameAr"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        QuestionSize = [Title sizeWithFont:[UIFont fontWithName:@"GESSTwoBold-Bold" size:13] constrainedToSize:CGSizeMake(tableView.frame.size.width , 2000000000000.0f)];
    }
    @catch (NSException *exception) {
        
    }
  
    return  MAX( QuestionSize.height,50);
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    @try {
        if ([[segue identifier] isEqualToString:@"DetailsSegue"])
        {
            BranchDetailsMapViewController *vc_branchDetails =(BranchDetailsMapViewController*)[segue destinationViewController];
            NSIndexPath *indexPath =  [FavouritesTableView indexPathForSelectedRow];
            [FavouritesTableView deselectRowAtIndexPath:indexPath animated:YES];
            id selectedBranch =[favouriteList objectAtIndex:indexPath.row];
            vc_branchDetails.SelectedBranch = selectedBranch;
            NSLog(@"selected branch %@",selectedBranch);
        }
    }
    @catch (NSException *exception) {
        
    }
   
}


- (IBAction)CloseAction:(UIButton *)sender {
    @try {
        [[SlideNavigationController  sharedInstance]dismissViewControllerAnimated:YES completion:nil];
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)BackAction:(UIButton *)sender {
    @try {
        [[SlideNavigationController sharedInstance]popToRootViewControllerAnimated:YES];
    }
    @catch (NSException *exception) {
        
    }
}
@end
