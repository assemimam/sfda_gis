//
//  SearchResultsViewController.m
//  sfda_gis
//
//  Created by assem on 9/16/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "SearchResultsViewController.h"
#import "TableHeaderView.h"
#import  "BranchCell.h"
#import "BranchDetailsMapViewController.h"

@interface SearchResultsViewController ()
{
    NSMutableArray *groupedBranchesList;
    NSUInteger _selectedIndex;
}
@end

@implementation SearchResultsViewController
@synthesize SearchedText,delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     TitleLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:TitleLabel.font.pointSize];
     NoResultLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:NoResultLabel.font.pointSize];
     BackButton.titleLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:BackButton.titleLabel.font.pointSize];
     BackButton.titleLabel.text =[textConverter convertArabic:BackButton.titleLabel.text];
     NoResultLabel.text =[textConverter convertArabic:NoResultLabel.text];
     TitleLabel.text = [NSString stringWithFormat:@"نتائج البحث عن (%@)",SearchedText];
     groupedBranchesList = [[NSMutableArray alloc]init];
    
    id typeFilteredArray;
    
         NSString *filterCondition =[NSString stringWithFormat:@" ( MainBranchName CONTAINS[cd] '%@' )|| ( NameAr CONTAINS[cd] '%@' ) || ( NameEn CONTAINS[cd] '%@' ) ", SearchedText,SearchedText,SearchedText];
    
            NSPredicate *predicate =[NSPredicate predicateWithFormat:filterCondition];
            typeFilteredArray = [BranchesList filteredArrayUsingPredicate:predicate];
            
            if ([typeFilteredArray count]>0) {
                NSPredicate *predicate ;
                NSMutableDictionary *outlets = [[NSMutableDictionary alloc]init];
                [outlets setObject:@"0" forKey:@"Id"];
                [outlets setObject:@"المنافذ" forKey:@"Name"];

                predicate=[NSPredicate predicateWithFormat:@"BranchTypeId == '1' "];
                
                
                id outletsFilteredArray = [typeFilteredArray filteredArrayUsingPredicate:predicate];
                [outlets setObject:outletsFilteredArray forKey:@"Branches"];
                [groupedBranchesList addObject:outlets];
                
                NSMutableDictionary *laboratories = [[NSMutableDictionary alloc]init];
                [laboratories setObject:@"0" forKey:@"Id"];
                [laboratories setObject:@"المختبرات" forKey:@"Name"];
           
                predicate =[NSPredicate predicateWithFormat:@"BranchTypeId == '3' "];
            
               
                id laboratoriesFilteredArray = [typeFilteredArray filteredArrayUsingPredicate:predicate];
                [laboratories setObject:laboratoriesFilteredArray forKey:@"Branches"];
                [groupedBranchesList addObject:laboratories];
                
                NSMutableDictionary *branches = [[NSMutableDictionary alloc]init];
                [branches setObject:@"0" forKey:@"Id"];
                [branches setObject:@"الفروع" forKey:@"Name"];
               
                predicate =[NSPredicate predicateWithFormat:@"BranchTypeId == '2' "];
                
               
                
                id branchesFilteredArray = [typeFilteredArray filteredArrayUsingPredicate:predicate];
                [branches setObject:branchesFilteredArray forKey:@"Branches"];
                [groupedBranchesList addObject:branches];

    }
   else{
                [BranchesTableView setHidden:YES];
                [NoResultView setHidden:NO];
                [self.view bringSubviewToFront:NoResultView];
    }

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BranchCell *cell;
    @try {
        
        cell=(BranchCell*)[tableView dequeueReusableCellWithIdentifier:@"cleBranch" forIndexPath:indexPath];
         cell.BranchNameLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:cell.BranchNameLabel.font.pointSize];
        cell.BranchNameLabel.text =[textConverter convertArabic:[[[[groupedBranchesList objectAtIndex:indexPath.section]objectForKey:@"Branches"]objectAtIndex:indexPath.row]objectForKey:@"NameAr"]];
       
    }
    @catch (NSException *exception) {
        
    }
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger noOfRows=0;
    
    @try {
        noOfRows = [[[groupedBranchesList objectAtIndex:section]objectForKey:@"Branches"]count];
    }
    @catch (NSException *exception) {
        
    }
    
    return noOfRows;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
#pragma -mark uitableview delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
    
         if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
             UIStoryboard *countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad"
                                                                         bundle:nil];
             
             BranchDetailsMapViewController *vc_branchDetails =(BranchDetailsMapViewController*)[countryStoryboard  instantiateViewControllerWithIdentifier:@"BranchDetailsMapViewController"];
             
             [BranchesTableView deselectRowAtIndexPath:indexPath animated:YES];
             id selectedBranch =[BranchesList objectAtIndex:indexPath.row];
             vc_branchDetails.SelectedBranch = selectedBranch;
             NSLog(@"selected branch %@",selectedBranch);
             
             [[SlideNavigationController sharedInstance] pushViewController:vc_branchDetails animated:YES ] ;
             
          }
    }
    @catch (NSException *exception) {
        
    }
   
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *Title= [textConverter convertArabic:[[[[[groupedBranchesList objectAtIndex:indexPath.section]objectForKey:@"Branches"]objectAtIndex:indexPath.row]objectForKey:@"NameAr"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    
    CGSize QuestionSize = [Title sizeWithFont:[UIFont fontWithName:@"GESSTwoBold-Bold" size:13] constrainedToSize:CGSizeMake(tableView.frame.size.width , 2000000000000.0f)lineBreakMode:NSLineBreakByWordWrapping];
    
    return  MAX( QuestionSize.height,50);
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 35;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    TableHeaderView *headerView;
    @try {
        if(groupedBranchesList!=nil && [groupedBranchesList count] > 0){
            NSString *headerText =[textConverter convertArabic: [[groupedBranchesList objectAtIndex:section]objectForKey:@"Name"]];
            float headerWidth =tableView.frame.size.width;
            headerView = (TableHeaderView*)[[[NSBundle mainBundle]loadNibNamed:@"TableHeaderView" owner:nil options:nil]objectAtIndex:0];
            headerView.frame = CGRectMake(0, 00, headerWidth, 35);
            headerView.autoresizesSubviews = YES;
            headerView.HeaderTitleLabel.text = headerText;
            headerView.HeaderTitleLabel.backgroundColor = [UIColor clearColor];
            headerView.HeaderTitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:headerView.HeaderTitleLabel.font.pointSize];
            headerView.RowsCountLabel.text = [NSString stringWithFormat:@"%i",[[[groupedBranchesList objectAtIndex:section]objectForKey:@"Branches"]count]];
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
    headerView.superview.backgroundColor = [UIColor redColor];
    return headerView;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"DetailsSegue"])
    {
        BranchDetailsMapViewController *vc_branchDetails =(BranchDetailsMapViewController*)[segue destinationViewController];
        NSIndexPath *indexPath =  [BranchesTableView indexPathForSelectedRow];
        [BranchesTableView deselectRowAtIndexPath:indexPath animated:YES];
        id selectedBranch =[[[groupedBranchesList objectAtIndex:indexPath.section]objectForKey:@"Branches"]objectAtIndex:indexPath.row];
        vc_branchDetails.SelectedBranch = selectedBranch;
        NSLog(@"selected branch %@",selectedBranch);
    }
}

- (IBAction)BackButtonAction:(UIButton *)sender {
    @try {
        if (self.delegate) {
            [self.delegate didClickBackButton];
            [[SlideNavigationController sharedInstance]popViewControllerAnimated:YES];
        }
    }
    @catch (NSException *exception) {
        
    }
    
}
@end
