//
//  BranchDetailsMapViewController.m
//  SFDA_GIS
//
//  Created by Assem Imam on 8/27/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "BranchDetailsMapViewController.h"
#import "LibrariesHeader.h"
#import "OCMapViewSampleHelpAnnotation.h"
#import "CustomAnnotationView.h"
#import "BranchDetailsView.h"
#import "RoutingViewController.h"
#import "SearchView.h"
#import "SearchResultsViewController.h"

@interface BranchDetailsMapViewController ()<SearchDelegate,SlideMenuDelegate,SearchResultsDelegate>
{
    SearchView *v_Search;
    CustomAnnotationView*SelectedAnnotation;
    BranchDetailsView *v_branchDetails;
    BOOL showDetailsView;
}
@end

@implementation BranchDetailsMapViewController
@synthesize SelectedBranch,ComeFromSearchResults,delegate;
#pragma -mark slide delegate
-(void)didSlideMenuForOpenState:(BOOL)open_state{
    if (open_state) {
        [MapView setUserInteractionEnabled:NO];
        [self CancelSearchAction:nil];
        [self HideDetailsView];

    }
    else{
        [MapView setUserInteractionEnabled:YES];
    }
}

#pragma -mark SearchResults delegate
-(void)didClickBackButton
{
    @try {
        [self SearchAction:[[UIButton alloc]init]];
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark search delegate methods
-(void)didSelectNearByItem:(id)item
{
    @try {
        [SearchTextField resignFirstResponder];
        [SearchHeaderView setHidden:YES];
        [self.view sendSubviewToBack:SearchHeaderView];
        [v_Search setHidden:YES];
        [self.view sendSubviewToBack:v_Search];
       
        UIStoryboard *countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone"
                                                                    bundle:nil];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad"
                                                          bundle:nil];
        }
        self.SelectedBranch = item;
        [self PerformInitialLoading];

        NSLog(@"selected branch %@",item);
    }
    @catch (NSException *exception) {
        
    }
}
-(void)didSelectRecentItem:(id)item
{
    @try {
        [SearchTextField resignFirstResponder];
        [SearchHeaderView setHidden:YES];
        [self.view sendSubviewToBack:SearchHeaderView];
        [v_Search setHidden:YES];
        [self.view sendSubviewToBack:v_Search];
        UIStoryboard *countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone"
                                                                    bundle:nil];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad"
                                                          bundle:nil];
        }

        self.SelectedBranch = item;
        [self PerformInitialLoading];
        
        NSLog(@"selected branch %@",item);
    }
    @catch (NSException *exception) {
        
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)ZoomToBranchLocation
{
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([[SelectedBranch objectForKey:@"Latitude"] floatValue ], [[SelectedBranch objectForKey:@"Longitude"] floatValue ]);
    MKCoordinateSpan span = MKCoordinateSpanMake(0.5, 0.5);
    MKCoordinateRegion region = {coord, span};
    [MapView setRegion:region animated:YES];

    [self DisplayBranchDetails:nil];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [SlideNavigationController sharedInstance].delegate = nil;
}
-(void)viewWillAppear:(BOOL)animated
{
    [SlideNavigationController sharedInstance].menuDelegate = self;
}
-(void)DismissKebord
{
    [SearchTextField resignFirstResponder];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    UIBarButtonItem *finishItem = [[UIBarButtonItem alloc]initWithTitle:@"انهاء" style:UIBarButtonItemStylePlain target:self action:@selector(DismissKebord)];
    toolbar.items =[NSArray arrayWithObject:finishItem];
    SearchTextField.inputAccessoryView = toolbar;

    
    showDetailsView=NO;
    v_Search =(SearchView*)[[[NSBundle mainBundle]loadNibNamed:@"SearchView" owner:nil options:nil]objectAtIndex:0];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        [v_Search setFrame:CGRectMake(0, SearchHeaderView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - SearchHeaderView.frame.size.height )];
    }
    else{
        [v_Search setFrame:CGRectMake(SearchHeaderView.frame.origin.x, SearchHeaderView.frame.size.height-10, SearchHeaderView.frame.size.width, self.view.frame.size.height - SearchHeaderView.frame.size.height )];
    }

    [self.view addSubview:v_Search];
    v_Search.currentLocation =[[CLLocation alloc]initWithLatitude:[[[NSUserDefaults standardUserDefaults]objectForKey:@"lat" ] floatValue ] longitude:[[[NSUserDefaults standardUserDefaults]objectForKey:@"long"] floatValue ]];
    [v_Search setHidden:YES];
    v_Search.delegate=self;
    [v_Search reloadData];
    [self.view sendSubviewToBack:v_Search];
    [self.view bringSubviewToFront:SearchHeaderView];
   
    UITapGestureRecognizer *branchRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(HideDetailsView)];
    branchRecognizer.numberOfTapsRequired=1;
    [MapView addGestureRecognizer:branchRecognizer];
    
    v_branchDetails =(BranchDetailsView*)[[[NSBundle mainBundle]loadNibNamed:@"BranchDetailsView" owner:nil options:nil]objectAtIndex:0];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
         [v_branchDetails setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, v_branchDetails.frame.size.height)];
    }
    else{
         [v_branchDetails setFrame:CGRectMake((self.view.frame.size.width - v_branchDetails.frame.size.width)/2, self.view.frame.size.height,v_branchDetails.frame.size.width, v_branchDetails.frame.size.height)];
    }
   
     v_branchDetails.ContainerViewController=self;
    [self.view addSubview:v_branchDetails];
    [v_branchDetails.ArrowButton addTarget:self action:@selector(DetailMenuButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [v_branchDetails.ShareButton addTarget:self action:@selector(SaveButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [v_branchDetails.DirectionsButton addTarget:self action:@selector(DirectionButtonAction:) forControlEvents:UIControlEventTouchUpInside];
  
    MapView.delegate = self;
    CancelSearchButton.titleLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size: CancelSearchButton.titleLabel.font.pointSize];
 
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    
    TitleLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:TitleLabel.font.pointSize];
    BackButton.titleLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:BackButton.titleLabel.font.pointSize];
    TitleLabel.text = [textConverter convertArabic:TitleLabel.text];
    BackButton.titleLabel.text = [textConverter convertArabic: BackButton.titleLabel.text];
    [self PerformInitialLoading];
}
-(void)PerformInitialLoading
{
    @try {
        TitleLabel.text = [SelectedBranch objectForKey:@"NameAr"];
        if ([[SelectedBranch objectForKey:@"BranchTypeId"]intValue]==[OUTLETES intValue]) {
            TitleLabel.textColor = [UIColor colorWithRed:88/255.0f green:93/255.0f blue:147/255.0f alpha:1];
        }
        if ([[SelectedBranch objectForKey:@"BranchTypeId"]intValue]==[BRANCHES intValue]) {
            TitleLabel.textColor = [UIColor colorWithRed:178/255.0f green:91/255.0f blue:109/255.0f alpha:1];
        }
        if ([[SelectedBranch objectForKey:@"BranchTypeId"]intValue]==[LABORATORIES intValue]) {
            TitleLabel.textColor = [UIColor colorWithRed:58/255.0f green:169/255.0f blue:157/255.0f alpha:1];
        }
        
        for (id anotation in MapView.annotations) {
            [MapView removeAnnotation:anotation];
        }
        
        CLLocationCoordinate2D location;
        location.latitude = [[SelectedBranch valueForKey:@"Latitude"] doubleValue];
        location.longitude = [[SelectedBranch valueForKey:@"Longitude"] doubleValue];
        
        OCMapViewSampleHelpAnnotation *annotation = [[OCMapViewSampleHelpAnnotation alloc] initWithCoordinate:location];
        annotation.DeatailsInformation = SelectedBranch;
        if ([[SelectedBranch valueForKey:@"BranchTypeId"]intValue] ==1) {
            annotation.groupTag=OUTLETES;
        }
        else if ([[SelectedBranch valueForKey:@"BranchTypeId"]intValue] ==2){
            annotation.groupTag = BRANCHES;
        }
        else if ([[SelectedBranch valueForKey:@"BranchTypeId"]intValue] ==3){
            annotation.groupTag = LABORATORIES;
        }
        
        [MapView addAnnotation:annotation];
        
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([[SelectedBranch objectForKey:@"Latitude"] floatValue ], [[SelectedBranch objectForKey:@"Longitude"] floatValue ]);
        MKCoordinateSpan span = MKCoordinateSpanMake(90, 90);
        MKCoordinateRegion region = {coord, span};
        [MapView setRegion:region ];
        
        
        [self  performSelector:@selector(ZoomToBranchLocation) withObject:nil afterDelay:1.5];
    }
    @catch (NSException *exception) {
        
    }
   
}
- (IBAction)SendButtonAction:(UIButton *)sender {
    @try {
        
    }
    @catch (NSException *exception) {
        
    }
   
}

- (void)HideDetailsView {
    if (showDetailsView==NO) {
        return;
    }
    
    [[SelectedAnnotation viewWithTag:10000]removeFromSuperview];
    [self ShowPulseIndicator];

    [UIView animateWithDuration:0.3 animations:^{
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
            [MapView setFrame:CGRectMake(0, HeaderView.frame.size.height, MapView.frame.size.width, self.view.frame.size.height -HeaderView.frame.size.height )];
             [v_branchDetails setFrame:CGRectMake(0, self.view.frame.size.height , self.view.frame.size.width, v_branchDetails.frame.size.height)];
        }
        else{
             [v_branchDetails setFrame:CGRectMake((self.view.frame.size.width-v_branchDetails.frame.size.width)/2, self.view.frame.size.height , v_branchDetails.frame.size.width, v_branchDetails.frame.size.height)];
        }
       
    }];
}

- (void)ShowPulseIndicator {
    if (SelectedAnnotation!=nil) {
        for (id anotation in MapView.annotations) {
            if ([[MapView viewForAnnotation:anotation] isKindOfClass:[CustomAnnotationView class]]) {
                [[[MapView viewForAnnotation:anotation] viewWithTag:10000]removeFromSuperview];
            }
        }
    }
    UIImageView *pulseRingImg = [[UIImageView alloc] initWithFrame:SelectedAnnotation.bounds];
    if ([[SelectedBranch objectForKey:@"BranchTypeId"]intValue]==[OUTLETES intValue]) {
        pulseRingImg.image = [UIImage imageNamed:@"branchcircle"];
    }
    if ([[SelectedBranch objectForKey:@"BranchTypeId"]intValue ]==[BRANCHES intValue]) {
        pulseRingImg.image = [UIImage imageNamed:@"storescircle"];
    }
    if ([[SelectedBranch objectForKey:@"BranchTypeId"] intValue ]==[LABORATORIES intValue]) {
        pulseRingImg.image = [UIImage imageNamed:@"labcircle"];
    }
    
    
    pulseRingImg.userInteractionEnabled = NO;
    CABasicAnimation *theAnimation;
    theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.scale.xy"];
    theAnimation.duration=1.0;
    theAnimation.repeatCount=HUGE_VALF;
    theAnimation.autoreverses=NO;
    pulseRingImg.alpha=0;
    theAnimation.fromValue=[NSNumber numberWithFloat:0.0];
    theAnimation.toValue=[NSNumber numberWithFloat:1.5];
    pulseRingImg.alpha = 1;
    pulseRingImg.tag=10000;
    [pulseRingImg.layer addAnimation:theAnimation forKey:@"pulse"];
    pulseRingImg.userInteractionEnabled = NO;
    
    [SelectedAnnotation insertSubview:pulseRingImg atIndex:0];
}

- (IBAction)DetailMenuButtonAction:(UIButton *)sender {
    @try {
        showDetailsView =NO;
        for (id anotation in MapView.annotations) {
            if ([[MapView viewForAnnotation:anotation] isKindOfClass:[CustomAnnotationView class]]) {
                [[[MapView viewForAnnotation:anotation] viewWithTag:10000]removeFromSuperview];
            }
        }
        

        [self ShowPulseIndicator];

        [UIView animateWithDuration:0.3 animations:^{
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
                [v_branchDetails setFrame:CGRectMake((self.view.frame.size.width- v_branchDetails.frame.size.width)/2, self.view.frame.size.height ,v_branchDetails.frame.size.width, v_branchDetails.frame.size.height)];
            }
            else{
                [MapView setFrame:CGRectMake(0, HeaderView.frame.size.height, MapView.frame.size.width, self.view.frame.size.height -HeaderView.frame.size.height )];
                [v_branchDetails setFrame:CGRectMake(0, self.view.frame.size.height , self.view.frame.size.width, v_branchDetails.frame.size.height)];
            }
        }];
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)DirectionButtonAction:(UIButton *)sender {
    @try {
        NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
        if ( [defaults objectForKey:@"lat"]==nil) {
            UIAlertView *gpsAlert = [[UIAlertView alloc]initWithTitle:@"" message:@"(GPS) برجاء تفعيل خدمة تحديد المواقع " delegate:self cancelButtonTitle:@"الغاء" otherButtonTitles:nil, nil];
            [gpsAlert show];
            return;
        }
        if (!InternetConnection) {
            UIAlertView *gpsAlert = [[UIAlertView alloc]initWithTitle:@"" message:@"برجاء الاتصال بالانترنت" delegate:self cancelButtonTitle:@"الغاء" otherButtonTitles:nil, nil];
            [gpsAlert show];
            return;
        }
        UIStoryboard *countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone"
                                                                    bundle:nil];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad"
                                                          bundle:nil];
        }
        RoutingViewController *vc_routing =[countryStoryboard  instantiateViewControllerWithIdentifier:@"RoutingViewController"];
        vc_routing.SelectedBranch = self.SelectedBranch;
        [self.navigationController pushViewController:vc_routing animated:YES];
       
    }
    @catch (NSException *exception) {
        
    }
  
}

- (IBAction)SaveButtonAction:(UIButton *)sender {
}
-(void)DisplayBranchDetails:(UITapGestureRecognizer*)recognizer
{
    @try {
        showDetailsView=YES;
        [self ShowPulseIndicator];
    
        v_branchDetails.BranchID =[SelectedBranch objectForKey:@"Id"];
        [v_branchDetails ReloadBranchData];
        NSLog(@"details : %@", SelectedAnnotation.DeatailsInformation);
        [UIView animateWithDuration:0.3 animations:^{
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
                [v_branchDetails setFrame:CGRectMake(0, self.view.frame.size.height -v_branchDetails.frame.size.height, self.view.frame.size.width, v_branchDetails.frame.size.height)];
                [MapView setFrame:CGRectMake(0, MapView.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height -v_branchDetails.frame.size.height-HeaderView.frame.size.height)];
            }
            else{
                if (recognizer.view) {
                    CLLocationCoordinate2D location = [MapView convertPoint:CGPointMake(MapView.center.x,recognizer.view.frame.origin.y +200) toCoordinateFromView:MapView];
                    [MapView  setCenterCoordinate:location animated:YES];
                }

                [v_branchDetails setFrame:CGRectMake((self.view.frame.size.width - v_branchDetails.frame.size.width)/2, self.view.frame.size.height -v_branchDetails.frame.size.height, v_branchDetails.frame.size.width, v_branchDetails.frame.size.height)];
 
            }
        }];
       
        
    }
    @catch (NSException *exception) {
        
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - map delegate

- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    CustomAnnotationView *annotationView;
    @try {
        
        OCMapViewSampleHelpAnnotation *singleAnnotation = (OCMapViewSampleHelpAnnotation *)annotation;
        annotationView = (CustomAnnotationView *)[aMapView dequeueReusableAnnotationViewWithIdentifier:@"singleAnnotationView"];
        annotationView.tag =SINGLE_ANNOTATION_TAG;
        if (!annotationView) {
            annotationView = [[CustomAnnotationView alloc] initWithAnnotation:singleAnnotation reuseIdentifier:@"singleAnnotationView"];
            annotationView.centerOffset = CGPointMake(0, -20);
            SelectedAnnotation = annotationView;
        }
        singleAnnotation.title = singleAnnotation.groupTag;
        
        UIImageView *pulseRingImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        pulseRingImg.center = CGPointMake(0, -20);
        if ([singleAnnotation.groupTag isEqualToString:OUTLETES]) {
            
            annotationView.image = [UIImage imageNamed:@"mapbranchic.png"];
            pulseRingImg.image = [UIImage imageNamed:@"branchcircle"];
        }
        else if([singleAnnotation.groupTag isEqualToString:BRANCHES]){
            
            annotationView.image = [UIImage imageNamed:@"mapstoresic.png"];
            pulseRingImg.image = [UIImage imageNamed:@"storescircle"];
        }
        else if([singleAnnotation.groupTag isEqualToString:LABORATORIES]){
            
            annotationView.image = [UIImage imageNamed:@"maplabic.png"];
            pulseRingImg.image = [UIImage imageNamed:@"labcircle"];
        }
        
        UITapGestureRecognizer *branchRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(DisplayBranchDetails:)];
        branchRecognizer.numberOfTapsRequired=1;
        annotationView.DeatailsInformation = singleAnnotation.DeatailsInformation;
        [annotationView addGestureRecognizer:branchRecognizer];
        
       
       


    }
    @catch (NSException *exception) {
        
    }
    
    return annotationView;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
}
- (IBAction)BackAction:(UIButton *)sender {
    @try {
        if (ComeFromSearchResults) {
            if (self.delegate) {
                [self.delegate didClickBackButtonFromBranchDetails];
            }
        }
        else{
           [self.navigationController popViewControllerAnimated:YES];
        }
    }
    @catch (NSException *exception) {
        
    }
}
- (IBAction)SearchAction:(UIButton *)sender {
    @try {
        [SearchButton setImage:[UIImage imageNamed:@"searchbtnhover.png"] forState:UIControlStateNormal];
        [SearchButton setImage:[UIImage imageNamed:@"searchbtnhover.png"] forState:UIControlStateHighlighted];
        [self HideDetailsView];
        [MapView setUserInteractionEnabled:NO];
        [SearchHeaderView setHidden:NO];
        [SearchHeaderView setAlpha:0];
        [v_Search setHidden:NO];
        [v_Search setAlpha:0];
        [v_Search reloadData];
      
        [self.view bringSubviewToFront:v_Search];
        [self.view bringSubviewToFront:SearchHeaderView];
        [UIView animateWithDuration:0.3 animations:^{
            [SearchHeaderView setAlpha:1];
            [v_Search setAlpha:0.9];
        }];
    }
    @catch (NSException *exception) {
        
    }
    
}
- (IBAction)CancelSearchAction:(UIButton *)sender {
    @try {
        [SearchButton setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
        [SearchButton setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateHighlighted];
         [MapView setUserInteractionEnabled:YES];
        [SearchTextField resignFirstResponder];
        [SearchHeaderView setHidden:YES];
        [self.view sendSubviewToBack:SearchHeaderView];
        [v_Search setHidden:YES];
        [self.view sendSubviewToBack:v_Search];
        
    }
    @catch (NSException *exception) {
        
    }

}

#pragma -mark uitextfield delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ([textField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
        [[[UIAlertView alloc]initWithTitle:nil message:@"برجاء ادخال اسم الفرع الرئيسى" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil, nil]show];
        return YES ;
    }
   
        UIStoryboard *countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone"
                                                                    bundle:nil];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad"
                                                          bundle:nil];
        }
        [SearchTextField resignFirstResponder];
        [SearchHeaderView setHidden:YES];
        [self.view sendSubviewToBack:SearchHeaderView];
        [v_Search setHidden:YES];
        [self.view sendSubviewToBack:v_Search];
  
        SearchResultsViewController *vc_searchResults=[countryStoryboard  instantiateViewControllerWithIdentifier:@"SearchResultsViewController"];
        vc_searchResults.SearchedText = [SearchTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        vc_searchResults.delegate=self;
        [[SlideNavigationController sharedInstance] pushViewController:vc_searchResults animated:YES];
        textField.text=@"";
    
    return YES;
}
@end
