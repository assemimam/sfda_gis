//
//  BranchDetailsMapViewController.h
//  SFDA_GIS
//
//  Created by Assem Imam on 8/27/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol BranchDetailsDelegate <NSObject>
@optional
-(void)didClickBackButtonFromBranchDetails;
@end
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface BranchDetailsMapViewController : UIViewController<MKMapViewDelegate,UITextFieldDelegate>
{
    
    __weak IBOutlet UIButton *SearchButton;
    __weak IBOutlet UIButton *CancelSearchButton;
    __weak IBOutlet UITextField *SearchTextField;
    __weak IBOutlet UIView *SearchHeaderView;
    __weak IBOutlet UIView *HeaderView;
    __weak IBOutlet MKMapView *MapView;
    __weak IBOutlet UILabel *TitleLabel;
    __weak IBOutlet UIButton *BackButton;
}
@property(nonatomic)BOOL ComeFromSearchResults;
@property(assign)id<BranchDetailsDelegate>delegate;
@property(nonatomic,retain)id SelectedBranch;
- (IBAction)SearchAction:(UIButton *)sender;
- (IBAction)BackAction:(UIButton *)sender;
- (IBAction)CancelSearchAction:(UIButton *)sender;
@end
