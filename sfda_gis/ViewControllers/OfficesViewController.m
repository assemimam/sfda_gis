//
//  OfficesViewController.m
//  SFDA_GIS
//
//  Created by Assem Imam on 8/19/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "OfficesViewController.h"
#import "TableHeaderView.h"
#import  "BranchCell.h"
#import "BranchDetailsMapViewController.h"

@interface OfficesViewController ()
{
    NSMutableArray *groupedBranchesList;
    NSUInteger _selectedIndex;
}
@end

@implementation OfficesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }

    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    TitleLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:TitleLabel.font.pointSize];
    NoresultLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:NoresultLabel.font.pointSize];
    BackButton.titleLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:BackButton.titleLabel.font.pointSize];
    
    groupedBranchesList = [[NSMutableArray alloc]init];
    id typeFilteredArray=[[NSArray alloc]init];
    NSString *iconTitleImageName;
    NSString *noDataMessage=@"";
    switch (self.BranchType) {
        case BRANCH:
        {
            NSString *filterCondition =@"BranchTypeId == '2'";
            NSPredicate *predicate =[NSPredicate predicateWithFormat:filterCondition];
            typeFilteredArray = [BranchesList filteredArrayUsingPredicate:predicate];
            TitleLabel.text=@"فروع الهيئة";
            TitleLabel.textColor = [UIColor colorWithRed:178/255.0f green:91/255.0f blue:109/255.0f alpha:1];
            iconTitleImageName=@"storesic";
            noDataMessage =@"لا توجد فروع";
        }
            break;
            
        case OUTLET:
        {
            NSString *filterCondition =@"BranchTypeId == '1'";
            NSPredicate *predicate =[NSPredicate predicateWithFormat:filterCondition ];
            typeFilteredArray = [BranchesList filteredArrayUsingPredicate:predicate];
            TitleLabel.text=@"منافذ الهيئة";
            TitleLabel.textColor = [UIColor colorWithRed:88/255.0f green:93/255.0f blue:147/255.0f alpha:1];
            iconTitleImageName=@"branchsic";
            noDataMessage =@"لا توجد منافذ فرعية";
        }
            break;
        case LABORATORY:
        {
            NSString *filterCondition =@"BranchTypeId == '3'";
            NSPredicate *predicate =[NSPredicate predicateWithFormat:filterCondition];
            typeFilteredArray = [BranchesList filteredArrayUsingPredicate:predicate];
            TitleLabel.text=@"مختبــرات الهيئــة";
            TitleLabel.textColor = [UIColor colorWithRed:58/255.0f green:169/255.0f blue:157/255.0f alpha:1];
            iconTitleImageName=@"labsic";
              noDataMessage =@"لا توجد مختبرات فرعية";
        }
            break;
    }
    NoresultLabel.text = [textConverter convertArabic:noDataMessage];
    TitleLabel.text=[textConverter convertArabic:TitleLabel.text];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {

        [IconTitlButton setImage:[UIImage imageNamed:iconTitleImageName] forState:UIControlStateNormal];
        [IconTitlButton setImage:[UIImage imageNamed:iconTitleImageName] forState:UIControlStateHighlighted];
        [IconTitlButton setImage:[UIImage imageNamed:iconTitleImageName] forState:UIControlStateSelected];
    }
    if (typeFilteredArray) {
        if ([typeFilteredArray count]>0) {
            for (id mainBranch in MainBranches) {
                NSMutableDictionary *groubedBranch = [[NSMutableDictionary alloc]init];
                [groubedBranch setObject:[mainBranch objectForKey:@"Id"] forKey:@"Id"];
                [groubedBranch setObject:[mainBranch objectForKey:@"Name"] forKey:@"Name"];
               
                NSPredicate *predicate =[NSPredicate predicateWithFormat:@"MainBranchId == %@", [mainBranch objectForKey:@"Id"]  ];
                id FilteredArray = [typeFilteredArray filteredArrayUsingPredicate:predicate];
                
                [groubedBranch setObject:FilteredArray forKey:@"Branches"];
                [groupedBranchesList addObject:groubedBranch];
            }
        }
        else{
            [OfficesTableView setHidden:YES];
            [NoResultView setHidden:NO];
            [self.view bringSubviewToFront:NoResultView];
        }
    }
    else{
        [OfficesTableView setHidden:YES];
        [NoResultView setHidden:NO];
        [self.view bringSubviewToFront:NoResultView];
    }
    [OfficesTableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BranchCell *cell;
    @try {
        
        cell=(BranchCell*)[tableView dequeueReusableCellWithIdentifier:@"cleBranch" forIndexPath:indexPath];
        cell.BranchNameLabel.text =[[[[groupedBranchesList objectAtIndex:indexPath.section]objectForKey:@"Branches"]objectAtIndex:indexPath.row]objectForKey:@"NameAr"];
        cell.BranchNameLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:cell.BranchNameLabel.font.pointSize];
        cell.BranchNameLabel.text = [textConverter convertArabic: cell.BranchNameLabel.text];
    }
    @catch (NSException *exception) {
        
    }
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger noOfRows=0;
    
    @try {
        noOfRows = [[[groupedBranchesList objectAtIndex:section]objectForKey:@"Branches"]count];
         }
    @catch (NSException *exception) {
        
    }
    
    return noOfRows;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [groupedBranchesList count];
}
#pragma -mark uitableview delegate methods
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize QuestionSize;
    @try {
        NSString *Title= [[[[[groupedBranchesList objectAtIndex:indexPath.section]objectForKey:@"Branches"]objectAtIndex:indexPath.row]objectForKey:@"NameAr"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        
         QuestionSize = [Title sizeWithFont:[UIFont fontWithName:@"GESSTwoBold-Bold" size:13] constrainedToSize:CGSizeMake(tableView.frame.size.width , 2000000000000.0f) lineBreakMode:NSLineBreakByWordWrapping];
    }
    @catch (NSException *exception) {
        
    }
    
    return  MAX( QuestionSize.height,50);
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    TableHeaderView *headerView;
    @try {
        if(groupedBranchesList!=nil && [groupedBranchesList count] > 0){
            NSString *headerText =[NSNull null]!=[[groupedBranchesList objectAtIndex:section]objectForKey:@"Name"]?[textConverter convertArabic:[[groupedBranchesList objectAtIndex:section]objectForKey:@"Name"]]:@"";
             float headerWidth =tableView.frame.size.width;
             headerView = (TableHeaderView*)[[[NSBundle mainBundle]loadNibNamed:@"TableHeaderView" owner:nil options:nil]objectAtIndex:0];
             headerView.frame = CGRectMake(0, 00, headerWidth, 35);
             headerView.autoresizesSubviews = YES;
             headerView.HeaderTitleLabel.text = headerText;
             headerView.HeaderTitleLabel.backgroundColor = [UIColor clearColor];
             headerView.HeaderTitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:headerView.HeaderTitleLabel.font.pointSize];
             headerView.RowsCountLabel.text = [NSString stringWithFormat:@"%i",[[[groupedBranchesList objectAtIndex:section]objectForKey:@"Branches"]count]];
             
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
    headerView.superview.backgroundColor = [UIColor redColor];
    return headerView;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            [self  dismissViewControllerAnimated:YES completion:^{
                UIStoryboard *countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone"
                                                                        bundle:nil];
                if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
                    countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad"
                                                                  bundle:nil];
                }
                
                BranchDetailsMapViewController *vc_branchDetails =(BranchDetailsMapViewController*)[countryStoryboard  instantiateViewControllerWithIdentifier:@"BranchDetailsMapViewController"];
                [OfficesTableView deselectRowAtIndexPath:indexPath animated:YES];
                id selectedBranch =[[[groupedBranchesList objectAtIndex:indexPath.section]objectForKey:@"Branches"]objectAtIndex:indexPath.row];
                vc_branchDetails.SelectedBranch = selectedBranch;
                [[SlideNavigationController sharedInstance]pushViewController:vc_branchDetails animated:YES];
                NSLog(@"selected branch %@",selectedBranch);
            }];

        }
    }
    @catch (NSException *exception) {
        
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"DetailsSegue"])
    {
        BranchDetailsMapViewController *vc_branchDetails =(BranchDetailsMapViewController*)[segue destinationViewController];
        NSIndexPath *indexPath =  [OfficesTableView indexPathForSelectedRow];
        [OfficesTableView deselectRowAtIndexPath:indexPath animated:YES];
        id selectedBranch =[[[groupedBranchesList objectAtIndex:indexPath.section]objectForKey:@"Branches"]objectAtIndex:indexPath.row];
        vc_branchDetails.SelectedBranch = selectedBranch;
        vc_branchDetails.ComeFromSearchResults=NO;
        NSLog(@"selected branch %@",selectedBranch);
    }
   
    
}

- (IBAction)CloseAction:(UIButton *)sender {
    @try {
        [[SlideNavigationController  sharedInstance]dismissViewControllerAnimated:YES completion:nil];
    }
    @catch (NSException *exception) {
        
    }
}
@end
