//
//  AboutViewController.m
//  SFDA_GIS
//
//  Created by Assem Imam on 8/21/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [ContainerScrollView setContentSize:CGSizeMake(ContainerScrollView.frame.size.width, 560)];
    BackButton.titleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:BackButton.titleLabel.font.pointSize];
     EmailTitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:EmailTitleLabel.font.pointSize];
     PhoneTitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:PhoneTitleLabel.font.pointSize];
     WebsiteTitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:WebsiteTitleLabel.font.pointSize];
     AddressLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:AddressLabel.font.pointSize];
    TitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:TitleLabel.font.pointSize];
    AboutTextView.font=[UIFont fontWithName:@"GESSTwoLight-Light" size:AboutTextView.font.pointSize];
    AboutLabel.font=[UIFont fontWithName:@"GESSTwoLight-Light" size:AboutLabel.font.pointSize];
    SfdaAppLabel.font=[UIFont fontWithName:@"GESSTwoLight-Light" size:SfdaAppLabel.font.pointSize];
    ContctsLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:ContctsLabel.font.pointSize];
    SfdaAppsLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:SfdaAppsLabel.font.pointSize];
    
    TitleLabel.text = [textConverter convertArabic:TitleLabel.text];
    AboutTextView.text = [textConverter convertArabic:AboutTextView.text];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)SfdaLinkAction:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.itunes.apple.com/us/app/alghdha-waldwa/id517576348?ls=1&mt=8"]];
}

- (IBAction)CloseAction:(UIButton *)sender {
    @try {
        [[SlideNavigationController  sharedInstance]dismissViewControllerAnimated:YES completion:nil];
    }
    @catch (NSException *exception) {
        
    }
}
@end
