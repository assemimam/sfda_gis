//
//  SplashViewController.m
//  SFDA_GIS
//
//  Created by Assem Imam on 8/14/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "SplashViewController.h"
#import "SplashView.h"
#import "ViewController.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma -mark uiscrollview delegate
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = SplashScrollView.frame.size.width;
    int page = floor((SplashScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
   SplashPageControl.currentPage = page;
}
- (void)viewDidLoad
{
   
   [super viewDidLoad];
    NSURL*url=[NSURL URLWithString:@"prefs:root=WIFI"];
    [[UIApplication sharedApplication] openURL:url];
    
   [[UIApplication sharedApplication]setStatusBarHidden:YES];
    for (int i = 0; i<3; i++) {
        SplashView *addedView = (SplashView*)[[[NSBundle mainBundle]loadNibNamed:@"SplashView" owner:nil options:nil]objectAtIndex:0];
        [addedView setFrame:CGRectMake(i*SplashScrollView.frame.size.width, 0, SplashScrollView.frame.size.width, SplashScrollView.frame.size.height)];
        
        switch (i) {
            case 0:
            {
                addedView.SplashImageView.image = [UIImage imageNamed:@"tour_3.png"];
                [addedView.StartApplicationButton setHidden:NO];
                [addedView.StartApplicationLabel setHidden:NO];
                [addedView.StartApplicationButton addTarget:self action:@selector(StartApplicationButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                [SplashScrollView setContentOffset:CGPointMake(3*addedView.frame.size.width, 0)];
            }
                break;
                
            case 1:
            {
               addedView.SplashImageView.image = [UIImage imageNamed:@"tour_2.png"];
            }
                break;
            case 2:
            {
                [SplashScrollView setContentSize:CGSizeMake(3*addedView.frame.size.width, SplashScrollView.frame.size.height)];
                addedView.SplashImageView.image = [UIImage imageNamed:@"tour_1.png"];
                
            }
                break;
        }
        [SplashScrollView addSubview:addedView];
    }
}

-(void)StartApplicationButtonAction:(UIButton*)sender
{
    @try {
        [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"splash"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        UIStoryboard *countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone"
                                                                    bundle:nil];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad"
                                                          bundle:nil];
        }
        ViewController *vc_main =[countryStoryboard  instantiateViewControllerWithIdentifier:@"ViewController"];
        [self.navigationController pushViewController:vc_main animated:YES];
    }
    @catch (NSException *exception) {
        
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
