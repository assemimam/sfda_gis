//
//  MasterViewController.h
//  SFDA_GIS
//
//  Created by Assem Imam on 8/5/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LibrariesHeader.h"
static NSString *const LABORATORIES = @"3";
static NSString *const OUTLETES = @"1";
static NSString *const BRANCHES = @"2";
static int const SINGLE_ANNOTATION_TAG = 2000;
@interface MasterViewController : UIViewController
{
    __weak IBOutlet UIButton *RightMenuButton;
}
- (IBAction)BackAction:(UIButton *)sender;
@end
