//
//  SplashViewController.h
//  SFDA_GIS
//
//  Created by Assem Imam on 8/14/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashViewController : UIViewController<UIScrollViewDelegate>
{
    __weak IBOutlet UIPageControl *SplashPageControl;
    __weak IBOutlet UIScrollView *SplashScrollView;
}
@end
