//
//  MasterViewController.m
//  SFDA_GIS
//
//  Created by Assem Imam on 8/5/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "MasterViewController.h"

#import "RightmenuViewController.h"
@interface MasterViewController ()
{
    BOOL *showSidemenu;
}
@end

@implementation MasterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [SlideNavigationController sharedInstance].rightBarButtonItem=RightMenuButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma -mark slidenavigation delegate
- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
   // ((RightmenuViewController*) [SlideNavigationController sharedInstance].rightMenu).delegate=self;
    return YES;
}
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    
    return NO;
}


#pragma mark - Helpers -

- (NSInteger)indexFromPixels:(NSInteger)pixels
{
    if (pixels == 60)
        return 0;
    else if (pixels == 120)
        return 1;
    else
        return 2;
}

- (NSInteger)pixelsFromIndex:(NSInteger)index
{
    switch (index)
    {
        case 0:
            return 60;
            
        case 1:
            return 120;
            
        case 2:
            return 200;
            
        default:
            return 0;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)BackAction:(UIButton *)sender {
    @try {
        [[SlideNavigationController sharedInstance]popToRootViewControllerAnimated:YES];
    }
    @catch (NSException *exception) {
        
    }
}
@end
