//
//  ViewController.m
//  SFDA_GIS
//
//  Created by Assem Imam on 8/5/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "ViewController.h"
#import "OCMapViewSampleHelpAnnotation.h"
#import "LibrariesHeader.h"
#import "CustomAnnotationView.h"
#import "FilterView.h"
#import "BranchDetailsView.h"
#import "RoutingViewController.h"
#import "SearchView.h"
#import "BranchDetailsMapViewController.h"
#import "SearchResultsViewController.h"
#import "NoInternetMessageView.h"

#define MAIN_BRANCHES_KEY @"main_branches"
#define BRANCHES_KEY @"branches"
static int const FILTER_VIEW_TAG = 1000;
static int const MAP_VIEW_TAG = 0;
static int const SETTLITE_VIEW_TAG = 1;
@interface ViewController ()<FilterDelegate,SearchDelegate,SlideMenuDelegate,UIPopoverControllerDelegate,SearchResultsDelegate,BranchDetailsDelegate,CLLocationManagerDelegate>
{
    YLActivityIndicatorView* LoadingIndicator;
    CustomAnnotationView*SelectedAnnotation;
    BranchDetailsView *v_branchDetails;
    SearchView *v_Search;
    float ZoomLevel ;
    UIPopoverController *filterPickerPopover;
    BOOL showDetailsView;
    FilterView *filterView;
    UIViewController *filterController;
    NoInternetMessageView *v_NoInternet;
    BOOL ShowSearchView;
    UIPopoverController *sharingPickerPopover;
    CLLocationManager *locationManager;
    CLLocation *selectedLocation;
}
@end

@implementation ViewController
#pragma -mark SearchResults delegate
-(void)didClickBackButton
{
    @try {
        [self SearchAction:[[UIButton alloc]init]];
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark uipopover delegate
-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    @try {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
             [MapView setScrollEnabled:YES];
            [FilterButton setImage:[UIImage imageNamed:@"filter.png"] forState:UIControlStateNormal];
            [FilterButton setImage:[UIImage imageNamed:@"filter.png"] forState:UIControlStateHighlighted];
            FilterButton.tag=0;
            [MapView setUserInteractionEnabled:YES];
            
            [filterPickerPopover dismissPopoverAnimated:YES];
        }
    }
    @catch (NSException *exception) {
        
    }
    
}
-(id)GetCashedMainBranches
{
    id  result;
    [[Cashing getObject] setDatabase:@"SFDA_GIS"];
    @try {
        
        DB_Field  *fldID = [[DB_Field alloc]init];
        fldID.FieldName=@"id";
        fldID.FieldAlias=@"Id";
        
        DB_Field  *fldName = [[DB_Field alloc]init];
        fldName.FieldName=@"Name";
        
        DB_Field  *fldZone= [[DB_Field alloc]init];
        fldZone.FieldName=@"Zone";
        
        DB_Recored *addedRecored = [[DB_Recored alloc]init];
        addedRecored.Fields = [NSMutableArray  arrayWithObjects:fldID,fldName,fldZone, nil];
        
        result= [[Cashing getObject] getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldID,fldName,fldZone, nil] Tables:[NSArray arrayWithObjects:@"main_branches",nil ] Where:nil FromIndex:nil ToIndex:nil OrderByField:nil AscendingOrder:YES];
        
        
        
    }
    @catch (NSException *exception) {
        
    }
    return result;
}

-(void)CashMainBranches
{
    [[Cashing getObject] setDatabase:@"SFDA_GIS"];
    @try {
        for (id BranchDetailsDic in MainBranches) {
            
            DB_Field  *fldID = [[DB_Field alloc]init];
            fldID.FieldName=@"id";
            fldID.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            fldID.IS_PRIMARY_KEY=YES;
            fldID.FieldValue=[NSString stringWithFormat:@"%i",[[BranchDetailsDic objectForKey:@"Id"]intValue]];
            
            DB_Field  *fldName = [[DB_Field alloc]init];
            fldName.FieldName=@"Name";
            fldName.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldName.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"Name"]?[BranchDetailsDic objectForKey:@"Name"]:@"";
            
            DB_Field  *fldZone= [[DB_Field alloc]init];
            fldZone.FieldName=@"Zone";
            fldZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldZone.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"Zone"]?[BranchDetailsDic objectForKey:@"Zone"]:@"";
            
            DB_Recored *addedRecored = [[DB_Recored alloc]init];
            addedRecored.Fields = [NSMutableArray  arrayWithObjects:fldID,fldName,fldZone, nil];
            
            BOOL isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:addedRecored] inTable:@"main_branches"];
            if (isSuccess) {
                [[NSUserDefaults standardUserDefaults]setObject:[NSDate date] forKey:MAIN_BRANCHES_KEY];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            
        }
         [NSThread detachNewThreadSelector:@selector(getBranches) toTarget:self withObject:nil];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)handleGetMainBranches
{
    @try {
        if (MainBranches) {
            if ([MainBranches count]>0) {
                [self  performSelectorInBackground:@selector(CashMainBranches) withObject:nil];
            }
            else
            {
                [self HideLoading];
                //dislay no connection message
                NSLog(@"no internet display message");
                [v_NoInternet setHidden:NO];
                [self.view bringSubviewToFront:v_NoInternet];
            }
        }
        else{
            [self HideLoading];
            //dislay no connection message
            NSLog(@"no internet display message");
            [v_NoInternet setHidden:NO];
            [self.view bringSubviewToFront:v_NoInternet];
        }
    }
    @catch (NSException *exception) {
        
    }
   
}
-(void)getMainBranches
{
    @try {
        MainBranches= [[NSArray alloc]init];
        MainBranches = [Webservice GetMainBranches];
        [self performSelectorOnMainThread:@selector(handleGetMainBranches) withObject:nil waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
}

#pragma -mark slide delegate
-(void)didSlideMenuForOpenState:(BOOL)open_state{
    if (open_state) {
        [self HideFilterView];
        [self HideDetailsView];
        [self CancelSearchAction:[[UIButton alloc]init]];
        [MapView setUserInteractionEnabled:NO];
        [MapView setScrollEnabled:NO];
    }
    else{
        if (ShowSearchView==NO) {
            [MapView setUserInteractionEnabled:YES];
            [MapView setScrollEnabled:YES];
        }
    }
}
-(void)viewDidDisappear:(BOOL)animated
{
    
    [SlideNavigationController sharedInstance].delegate=nil;
}
-(void)viewWillAppear:(BOOL)animated
{
    [SlideNavigationController sharedInstance].menuDelegate = self;
}
-(id)GetCashedBranches
{
    NSMutableArray* result = [[NSMutableArray alloc]init];
    [[Cashing getObject] setDatabase:@"SFDA_GIS"];
    @try {
        DB_Field  *fldID = [[DB_Field alloc]init];
        fldID.FieldName=@"id";
        fldID.FieldAlias=@"Id";
        
        DB_Field  *fldNameAr = [[DB_Field alloc]init];
        fldNameAr.FieldName=@"NameAr";
        
        DB_Field  *fldNameEn = [[DB_Field alloc]init];
        fldNameEn.FieldName=@"NameEn";
        
        DB_Field  *fldSectionId = [[DB_Field alloc]init];
        fldSectionId.FieldName=@"SectionId";
        
        DB_Field  *fldMainBranchId = [[DB_Field alloc]init];
        fldMainBranchId.FieldName=@"MainBranchId";
        
        DB_Field  *fldBranchTypeId = [[DB_Field alloc]init];
        fldBranchTypeId.FieldName=@"BranchTypeId";
        
        DB_Field  *fldLatitude = [[DB_Field alloc]init];
        fldLatitude.FieldName=@"Latitude";
        
        DB_Field  *fldLongitude= [[DB_Field alloc]init];
        fldLongitude.FieldName=@"Longitude";
        
        DB_Field  *fldEmail = [[DB_Field alloc]init];
        fldEmail.FieldName=@"Email";
        
        DB_Field  *fldAddress = [[DB_Field alloc]init];
        fldAddress.FieldName=@"Address";
        
        DB_Field  *fldFax = [[DB_Field alloc]init];
        fldFax.FieldName=@"Fax";
        
        DB_Field  *fldFirstPhone = [[DB_Field alloc]init];
        fldFirstPhone.FieldName=@"FirstPhone";
        
        DB_Field  *fldSecondPhone = [[DB_Field alloc]init];
        fldSecondPhone.FieldName=@"SecondPhone";
        
        DB_Field  *fldHasFood = [[DB_Field alloc]init];
        fldHasFood.FieldName=@"HasFood";
        
        DB_Field  *fldHasDrug = [[DB_Field alloc]init];
        fldHasDrug.FieldName=@"HasDrug";
        
        DB_Field  *fldHasMedical = [[DB_Field alloc]init];
        fldHasMedical.FieldName=@"HasMedical";
        
        DB_Field  *fldMainBranchName = [[DB_Field alloc]init];
        fldMainBranchName.FieldName=@"MainBranchName";
   
        id BranchesList =[[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldID,fldNameAr,fldNameEn,fldAddress,fldFax,fldEmail,fldFirstPhone,fldSecondPhone,fldMainBranchId,fldSectionId,fldLatitude,fldLongitude,fldHasMedical,fldHasFood,fldHasDrug,fldBranchTypeId,fldMainBranchName, nil] Tables:[NSArray arrayWithObject:@"branches"] Where:nil FromIndex:nil ToIndex:nil OrderByField:nil AscendingOrder:YES];
        if (BranchesList) {
            if ([BranchesList count]>0) {
                for (id branch in BranchesList) {
                    NSMutableDictionary *branchDic = [branch mutableCopy];
                    
                    DB_Field  *fldDetailsId = [[DB_Field alloc]init];
                    fldDetailsId.FieldName=@"Id";
                    
                    
                    DB_Field  *fldKey = [[DB_Field alloc]init];
                    fldKey.FieldName=@"Key";
                    
                    
                    DB_Field  *fldValue = [[DB_Field alloc]init];
                    fldValue.FieldName=@"Value";
                    
                    DB_Field  *fldOrder = [[DB_Field alloc]init];
                    fldOrder.FieldName=@"key_order";
                    
                    
                    DB_Field  *fldType = [[DB_Field alloc]init];
                    fldType.FieldName=@"Type";
                    
                    id BranchDetails =[[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldDetailsId,fldKey,fldValue,fldOrder,fldType, nil] Tables:[NSArray arrayWithObject:@"branch_details"] Where:[NSString stringWithFormat:@"branch_id=%i",[[branch objectForKey:@"Id"]intValue]]  FromIndex:nil ToIndex:nil OrderByField:nil AscendingOrder:YES];
                    if (BranchDetails) {
                        [branchDic setObject:BranchDetails forKey:@"BranchDetails"];
                    }
                    
                    [result addObject:branchDic];
                }
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
    return result;
}
#pragma -mark branch Details delegate methods
-(void)didClickBackButtonFromBranchDetails
{
    @try {
         [self.navigationController popViewControllerAnimated:YES];
        [self didClickBackButton];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

#pragma -mark search delegate methods

-(void)didSelectNearByItem:(id)item
{
    @try {

        [SearchTextField resignFirstResponder];
        [LogoImageView setHidden:NO];
        [SearchHeaderView setHidden:YES];
        [self.view sendSubviewToBack:SearchHeaderView];
        [v_Search setHidden:YES];
        [self.view sendSubviewToBack:v_Search];
        UIStoryboard *countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone"
                                                                    bundle:nil];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad"
                                                          bundle:nil];
        }
        BranchDetailsMapViewController *vc_branchDetails=[countryStoryboard  instantiateViewControllerWithIdentifier:@"BranchDetailsMapViewController"];
        vc_branchDetails.SelectedBranch = item;
        vc_branchDetails.delegate= self;
        vc_branchDetails.ComeFromSearchResults=YES;
        [[SlideNavigationController sharedInstance] pushViewController:vc_branchDetails animated:YES];
        [MapView setUserInteractionEnabled:YES];
        [MapView  setScrollEnabled:YES];
        NSLog(@"selected branch %@",item);
    }
    @catch (NSException *exception) {
        
    }
}
-(void)didSelectRecentItem:(id)item
{
    @try {

        [SearchTextField resignFirstResponder];
        [SearchHeaderView setHidden:YES];
        [LogoImageView setHidden:NO];
        [self.view sendSubviewToBack:SearchHeaderView];
        [v_Search setHidden:YES];
        [self.view sendSubviewToBack:v_Search];
        
        UIStoryboard *countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone"
                                                                    bundle:nil];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad"
                                                          bundle:nil];
        }
        BranchDetailsMapViewController *vc_branchDetails=[countryStoryboard  instantiateViewControllerWithIdentifier:@"BranchDetailsMapViewController"];
        vc_branchDetails.SelectedBranch = item;
        vc_branchDetails.delegate=self;
        vc_branchDetails.ComeFromSearchResults=YES;
        [[SlideNavigationController sharedInstance] pushViewController:vc_branchDetails animated:YES];
        [MapView setUserInteractionEnabled:YES];
        [MapView  setScrollEnabled:YES];
        NSLog(@"selected branch %@",item);
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark filter delegate methods
-(void)didChangeFilterOptions:(id)options
{
    @try {
        NSPredicate *predicate ;
        NSArray *firstSectionOptions;
        NSArray *seconedectionOptions;
        NSArray *filteredArray;
        NSString*filteredString=@"";
        
        if ([options count]>0) {
            predicate =[NSPredicate predicateWithFormat:@"section == %@",@"0"];
            firstSectionOptions = [options filteredArrayUsingPredicate:predicate];
            
            predicate =[NSPredicate predicateWithFormat:@"section == %@",@"1"];
            seconedectionOptions = [options filteredArrayUsingPredicate:predicate];
            if ([firstSectionOptions count]>0) {
                filteredString =[filteredString stringByAppendingString:@"("];
                for (id option in firstSectionOptions) {

                        filteredString=[filteredString stringByAppendingFormat:@"( %@ == '%@' ) || ",[option objectForKey:@"fiter_key_string"],[option objectForKey:@"filter_key_value"]];
                    
                }
                filteredString =[filteredString substringToIndex:[filteredString length]-3];
                filteredString =[filteredString stringByAppendingString:@")"];
                filteredString =[filteredString stringByAppendingString:@" && "];
            }
            if ([seconedectionOptions count]>0) {
                filteredString =[filteredString stringByAppendingString:@"("];
                for (id option in seconedectionOptions) {
     
   
                        filteredString=[filteredString stringByAppendingFormat:@"( %@ == '%@' ) || ",[option objectForKey:@"fiter_key_string"],[option objectForKey:@"filter_key_value"]];
                        
                    
                }
                filteredString =[filteredString substringToIndex:[filteredString length]-3];
                filteredString =[filteredString stringByAppendingString:@")"];
            }
            else{
                filteredString = [filteredString substringToIndex:filteredString.length - 3];
            }
            
        }
        else{
            filteredString=@"Id=-1000011000111";
        }
        
        NSLog(@"%@",filteredString);
        predicate =[NSPredicate predicateWithFormat:filteredString];
        filteredArray = [BranchesList filteredArrayUsingPredicate:predicate];
        [self DoMapClustringWithData:filteredArray];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)CashBranches
{
    [[Cashing getObject] setDatabase:@"SFDA_GIS"];
    @try {
        for (id BranchDetailsDic in BranchesList) {
            
            DB_Field  *fldID = [[DB_Field alloc]init];
            fldID.FieldName=@"id";
            fldID.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            fldID.IS_PRIMARY_KEY=YES;
            fldID.FieldValue=[NSString stringWithFormat:@"%i",[[BranchDetailsDic objectForKey:@"Id"]intValue]];
            
            DB_Field  *fldNameAr = [[DB_Field alloc]init];
            fldNameAr.FieldName=@"NameAr";
            fldNameAr.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldNameAr.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"NameAr"]?[BranchDetailsDic objectForKey:@"NameAr"]:@"";
            
            DB_Field  *fldNameEn = [[DB_Field alloc]init];
            fldNameEn.FieldName=@"NameEn";
            fldNameEn.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldNameEn.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"NameEn"]?[BranchDetailsDic objectForKey:@"NameEn"]:@"";
            
            
            DB_Field  *fldSectionId = [[DB_Field alloc]init];
            fldSectionId.FieldName=@"SectionId";
            fldSectionId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            fldSectionId.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"SectionId"]?[NSString stringWithFormat:@"%i",[[BranchDetailsDic objectForKey:@"SectionId"]intValue]]:@"";
            
            DB_Field  *fldMainBranchId = [[DB_Field alloc]init];
            fldMainBranchId.FieldName=@"MainBranchId";
            fldMainBranchId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            fldMainBranchId.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"MainBranchId"]?[NSString stringWithFormat:@"%i",[[BranchDetailsDic objectForKey:@"MainBranchId"]intValue]]:@"";
            
            DB_Field  *fldMainBranchName = [[DB_Field alloc]init];
            fldMainBranchName.FieldName=@"MainBranchName";
            fldMainBranchName.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldMainBranchName.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"MainBranchName"]?[BranchDetailsDic objectForKey:@"MainBranchName"]:@"";
            
            DB_Field  *fldBranchTypeId = [[DB_Field alloc]init];
            fldBranchTypeId.FieldName=@"BranchTypeId";
            fldBranchTypeId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            fldBranchTypeId.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"BranchTypeId"]?[NSString stringWithFormat:@"%i",[[BranchDetailsDic objectForKey:@"BranchTypeId"]intValue]]:@"";
            
            DB_Field  *fldLatitude = [[DB_Field alloc]init];
            fldLatitude.FieldName=@"Latitude";
            fldLatitude.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldLatitude.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"Latitude"]?[NSString stringWithFormat:@"%f",[[BranchDetailsDic objectForKey:@"Latitude"]floatValue]]:@"";
        
            DB_Field  *fldLongitude= [[DB_Field alloc]init];
            fldLongitude.FieldName=@"Longitude";
            fldLongitude.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldLongitude.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"Longitude"]?[NSString stringWithFormat:@"%f",[[BranchDetailsDic objectForKey:@"Longitude"]floatValue]]:@"";
            
            DB_Field  *fldEmail = [[DB_Field alloc]init];
            fldEmail.FieldName=@"Email";
            fldEmail.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldEmail.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"Email"]?[BranchDetailsDic objectForKey:@"Email"]:@"";
            
            DB_Field  *fldAddress = [[DB_Field alloc]init];
            fldAddress.FieldName=@"Address";
            fldAddress.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldAddress.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"Address"]?[BranchDetailsDic objectForKey:@"Address"]:@"";
            
            
            DB_Field  *fldFax = [[DB_Field alloc]init];
            fldFax.FieldName=@"Fax";
            fldFax.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldFax.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"Fax"]?[BranchDetailsDic objectForKey:@"Fax"]:@"";
            
            DB_Field  *fldFirstPhone = [[DB_Field alloc]init];
            fldFirstPhone.FieldName=@"FirstPhone";
            fldFirstPhone.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldFirstPhone.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"FirstPhone"]?[BranchDetailsDic objectForKey:@"FirstPhone"]:@"";
            
            DB_Field  *fldSecondPhone = [[DB_Field alloc]init];
            fldSecondPhone.FieldName=@"SecondPhone";
            fldSecondPhone.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldSecondPhone.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"SecondPhone"]?[BranchDetailsDic objectForKey:@"SecondPhone"]:@"";
            
            DB_Field  *fldHasFood = [[DB_Field alloc]init];
            fldHasFood.FieldName=@"HasFood";
            fldHasFood.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldHasFood.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"HasFood"]?[NSString stringWithFormat:@"%i",[[BranchDetailsDic objectForKey:@"HasFood"]intValue]]:@"";
            
            DB_Field  *fldHasDrug = [[DB_Field alloc]init];
            fldHasDrug.FieldName=@"HasDrug";
            fldHasDrug.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldHasDrug.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"HasDrug"]?[NSString stringWithFormat:@"%i",[[BranchDetailsDic objectForKey:@"HasDrug"]intValue]]:@"";
            
            DB_Field  *fldHasMedical = [[DB_Field alloc]init];
            fldHasMedical.FieldName=@"HasMedical";
            fldHasMedical.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldHasMedical.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"HasMedical"]?[NSString stringWithFormat:@"%i",[[BranchDetailsDic objectForKey:@"HasMedical"]intValue]]:@"";
            
            DB_Recored *addedRecored = [[DB_Recored alloc]init];
            addedRecored.Fields = [NSMutableArray  arrayWithObjects:fldID,fldNameAr,fldNameEn,fldSectionId,fldMainBranchId,fldBranchTypeId,fldLatitude,fldLongitude,fldEmail,fldAddress,fldFax,fldFirstPhone,fldSecondPhone,fldHasDrug,fldHasFood,fldHasMedical,fldMainBranchName, nil];
            
            BOOL isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:addedRecored] inTable:@"branches"];
            
            if (isSuccess) {
                [[NSUserDefaults standardUserDefaults]setObject:[NSDate date] forKey:BRANCHES_KEY];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                for (id BranchDetails in [BranchDetailsDic objectForKey:@"BranchDetails"]) {
                    DB_Field  *fldDetailsId = [[DB_Field alloc]init];
                    fldDetailsId.FieldName=@"Id";
                    fldDetailsId.IS_PRIMARY_KEY=YES;
                    fldDetailsId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                    fldDetailsId.FieldValue=[NSNull null]!=[BranchDetails objectForKey:@"Id"]?[BranchDetails objectForKey:@"Id"]:@"";
                    
                    DB_Field  *fldBranchID = [[DB_Field alloc]init];
                    fldBranchID.FieldName=@"branch_id";
                    fldBranchID.IS_PRIMARY_KEY=YES;
                    fldBranchID.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                    fldBranchID.FieldValue=[NSString stringWithFormat:@"%i",[[BranchDetailsDic objectForKey:@"Id"]intValue]];
                    
                    DB_Field  *fldKey = [[DB_Field alloc]init];
                    fldKey.FieldName=@"Key";
                    fldKey.FieldDataType=FIELD_DATA_TYPE_TEXT;
                    fldKey.FieldValue=[NSNull null]!=[BranchDetails objectForKey:@"Key"]?[BranchDetails objectForKey:@"Key"]:@"";
                    
                    DB_Field  *fldValue = [[DB_Field alloc]init];
                    fldValue.FieldName=@"Value";
                    fldValue.FieldDataType=FIELD_DATA_TYPE_TEXT;
                    fldValue.FieldValue=[NSNull null]!=[BranchDetails objectForKey:@"Value"]?[BranchDetails objectForKey:@"Value"]:@"";
                    
                    DB_Field  *fldOrder = [[DB_Field alloc]init];
                    fldOrder.FieldName=@"key_order";
                    fldOrder.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                    fldOrder.FieldValue=[NSNull null]!=[BranchDetails objectForKey:@"Order"]?[BranchDetails objectForKey:@"Order"]:@"";
                    
                    DB_Field  *fldType = [[DB_Field alloc]init];
                    fldType.FieldName=@"Type";
                    fldType.FieldDataType=FIELD_DATA_TYPE_TEXT;
                    fldType.FieldValue=[NSNull null]!=[BranchDetails objectForKey:@"Type"]?[BranchDetails objectForKey:@"Type"]:@"";
                    
                    DB_Recored *addedRecored = [[DB_Recored alloc]init];
                    addedRecored.Fields = [NSMutableArray  arrayWithObjects:fldBranchID,fldDetailsId,fldKey,fldValue,fldOrder,fldType, nil];
                    BOOL isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:addedRecored] inTable:@"branch_details"];
                    
                }
                
            }
            
        }
          BranchesList = [[NSArray alloc]init];
          MainBranches = [[NSArray alloc]init];
          BranchesList =[self GetCashedBranches];
          MainBranches =[self GetCashedMainBranches];
         [self HideLoading];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)DoMapClustringWithData:(id)data
{
    @try {
        NSMutableSet *annotationsToAdd = [[NSMutableSet alloc] init];
        for (id anotation in MapView.annotations) {
            [MapView removeAnnotation:anotation];
        }
        MKCoordinateRegion visibleRegion = MapView.region;
        visibleRegion.span.latitudeDelta *= 0.0;
        visibleRegion.span.longitudeDelta *= 0.0;
        
        for (id branch in data) {
            // start with top left corner
            CLLocationCoordinate2D location;
            location.latitude = [[branch valueForKey:@"Latitude"] doubleValue];
            location.longitude = [[branch valueForKey:@"Longitude"] doubleValue];
            
            OCMapViewSampleHelpAnnotation *annotation = [[OCMapViewSampleHelpAnnotation alloc] initWithCoordinate:location];
            annotation.DeatailsInformation = branch;
            [annotationsToAdd addObject:annotation];
            if ([[branch valueForKey:@"BranchTypeId"]intValue] ==1) {
                annotation.groupTag=OUTLETES;
            }
            else if ([[branch valueForKey:@"BranchTypeId"]intValue] ==2){
                annotation.groupTag = BRANCHES;
            }
            else if ([[branch valueForKey:@"BranchTypeId"]intValue] ==3){
                annotation.groupTag = LABORATORIES;
            }
        }
        
        [MapView addAnnotations:[annotationsToAdd allObjects]];
        MapView.clusteringMethod = OCClusteringMethodBubble;
        MapView.clusterByGroupTag = YES;
        [MapView doClustering];
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)HandleGetBranchesResult
{
    @try {
        [self.view setUserInteractionEnabled:YES];
        if ([BranchesList count]>0) {
            v_Search =(SearchView*)[[[NSBundle mainBundle]loadNibNamed:@"SearchView" owner:nil options:nil]objectAtIndex:0];
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
                [v_Search setFrame:CGRectMake(0, SearchHeaderView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - SearchHeaderView.frame.size.height )];
            }
            else{
                [v_Search setFrame:CGRectMake(SearchHeaderView.frame.origin.x, SearchHeaderView.frame.size.height-10, SearchHeaderView.frame.size.width, self.view.frame.size.height - SearchHeaderView.frame.size.height )];
            }
            
            [self.view addSubview:v_Search];
         
            v_Search.currentLocation =[[CLLocation alloc]initWithLatitude:[[[NSUserDefaults standardUserDefaults]objectForKey:@"lat" ] floatValue ] longitude:[[[NSUserDefaults standardUserDefaults]objectForKey:@"long"] floatValue ]];
            [v_Search setHidden:YES];
            v_Search.delegate=self;
            [v_Search reloadData];
            [self.view sendSubviewToBack:v_Search];
            [self.view bringSubviewToFront:SearchHeaderView];
            
            
            [self performSelectorInBackground:@selector(CashBranches) withObject:nil];
            //Do Map Culstring
            [self DoMapClustringWithData:BranchesList];
        }
        else{
             [self HideLoading];
            //dislay no connection message
            NSLog(@"no internet display message");
            [v_NoInternet setHidden:NO];
            [self.view bringSubviewToFront:v_NoInternet];
            
        }
      
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)getBranches
{
    @try {
        BranchesList = [[NSArray alloc]init];
        BranchesList =[Webservice GetBranches];
        [self performSelectorOnMainThread:@selector(HandleGetBranchesResult) withObject:nil waitUntilDone:NO];
        
    }
    @catch (NSException *exception) {
        
    }
    
}
- (void)HideLoading {
    [self.view setUserInteractionEnabled:YES];
    [LoadingIndicator stopAnimating];
    [LoadingView setHidden:YES];
    [self.view  sendSubviewToBack:LoadingView];
    
}
-(void)ShowLoadingView
{
    @try {
        [self.view setUserInteractionEnabled:NO];
        [CommonMethods  FormatView:LoadingView WithShadowRadius:5 CornerRadius:5 ShadowOpacity:0.8 BorderWidth:1 BorderColor:[UIColor whiteColor] ShadowColor:[UIColor whiteColor] andXPosition:3];
        LoadingLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:LoadingLabel.font.pointSize];
        [self.view bringSubviewToFront:LoadingView];
        [LoadingView setHidden:NO];
        [LoadingIndicator startAnimating];
      
    }
    @catch (NSException *exception) {
        
    }
    
}

-(void)NoInternetCancelButtonAction:(UIButton*)sender
{
    @try {
        [v_NoInternet setHidden:YES];
        [self.view sendSubviewToBack:v_NoInternet];
        
    }
    @catch (NSException *exception) {
        
    }
   
}

-(void)NoInternetRetryButtonAction:(UIButton*)sender
{
    @try {
        [v_NoInternet setHidden:YES];
        [self.view sendSubviewToBack:v_NoInternet];
        [self ShowLoadingView];
        [NSThread detachNewThreadSelector:@selector(getMainBranches) toTarget:self withObject:nil];
       
    }
    @catch (NSException *exception) {
        
    }
    
}

#pragma mark - Reachability
- (void)internetReachabilityChanged:(NSNotification *)note
{
    @try {
        NetworkStatus ns = [(Reachability *)[note object] currentReachabilityStatus];
    
        if (ns == NotReachable)
        {
            if (InternetConnection)
            {
                InternetConnection = NO;
                               //display error window
                NSLog(@"no internet");
                [self HideLoading];
                if ([[NSUserDefaults standardUserDefaults]objectForKey:BRANCHES_KEY]) {
                    int  munites = ceil([[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults]objectForKey:BRANCHES_KEY]]/60);
                    if (munites>180) {
                        NSLog(@"no internet display message");
                        [v_NoInternet.ButtonsContainer setHidden:NO];
                        [v_NoInternet.HideButton setHidden:YES];
                        
                        [v_NoInternet setHidden:NO];
                        [self.view bringSubviewToFront:v_NoInternet];
                    }
                    else{
                        NSLog(@"no internet display message");
                        [v_NoInternet.ButtonsContainer setHidden:YES];
                        [v_NoInternet.HideButton setHidden:NO];
                        
                        [v_NoInternet setHidden:NO];
                        [self.view bringSubviewToFront:v_NoInternet];
                    }
                }
                else{
                    
                    NSLog(@"no internet display message");
                    [v_NoInternet.ButtonsContainer setHidden:NO];
                    [v_NoInternet.HideButton setHidden:YES];
                    
                    [v_NoInternet setHidden:NO];
                    [self.view bringSubviewToFront:v_NoInternet];
                }
                
                if (FromCashedData==NO) {
                   

                }
                else{
                   
                    
                }
            }
        }
        else
        {
            
            //[alert dismissWithClickedButtonIndex:0 animated:YES];
           // [self NoInternetCancelButtonAction:nil];
            if (!InternetConnection)
            {
                InternetConnection = YES;
                
            }
        }
    }
    @catch (NSException *exception) {
        
    }
    
    
}
-(void)MapRecognizerAction
{
    @try {
        
        [self HideFilterView];
        [self HideDetailsView];
        showDetailsView=NO;
    }
    @catch (NSException *exception) {
        
    }
   
}
-(void)DismissKebord
{
    [SearchTextField resignFirstResponder];
}
- (void)viewDidLoad
{

    [super viewDidLoad];
    ShowSearchView =NO;
 
    ////////////////////////////////////////////////
    Reachability *internetReach = [Reachability reachabilityForInternetConnection];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(internetReachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    [internetReach startNotifier];
    if (internetReach.currentReachabilityStatus == NotReachable)
    {
        InternetConnection = NO;
        FromCashedData=YES;
    }
    else
    {
        InternetConnection = YES;

    }
    
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    UIBarButtonItem *finishItem = [[UIBarButtonItem alloc]initWithTitle:@"انهاء" style:UIBarButtonItemStylePlain target:self action:@selector(DismissKebord)];
    toolbar.items =[NSArray arrayWithObject:finishItem];
    SearchTextField.inputAccessoryView = toolbar;


    
    //handle gps
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    
    if([CLLocationManager locationServicesEnabled]){
        [locationManager startUpdatingLocation];
        InitialLocation = nil;
        NSLog(@"Location Services Enabled");
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        {
            [locationManager requestAlwaysAuthorization];

        }
        
                // Switch through the possible location
        // authorization states
        switch([CLLocationManager authorizationStatus])
        {
            case kCLAuthorizationStatusAuthorized:
            {
                InitialLocation = nil;
            }
                break;
                
            case kCLAuthorizationStatusDenied:
            {
                NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
                [defaults setValue:nil forKey:@"lat"];
                [defaults setValue:nil forKey:@"long"];
                [defaults synchronize];
                
                InitialLocation =[[NSMutableDictionary alloc]init];
                [InitialLocation setValue:@"24.610763" forKey:@"lat"];
                [InitialLocation setValue:@"46.719122" forKey:@"long"];
            }
                break;
            case kCLAuthorizationStatusRestricted:
            {
                NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
                [defaults setValue:nil forKey:@"lat"];
                [defaults setValue:nil forKey:@"long"];
                [defaults synchronize];
                
                InitialLocation =[[NSMutableDictionary alloc]init];
                [InitialLocation setValue:@"24.610763" forKey:@"lat"];
                [InitialLocation setValue:@"46.719122" forKey:@"long"];
                
                
            }
                break;
            case kCLAuthorizationStatusNotDetermined:
            {
                NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
                [defaults setValue:nil forKey:@"lat"];
                [defaults setValue:nil forKey:@"long"];
                [defaults synchronize];
                InitialLocation =[[NSMutableDictionary alloc]init];
                [InitialLocation setValue:@"24.610763" forKey:@"lat"];
                [InitialLocation setValue:@"46.719122" forKey:@"long"];
            }
                break;
                
        }
    }
    else{
        
        NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
        [defaults setValue:nil forKey:@"lat"];
        [defaults setValue:nil forKey:@"long"];
        [defaults synchronize];
        InitialLocation =[[NSMutableDictionary alloc]init];
        [InitialLocation setValue:@"24.610763" forKey:@"lat"];
        [InitialLocation setValue:@"46.719122" forKey:@"long"];
    }
   
    CLLocationCoordinate2D currentLocationCoordinates;
    if (InitialLocation) {
        currentLocationCoordinates.latitude = [[InitialLocation objectForKey:@"lat" ] floatValue ];
        currentLocationCoordinates.longitude = [[InitialLocation objectForKey:@"long"] floatValue ];
    }
    else{
        currentLocationCoordinates.latitude = [[[NSUserDefaults standardUserDefaults]objectForKey:@"lat" ] floatValue ];
        currentLocationCoordinates.longitude = [[[NSUserDefaults standardUserDefaults]objectForKey:@"long"] floatValue ];
    }
    
    MKCoordinateSpan span = MKCoordinateSpanMake(10, 10);
    MKCoordinateRegion region = {currentLocationCoordinates, span};
    [MapView setRegion:region animated:YES];
    [MapView setCenterCoordinate:currentLocationCoordinates animated:NO];

    //handle loading view
    LoadingIndicator = [[YLActivityIndicatorView alloc] initWithFrame:LoadingActivityControl.frame];
    LoadingIndicator.duration = .8f;
    LoadingIndicator.tag =10;
    [LoadingView addSubview:LoadingIndicator];
    
    //handle no internet message view
    v_NoInternet = (NoInternetMessageView*)[[[NSBundle mainBundle]loadNibNamed:@"NoInternetMessageView" owner:nil options:nil]objectAtIndex:0];
    [CommonMethods FormatView:v_NoInternet WithShadowRadius:1 CornerRadius:15 ShadowOpacity:.5 BorderWidth:1 BorderColor:[UIColor clearColor] ShadowColor:[UIColor blackColor] andXPosition:0];
    [v_NoInternet setFrame:CGRectMake((self.view.frame.size.width- v_NoInternet.frame.size.width)/2, (self.view.frame.size.height - v_NoInternet.frame.size.height)/2,v_NoInternet.frame.size.width, v_NoInternet.frame.size.height)];
    [v_NoInternet.HideButton addTarget:self action:@selector(NoInternetCancelButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [v_NoInternet.CancelButton addTarget:self action:@selector(NoInternetCancelButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [v_NoInternet.RetryButton addTarget:self action:@selector(NoInternetRetryButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:v_NoInternet];
    [v_NoInternet setHidden:YES];
    [self.view sendSubviewToBack:v_NoInternet];
    
    //handle filter view
    filterController = [[UIViewController alloc]init];
    filterView = (FilterView*)[[[NSBundle mainBundle]loadNibNamed:@"FilterView" owner:nil options:nil]objectAtIndex:0];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        filterPickerPopover = [[UIPopoverController alloc] initWithContentViewController:filterController];
        [filterView setFrame:CGRectMake(0, 0,filterView.frame.size.width, filterView.frame.size.height)];
        filterController.view.frame = CGRectMake(0, 0,filterView.frame.size.width, 400);
        filterController.view  = filterView;
        filterView.tag =FILTER_VIEW_TAG;
        filterView.delegate=self;
        filterPickerPopover.popoverContentSize = CGSizeMake(filterView.frame.size.width, 460);
        filterPickerPopover.delegate= self
        ;
    }

    


    showDetailsView =NO;
    @try {
        
        [[UIApplication sharedApplication]setStatusBarHidden:NO];
        if ([[NSUserDefaults standardUserDefaults]objectForKey:BRANCHES_KEY]) {
            int  munites = ceil([[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults]objectForKey:BRANCHES_KEY]]/60);
            if (munites>180) {
                if (InternetConnection) {
                     FromCashedData=NO;
                    NSLog(@"get branches  from internet");
                    [self ShowLoadingView];
                    [NSThread detachNewThreadSelector:@selector(getMainBranches) toTarget:self withObject:nil];
                }
                else{
                    //dislay no connection message
                    NSLog(@"no internet display message");
                    [v_NoInternet setHidden:NO];
                    [self.view bringSubviewToFront:v_NoInternet];
                }
            }
            else {
//
//                if (!InternetConnection) {
//                    NSLog(@"no internet display message");
//                    [v_NoInternet.ButtonsContainer setHidden:YES];
//                    [v_NoInternet.HideButton setHidden:NO];
//                    [v_NoInternet setHidden:NO];
//                    [self.view bringSubviewToFront:v_NoInternet];
//                }
//      

                FromCashedData=YES;
                NSLog(@"get branches  from cash");
                MainBranches = [self GetCashedMainBranches];
                BranchesList = [self GetCashedBranches];
                //Do Map Culstring
                [self DoMapClustringWithData:BranchesList];
                v_Search =(SearchView*)[[[NSBundle mainBundle]loadNibNamed:@"SearchView" owner:nil options:nil]objectAtIndex:0];
                if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
                    [v_Search setFrame:CGRectMake(0, SearchHeaderView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - SearchHeaderView.frame.size.height )];
                }
                else{
                    [v_Search setFrame:CGRectMake(SearchHeaderView.frame.origin.x, SearchHeaderView.frame.size.height-10, SearchHeaderView.frame.size.width, self.view.frame.size.height - SearchHeaderView.frame.size.height )];
                }
                [self.view addSubview:v_Search];
                
                v_Search.currentLocation =[[CLLocation alloc]initWithLatitude:[[[NSUserDefaults standardUserDefaults]objectForKey:@"lat" ] floatValue ] longitude:[[[NSUserDefaults standardUserDefaults]objectForKey:@"long"] floatValue ]];
                [v_Search setHidden:YES];
                 v_Search.delegate=self;
                [v_Search reloadData];
                [self.view sendSubviewToBack:v_Search];
                [self.view bringSubviewToFront:SearchHeaderView];
            }

        }
        else{
            if (InternetConnection) {
                FromCashedData=NO;
                NSLog(@"get branches  from internet");
                [self ShowLoadingView];
               [NSThread detachNewThreadSelector:@selector(getMainBranches) toTarget:self withObject:nil];
              
            }
            else{
                //dislay no connection message
                NSLog(@"no internet display message");
                [v_NoInternet setHidden:NO];
                [self.view bringSubviewToFront:v_NoInternet];
            }
        }

        MapView.delegate = self;
        CancelSearchButton.titleLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:CancelSearchButton.titleLabel.font.pointSize];
        ZoomLevel = 20;
        
        UITapGestureRecognizer *branchRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(MapRecognizerAction)];
        branchRecognizer.numberOfTapsRequired=1;
        [MapView addGestureRecognizer:branchRecognizer];
        
     
       //handle branch details view
        v_branchDetails =(BranchDetailsView*)[[[NSBundle mainBundle]loadNibNamed:@"BranchDetailsView" owner:nil options:nil]objectAtIndex:0];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            [v_branchDetails setFrame:CGRectMake((self.view.frame.size.width- v_branchDetails.frame.size.width)/2, self.view.frame.size.height,v_branchDetails.frame.size.width, v_branchDetails.frame.size.height)];
        }
        else{
            [v_branchDetails setFrame:CGRectMake(0, self.view.frame.size.height, v_branchDetails.frame.size.width, v_branchDetails.frame.size.height)];
        }
        v_branchDetails.ContainerViewController=self;
        [self.view addSubview:v_branchDetails];
        [v_branchDetails.ArrowButton addTarget:self action:@selector(DetailMenuButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [v_branchDetails.DirectionsButton addTarget:self action:@selector(DirectionButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationController.viewControllers = [NSArray arrayWithObject:self];
        //[self performSelector:@selector(ZoomToUserLocation) withObject:nil afterDelay:0.5];
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)ZoomToUserLocation
{
    MKUserLocation *userLocation = MapView.userLocation;
    MKCoordinateRegion region =
    MKCoordinateRegionMakeWithDistance (userLocation.location.coordinate,10, 10);
    [MapView setRegion:region animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)ToggleFilterView {
    @try {

        [CommonMethods FormatView:[self.view viewWithTag:FILTER_VIEW_TAG] WithShadowRadius:1 CornerRadius:15 ShadowOpacity:.5 BorderWidth:1 BorderColor:[UIColor clearColor] ShadowColor:[UIColor blackColor] andXPosition:0];
        
        if ([self.view viewWithTag:FILTER_VIEW_TAG].hidden) {
            
            [FilterButton setImage:[UIImage imageNamed:@"filterbtnhove.png"] forState:UIControlStateNormal];
            [FilterButton setImage:[UIImage imageNamed:@"filterbtnhove.png"] forState:UIControlStateHighlighted];
            [[self.view viewWithTag:FILTER_VIEW_TAG] setHidden:NO];
            [[self.view viewWithTag:FILTER_VIEW_TAG] setFrame:CGRectMake(0, 0, self.view.frame.size.width, [self.view viewWithTag:FILTER_VIEW_TAG].frame.size.height)];
            [UIView animateWithDuration:0.3 animations:^{
                [[self.view viewWithTag:FILTER_VIEW_TAG] setFrame:CGRectMake(0, HeaderView.frame.size.height+20, self.view.frame.size.width, [self.view viewWithTag:FILTER_VIEW_TAG].frame.size.height)];
            }completion:^(BOOL finished) {
                if (finished) {
                    [UIView animateWithDuration:0.3 animations:^{
                        [[self.view viewWithTag:FILTER_VIEW_TAG] setFrame:CGRectMake(0, HeaderView.frame.size.height, self.view.frame.size.width, [self.view viewWithTag:FILTER_VIEW_TAG].frame.size.height)];
                    }];
                }
            }];
        }
        else{
        
            [FilterButton setImage:[UIImage imageNamed:@"filter.png"] forState:UIControlStateNormal];
            [FilterButton setImage:[UIImage imageNamed:@"filter.png"] forState:UIControlStateHighlighted];
             [MapView setUserInteractionEnabled:YES];
            [MapView setScrollEnabled:YES];
            [UIView animateWithDuration:0.3 animations:^{
                [[self.view viewWithTag:FILTER_VIEW_TAG] setFrame:CGRectMake(0,-[self.view viewWithTag:FILTER_VIEW_TAG].frame.size.height - HeaderView.frame.size.height, self.view.frame.size.width, [self.view viewWithTag:FILTER_VIEW_TAG].frame.size.height)];
                
            } completion:^(BOOL finished) {
                if (finished) {
                    [[self.view viewWithTag:FILTER_VIEW_TAG] setHidden:YES];
                }
            }];
        }
        
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)MapButtonAction:(UIButton *)sender {
    if (sender.tag==MAP_VIEW_TAG ) {
        MapView.mapType=MKMapTypeSatellite;
        sender.tag=SETTLITE_VIEW_TAG;
        
        [sender setImage:[UIImage imageNamed:@"map_ic"] forState:UIControlStateNormal];
        [sender setImage:[UIImage imageNamed:@"map_ic"] forState:UIControlStateHighlighted];
        [sender setImage:[UIImage imageNamed:@"map_ic"] forState:UIControlStateSelected];
    }
    else if (SETTLITE_VIEW_TAG){
        MapView.mapType=MKMapTypeStandard;
        sender.tag=MAP_VIEW_TAG;
        [sender setImage:[UIImage imageNamed:@"satetlite_ic"] forState:UIControlStateNormal];
        [sender setImage:[UIImage imageNamed:@"satetlite_ic"] forState:UIControlStateHighlighted];
        [sender setImage:[UIImage imageNamed:@"satetlite_ic"] forState:UIControlStateSelected];
    }
}

- (IBAction)CancelSearchAction:(UIButton *)sender {
    @try {
        [SearchButton setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
        [SearchButton setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateHighlighted];
        
        ShowSearchView=NO;
        [MapView setUserInteractionEnabled:YES];
        [MapView setScrollEnabled:YES];
        [LogoImageView setHidden:NO];
        [SearchTextField resignFirstResponder];
        [SearchHeaderView setHidden:YES];
        [self.view sendSubviewToBack:SearchHeaderView];
        [v_Search setHidden:YES];
        [self.view sendSubviewToBack:v_Search];
 
    }
    @catch (NSException *exception) {
        
    }
}
- (void)HideFilterView {
    @try {
        [MapView setScrollEnabled:YES];
        [MapView setUserInteractionEnabled:YES];
        [FilterButton setImage:[UIImage imageNamed:@"filter.png"] forState:UIControlStateNormal];
        [FilterButton setImage:[UIImage imageNamed:@"filter.png"] forState:UIControlStateHighlighted];
        if ([self.view viewWithTag:FILTER_VIEW_TAG]) {
            [UIView animateWithDuration:0.3 animations:^{
                [[self.view viewWithTag:FILTER_VIEW_TAG] setFrame:CGRectMake(0,-[self.view viewWithTag:FILTER_VIEW_TAG].frame.size.height - HeaderView.frame.size.height, self.view.frame.size.width, [self.view viewWithTag:FILTER_VIEW_TAG].frame.size.height)];
                
            } completion:^(BOOL finished) {
                if (finished) {
                    [[self.view viewWithTag:FILTER_VIEW_TAG] setHidden:YES];
                }
            }];
        }
 
    }
    @catch (NSException *exception) {
        
    }
  
}

- (IBAction)SearchAction:(UIButton *)sender {
    @try {
        [SearchButton setImage:[UIImage imageNamed:@"searchbtnhover.png"] forState:UIControlStateNormal];
        [SearchButton setImage:[UIImage imageNamed:@"searchbtnhover.png"] forState:UIControlStateHighlighted];
        ShowSearchView=YES;
         [self HideDetailsView];
         SearchTextField.text =@"";
         [v_Search reloadData];
         [MapView setUserInteractionEnabled:NO];
        [MapView setScrollEnabled:NO];
        [self HideFilterView];
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
            [SearchHeaderView setHidden:NO];
            [SearchHeaderView setAlpha:0];
            [v_Search setHidden:NO];
            [v_Search setAlpha:0];

            [self.view bringSubviewToFront:v_Search];
            [self.view bringSubviewToFront:SearchHeaderView];
            [UIView animateWithDuration:0.3 animations:^{
                [SearchHeaderView setAlpha:1];
                [v_Search setAlpha:0.9];
            }];
            
        }
        else{
            [v_Search setHidden:NO];
            [SearchHeaderView setHidden:NO];
            [LogoImageView setHidden:YES];
            [v_Search setAlpha:0];
            [self.view bringSubviewToFront:v_Search];
            [SearchHeaderView setAlpha:0];
            [self.view bringSubviewToFront:SearchHeaderView];
            [UIView animateWithDuration:0.3 animations:^{
                [SearchHeaderView setAlpha:1];
                [v_Search setAlpha:0.9];
            }];
        }
    }
    @catch (NSException *exception) {
        
    }
    
}

- (IBAction)FilterButtonAction:(UIButton *)sender {
    @try {
        [self HideDetailsView];
        [self CancelSearchAction:nil];
        ShowSearchView=NO;
       
        [sender setImage:[UIImage imageNamed:@"filterbtnhove.png"] forState:UIControlStateNormal];
        [sender setImage:[UIImage imageNamed:@"filterbtnhove.png"] forState:UIControlStateHighlighted];
       
       
        [MapView setScrollEnabled:NO];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            if (sender.tag==1) {
                [sender setImage:[UIImage imageNamed:@"filter.png"] forState:UIControlStateNormal];
                [sender setImage:[UIImage imageNamed:@"filter.png"] forState:UIControlStateHighlighted];
                  sender.tag=0;
                [MapView setUserInteractionEnabled:YES];
                [MapView setScrollEnabled:YES];
                [filterPickerPopover dismissPopoverAnimated:YES];
               
            }
            else{
                [filterPickerPopover  presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
                sender.tag=1;
                return;
            }

        }
        else{
            if ([self.view viewWithTag:FILTER_VIEW_TAG]) {
                [self ToggleFilterView];
            }
            else{
                
                [CommonMethods FormatView:filterView  WithShadowRadius:1 CornerRadius:15 ShadowOpacity:.5 BorderWidth:0 BorderColor:[UIColor clearColor] ShadowColor:[UIColor blackColor] andXPosition:0];
                [filterView setFrame:CGRectMake(0, 0, self.view.frame.size.width, filterView.frame.size.height)];
                [self.view addSubview:filterView];
                filterView.tag =FILTER_VIEW_TAG;
                filterView.delegate=self;
                [UIView animateWithDuration:0.3 animations:^{
                    [filterView setFrame:CGRectMake(0, HeaderView.frame.size.height+20, self.view.frame.size.width, filterView.frame.size.height)];
                }completion:^(BOOL finished) {
                    if (finished) {
                        [UIView animateWithDuration:0.2 animations:^{
                            [filterView setFrame:CGRectMake(0, HeaderView.frame.size.height, self.view.frame.size.width, filterView.frame.size.height)];
                        }];
                    }
                }];
            }

        }
        
       [self.view bringSubviewToFront:HeaderView];

    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)MyLocationButtonAction:(UIButton *)sender {
    @try {
        CLLocationCoordinate2D currentLocationCoordinates;
        if (InitialLocation) {
            currentLocationCoordinates.latitude = [[InitialLocation objectForKey:@"lat" ] floatValue ];
            currentLocationCoordinates.longitude = [[InitialLocation objectForKey:@"long"] floatValue ];
        }
        else{
        currentLocationCoordinates.latitude = [[[NSUserDefaults standardUserDefaults]objectForKey:@"lat" ] floatValue ];
        currentLocationCoordinates.longitude = [[[NSUserDefaults standardUserDefaults]objectForKey:@"long"] floatValue ];
        }
        MKCoordinateSpan span = MKCoordinateSpanMake(3, 3);
        MKCoordinateRegion region = {currentLocationCoordinates, span};
        [MapView setRegion:region animated:YES];
        
    }
    @catch (NSException *exception) {
        
    }
}

- (void)HideDetailsView {
    @try {
        if (showDetailsView==NO) {
            return;
        }
        for (id anotation in MapView.annotations) {
            if ([[MapView viewForAnnotation:anotation] isKindOfClass:[CustomAnnotationView class]]) {
                [[[MapView viewForAnnotation:anotation] viewWithTag:10000]removeFromSuperview];
            }
        }
        
        [[SelectedAnnotation viewWithTag:10000]removeFromSuperview];
        [UIView animateWithDuration:0.3 animations:^{
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
                [v_branchDetails setFrame:CGRectMake((self.view.frame.size.width- v_branchDetails.frame.size.width)/2, self.view.frame.size.height ,v_branchDetails.frame.size.width, v_branchDetails.frame.size.height)];
            }
            else{
                [MapView setFrame:CGRectMake(0, HeaderView.frame.size.height, MapView.frame.size.width, self.view.frame.size.height -HeaderView.frame.size.height )];
                [v_branchDetails setFrame:CGRectMake(0, self.view.frame.size.height , self.view.frame.size.width, v_branchDetails.frame.size.height)];
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }
    
}

- (IBAction)DetailMenuButtonAction:(UIButton *)sender {
    @try {
        showDetailsView =NO;
        for (id anotation in MapView.annotations) {
            if ([[MapView viewForAnnotation:anotation] isKindOfClass:[CustomAnnotationView class]]) {
                [[[MapView viewForAnnotation:anotation] viewWithTag:10000]removeFromSuperview];
            }
        }
        
        for (id anotation in MapView.annotations) {
            if ([[MapView viewForAnnotation:anotation] isKindOfClass:[CustomAnnotationView class]]) {
                [[[MapView viewForAnnotation:anotation] viewWithTag:10000]removeFromSuperview];
            }
        }
        
        [[SelectedAnnotation viewWithTag:10000]removeFromSuperview];
        [UIView animateWithDuration:0.3 animations:^{
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
                [v_branchDetails setFrame:CGRectMake((self.view.frame.size.width- v_branchDetails.frame.size.width)/2, self.view.frame.size.height ,v_branchDetails.frame.size.width, v_branchDetails.frame.size.height)];
            }
            else{
                [MapView setFrame:CGRectMake(0, HeaderView.frame.size.height, MapView.frame.size.width, self.view.frame.size.height -HeaderView.frame.size.height )];
                [v_branchDetails setFrame:CGRectMake(0, self.view.frame.size.height , self.view.frame.size.width, v_branchDetails.frame.size.height)];
            }
        }];
        
        
        
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)DirectionButtonAction:(UIButton *)sender {
    @try {
        NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
        if ( [defaults objectForKey:@"lat"]==nil) {
            UIAlertView *gpsAlert = [[UIAlertView alloc]initWithTitle:@"" message:@"(GPS) برجاء تفعيل خدمة تحديد المواقع " delegate:self cancelButtonTitle:@"الغاء" otherButtonTitles:nil, nil];
            [gpsAlert show];
            return;
        }
        if (!InternetConnection) {
            UIAlertView *gpsAlert = [[UIAlertView alloc]initWithTitle:@"" message:@"برجاء الاتصال بالانترنت" delegate:self cancelButtonTitle:@"الغاء" otherButtonTitles:nil, nil];
            [gpsAlert show];
            return;
        }
        UIStoryboard *countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone"
                                                                    bundle:nil];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad"
                                                          bundle:nil];
        }
        RoutingViewController *vc_routing =[countryStoryboard  instantiateViewControllerWithIdentifier:@"RoutingViewController"];
        vc_routing.SelectedBranch = SelectedAnnotation.DeatailsInformation;
        [self.navigationController pushViewController:vc_routing animated:YES];
        [self HideDetailsView];
    }
    @catch (NSException *exception) {
        
    }
    
}

-(void)DisplayBranchDetails:(UITapGestureRecognizer*)recognizer
{
    showDetailsView =YES;
    @try {
        
        for (id anotation in MapView.annotations) {
            if ([[MapView viewForAnnotation:anotation] isKindOfClass:[CustomAnnotationView class]]) {
                [[[MapView viewForAnnotation:anotation] viewWithTag:10000]removeFromSuperview];
            }
        }
        
        SelectedAnnotation =  (CustomAnnotationView*) recognizer.view;
        UIImageView *pulseRingImg = [[UIImageView alloc] initWithFrame:SelectedAnnotation.bounds];
        
        if ([[SelectedAnnotation.DeatailsInformation objectForKey:@"BranchTypeId"]intValue]==[OUTLETES intValue]) {
            pulseRingImg.image = [UIImage imageNamed:@"branchcircle"];
        }
        if ([[SelectedAnnotation.DeatailsInformation objectForKey:@"BranchTypeId"]intValue ]==[BRANCHES intValue]) {
            pulseRingImg.image = [UIImage imageNamed:@"storescircle"];
        }
        if ([[SelectedAnnotation.DeatailsInformation objectForKey:@"BranchTypeId"] intValue ]==[LABORATORIES intValue]) {
            pulseRingImg.image = [UIImage imageNamed:@"labcircle"];
        }
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([[SelectedAnnotation.DeatailsInformation objectForKey:@"Latitude"] floatValue ], [[SelectedAnnotation.DeatailsInformation objectForKey:@"Longitude"] floatValue ]);
        
        
        [MapView setCenterCoordinate:coord animated:NO];
        
        pulseRingImg.userInteractionEnabled = NO;
        CABasicAnimation *theAnimation;
        theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.scale.xy"];
        theAnimation.duration=1.0;
        theAnimation.repeatCount=HUGE_VALF;
        theAnimation.autoreverses=NO;
        pulseRingImg.alpha=0;
        theAnimation.fromValue=[NSNumber numberWithFloat:0.0];
        theAnimation.toValue=[NSNumber numberWithFloat:1.5];
        pulseRingImg.alpha = 1;
        pulseRingImg.tag=10000;
        [pulseRingImg.layer addAnimation:theAnimation forKey:@"pulse"];
        pulseRingImg.userInteractionEnabled = NO;
        
        [[SelectedAnnotation viewWithTag:10000]removeFromSuperview];
        [SelectedAnnotation insertSubview:pulseRingImg atIndex:0];
        
        v_branchDetails.BranchID =[SelectedAnnotation.DeatailsInformation objectForKey:@"Id"];
        [v_branchDetails ReloadBranchData];
        NSLog(@"details : %@", SelectedAnnotation.DeatailsInformation);
        [UIView animateWithDuration:0.3 animations:^{
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
                [v_branchDetails setFrame:CGRectMake((self.view.frame.size.width- v_branchDetails.frame.size.width)/2, self.view.frame.size.height -v_branchDetails.frame.size.height,v_branchDetails.frame.size.width, v_branchDetails.frame.size.height)];
                CLLocationCoordinate2D location = [MapView convertPoint:CGPointMake(MapView.center.x,v_branchDetails.frame.origin.y + 200) toCoordinateFromView:MapView];
                [MapView  setCenterCoordinate:location animated:YES];
            }
            else{
                [v_branchDetails setFrame:CGRectMake(0, self.view.frame.size.height -v_branchDetails.frame.size.height,v_branchDetails.frame.size.width, v_branchDetails.frame.size.height)];
                 [MapView setFrame:CGRectMake(0, MapView.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height -v_branchDetails.frame.size.height-HeaderView.frame.size.height)];
            }
            
        }];
        
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)ZoomToSubAnnotations:(UITapGestureRecognizer*)recognizer
{
    @try {
        for (id anotation in MapView.annotations) {
            if ([[MapView viewForAnnotation:anotation] isKindOfClass:[CustomAnnotationView class]]) {
                [[[MapView viewForAnnotation:anotation] viewWithTag:10000]removeFromSuperview];
            }
        }
        
        CustomAnnotationView *currentAnnotation =(CustomAnnotationView*)recognizer.view;
        ZoomLevel = [currentAnnotation getClusterText];
        if (MapView.region.span.latitudeDelta>0) {
            if (ZoomLevel>MapView.region.span.latitudeDelta) {
                ZoomLevel=MapView.region.span.latitudeDelta;
            }
        }
       
  
        ZoomLevel=ZoomLevel<2?ZoomLevel-0.89:ZoomLevel-(ZoomLevel/2);
        if (ZoomLevel<=0) {
            ZoomLevel=0.013;
        }
        CGPoint pt = recognizer.view.center;
        CLLocationCoordinate2D coord= [MapView convertPoint:pt toCoordinateFromView:MapView];
        MKCoordinateSpan span = MKCoordinateSpanMake(ZoomLevel, ZoomLevel);
        MKCoordinateRegion region = {coord, span};
        
        [MapView setRegion:region animated:YES];
       
        
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma mark - map delegate
-(void)mapView:(MKMapView *)aMapView regionWillChangeAnimated:(BOOL)animated
{
    if (ShowSearchView==YES) {
        [MapView setScrollEnabled:NO];
        [MapView setUserInteractionEnabled:NO];
    }
    else{
        [MapView setScrollEnabled:YES];
        [MapView setUserInteractionEnabled:YES];
    }
    NSLog(@"lat: %f & long: %f",aMapView.region.span.latitudeDelta,aMapView.region.span.longitudeDelta);
    [MapView doClustering];
}
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKAnnotationView *userLocationView = [mapView viewForAnnotation:userLocation];
    userLocationView.canShowCallout = NO;
}
- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    CustomAnnotationView *annotationView;
    @try {
        // if it's a cluster
        if ([annotation isKindOfClass:[OCAnnotation class]]) {
            OCAnnotation *clusterAnnotation = (OCAnnotation *)annotation;
            annotationView = (CustomAnnotationView *)[aMapView dequeueReusableAnnotationViewWithIdentifier:@"ClusterView"];
            if (!annotationView) {
                annotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"ClusterView"];
                annotationView.canShowCallout = NO;
                if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
                     annotationView.centerOffset = CGPointMake(20,40);
                }
                else{
                     annotationView.centerOffset = CGPointMake(0, -20);
                }

            }
            // set title
            [annotationView setClusterText:  [NSString stringWithFormat:@" %zd", [clusterAnnotation.annotationsInCluster count]]];
            
            // change pin image for group
            if (MapView.clusterByGroupTag) {
                if ([clusterAnnotation.groupTag isEqualToString:OUTLETES]) {
                    annotationView.image = [UIImage imageNamed:@"mapbranchcomplexic.png"];
                }
                else if([clusterAnnotation.groupTag isEqualToString:BRANCHES]){
                    annotationView.image = [UIImage imageNamed:@"mapstorecomplexic.png"];
                }
                else if([clusterAnnotation.groupTag isEqualToString:LABORATORIES]){
                    annotationView.image = [UIImage imageNamed:@"maplabcomplexbtn.png"];
                }
                clusterAnnotation.title = clusterAnnotation.groupTag;
            }
        }
        
        if([annotation isKindOfClass:[OCAnnotation class]]){
            UITapGestureRecognizer *branchRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ZoomToSubAnnotations:)];
            branchRecognizer.numberOfTapsRequired=1;
            [annotationView addGestureRecognizer:branchRecognizer];
        }
        
        
        // If it's a single annotation
        if([annotation isKindOfClass:[OCMapViewSampleHelpAnnotation class]]){
            
            OCMapViewSampleHelpAnnotation *singleAnnotation = (OCMapViewSampleHelpAnnotation *)annotation;
            annotationView = (CustomAnnotationView *)[aMapView dequeueReusableAnnotationViewWithIdentifier:@"singleAnnotationView"];
            annotationView.tag =SINGLE_ANNOTATION_TAG;
            if (!annotationView) {
                annotationView = [[CustomAnnotationView alloc] initWithAnnotation:singleAnnotation reuseIdentifier:@"singleAnnotationView"];
                annotationView.centerOffset = CGPointMake(0, -20);
            }
            singleAnnotation.title = singleAnnotation.groupTag;
            
            if ([singleAnnotation.groupTag isEqualToString:OUTLETES]) {
                
                annotationView.image = [UIImage imageNamed:@"mapbranchic.png"];
            }
            else if([singleAnnotation.groupTag isEqualToString:BRANCHES]){
                
                annotationView.image = [UIImage imageNamed:@"mapstoresic.png"];
            }
            else if([singleAnnotation.groupTag isEqualToString:LABORATORIES]){
                
                annotationView.image = [UIImage imageNamed:@"maplabic.png"];
            }
            
            UITapGestureRecognizer *branchRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(DisplayBranchDetails:)];
            branchRecognizer.numberOfTapsRequired=1;
            annotationView.DeatailsInformation = singleAnnotation.DeatailsInformation;
            [annotationView addGestureRecognizer:branchRecognizer];
            
            
        }
        // Error
        else{
            
        }
        
        CABasicAnimation *theAnimation;
        theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.scale.xy"];
        theAnimation.duration=0.4;
        theAnimation.autoreverses=NO;
        annotationView.alpha=0;
        theAnimation.fromValue=[NSNumber numberWithFloat:0.0];
        theAnimation.toValue=[NSNumber numberWithFloat:1.0];
        annotationView.alpha = 1;
        [annotationView.layer addAnimation:theAnimation forKey:@"pulse"];
        
    }
    @catch (NSException *exception) {
        
    }
    
    return annotationView;
}

- (void)mapView:(MKMapView *)aMapView regionDidChangeAnimated:(BOOL)animated
{
   
}

#pragma -mark uitextfield delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
      [textField resignFirstResponder];
       if ([textField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
           [[[UIAlertView alloc]initWithTitle:nil message:@"برجاء ادخال اسم الفرع الرئيسى" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil, nil]show];
            return YES ;
      }
    
        UIStoryboard *countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone"
                                                                    bundle:nil];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad"
                                                          bundle:nil];
        }
    
        [SearchTextField resignFirstResponder];
        [SearchHeaderView setHidden:YES];
        [self.view sendSubviewToBack:SearchHeaderView];
        [v_Search setHidden:YES];
        [self.view sendSubviewToBack:v_Search];
    
        SearchResultsViewController *vc_searchResults=[countryStoryboard  instantiateViewControllerWithIdentifier:@"SearchResultsViewController"];
        vc_searchResults.SearchedText = [SearchTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        vc_searchResults.delegate=self;
        [[SlideNavigationController sharedInstance] pushViewController:vc_searchResults animated:YES];
   
    return YES;
}

#pragma -mark location manager delegate
-(void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager
{
    
}
-(void)locationManagerDidResumeLocationUpdates:(CLLocationManager *)manager
{
    
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    NSString *_lat,*_long;
    
    if (currentLocation != nil) {
        _long= [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        _lat= [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        selectedLocation =
        [[CLLocation alloc] initWithLatitude:currentLocation.coordinate.latitude
                                   longitude:currentLocation.coordinate.longitude];
        NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
        [defaults setValue:_lat forKey:@"lat"];
        [defaults setValue:_long forKey:@"long"];
        [defaults synchronize];
        
        [locationManager stopUpdatingLocation];
        CLLocationCoordinate2D currentLocationCoordinates;
        if (InitialLocation) {
            currentLocationCoordinates.latitude = [[InitialLocation objectForKey:@"lat" ] floatValue ];
            currentLocationCoordinates.longitude = [[InitialLocation objectForKey:@"long"] floatValue ];
        }
        else{
            currentLocationCoordinates.latitude = [[[NSUserDefaults standardUserDefaults]objectForKey:@"lat" ] floatValue ];
            currentLocationCoordinates.longitude = [[[NSUserDefaults standardUserDefaults]objectForKey:@"long"] floatValue ];
        }
        
        MKCoordinateSpan span = MKCoordinateSpanMake(10, 10);
        MKCoordinateRegion region = {currentLocationCoordinates, span};
        [MapView setRegion:region animated:YES];
        [MapView setCenterCoordinate:currentLocationCoordinates animated:NO];

    }
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"didFailWithError: %@", error);
    
    InitialLocation =[[NSMutableDictionary alloc]init];
    [InitialLocation setValue:@"24.610763" forKey:@"lat"];
    [InitialLocation setValue:@"46.719122" forKey:@"long"];
    
    CLLocationCoordinate2D currentLocationCoordinates;
    if (InitialLocation) {
        currentLocationCoordinates.latitude = [[InitialLocation objectForKey:@"lat" ] floatValue ];
        currentLocationCoordinates.longitude = [[InitialLocation objectForKey:@"long"] floatValue ];
    }
    else{
        currentLocationCoordinates.latitude = [[[NSUserDefaults standardUserDefaults]objectForKey:@"lat" ] floatValue ];
        currentLocationCoordinates.longitude = [[[NSUserDefaults standardUserDefaults]objectForKey:@"long"] floatValue ];
    }
    
    MKCoordinateSpan span = MKCoordinateSpanMake(10, 10);
    MKCoordinateRegion region = {currentLocationCoordinates, span};
    [MapView setRegion:region animated:YES];
    [MapView setCenterCoordinate:currentLocationCoordinates animated:NO];

}
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    @try {
        [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"app_installed"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        if([CLLocationManager locationServicesEnabled]){
            
            NSLog(@"Location Services Enabled");
            
            // Switch through the possible location
            // authorization states
            switch([CLLocationManager authorizationStatus]){
                case kCLAuthorizationStatusAuthorized:
                {
                    InitialLocation = nil;
                    
                    locationManager.delegate = self;
                    [locationManager stopUpdatingLocation];
                    [locationManager startUpdatingLocation];
                }
                    break;
                    
                case kCLAuthorizationStatusDenied:
                {
                    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
                    [defaults setValue:nil forKey:@"lat"];
                    [defaults setValue:nil forKey:@"long"];
                    [defaults synchronize];
                    
                    InitialLocation =[[NSMutableDictionary alloc]init];
                    [InitialLocation setValue:@"24.610763" forKey:@"lat"];
                    [InitialLocation setValue:@"46.719122" forKey:@"long"];
                    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"app_installed"]) {
                        UIAlertView *gpsAlert = [[UIAlertView alloc]initWithTitle:@"" message:@"(GPS) برجاء تفعيل خدمة تحديد المواقع " delegate:self cancelButtonTitle:@"الغاء" otherButtonTitles:nil, nil];
                        [gpsAlert show];
                        
                    }
                }
                    break;
                case kCLAuthorizationStatusRestricted:
                {
                    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
                    [defaults setValue:nil forKey:@"lat"];
                    [defaults setValue:nil forKey:@"long"];
                    [defaults synchronize];
                    
                    InitialLocation =[[NSMutableDictionary alloc]init];
                    [InitialLocation setValue:@"24.610763" forKey:@"lat"];
                    [InitialLocation setValue:@"46.719122" forKey:@"long"];
                    
                    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"app_installed"]) {
                        UIAlertView *gpsAlert = [[UIAlertView alloc]initWithTitle:@"" message:@"(GPS) برجاء تفعيل خدمة تحديد المواقع " delegate:self cancelButtonTitle:@"الغاء" otherButtonTitles:nil, nil];
                        [gpsAlert show];
                        
                    }
                }
                    break;
                case kCLAuthorizationStatusNotDetermined:
                {
                    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
                    [defaults setValue:nil forKey:@"lat"];
                    [defaults setValue:nil forKey:@"long"];
                    [defaults synchronize];
                    InitialLocation =[[NSMutableDictionary alloc]init];
                    [InitialLocation setValue:@"24.610763" forKey:@"lat"];
                    [InitialLocation setValue:@"46.719122" forKey:@"long"];
                }
                    break;
                    
            }
        }
        else{
            
            NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
            [defaults setValue:nil forKey:@"lat"];
            [defaults setValue:nil forKey:@"long"];
            [defaults synchronize];
            InitialLocation =[[NSMutableDictionary alloc]init];
            [InitialLocation setValue:@"24.610763" forKey:@"lat"];
            [InitialLocation setValue:@"46.719122" forKey:@"long"];
            if ([[NSUserDefaults standardUserDefaults]objectForKey:@"app_installed"]) {
                UIAlertView *gpsAlert = [[UIAlertView alloc]initWithTitle:@"" message:@"(GPS) برجاء تفعيل خدمة تحديد المواقع " delegate:self cancelButtonTitle:@"الغاء" otherButtonTitles:nil, nil];
                [gpsAlert show];
                
            }
            
        }
        
       
        
    }
    @catch (NSException *exception) {
        
    }
    
}
@end
