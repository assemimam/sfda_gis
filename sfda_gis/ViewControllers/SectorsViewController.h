//
//  SectorsViewController.h
//  SFDA_GIS
//
//  Created by assem on 9/4/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"
@interface SectorsViewController : MasterViewController<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UIButton *IconTitleButton;
    __weak IBOutlet UIView *NoResultView;
    __weak IBOutlet UILabel *NoResultLabel;
    __weak IBOutlet UITableView *BranchesTableView;
    __weak IBOutlet UIButton *BackButton;
    __weak IBOutlet UILabel *TitleLabel;
}
- (IBAction)CloseAction:(UIButton *)sender;

@property(nonatomic)SECTOR_TYPE SectorType;

@end
