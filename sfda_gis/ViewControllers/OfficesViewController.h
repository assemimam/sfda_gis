//
//  OfficesViewController.h
//  SFDA_GIS
//
//  Created by Assem Imam on 8/19/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"

@interface OfficesViewController : MasterViewController<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UIButton *IconTitlButton;
    __weak IBOutlet UILabel *NoresultLabel;
    __weak IBOutlet UIView *NoResultView;
    __weak IBOutlet UITableView *OfficesTableView;
    __weak IBOutlet UIButton *BackButton;
    __weak IBOutlet UILabel *TitleLabel;
}
- (IBAction)CloseAction:(UIButton *)sender;
@property(nonatomic)BRANCH_TYPE BranchType;
@end
