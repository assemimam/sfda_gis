//
//  SearchResultsViewController.h
//  sfda_gis
//
//  Created by assem on 9/16/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol SearchResultsDelegate <NSObject>
@optional
-(void)didClickBackButton;

@end
#import <UIKit/UIKit.h>
#import "MasterViewController.h"
@interface SearchResultsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    
    __weak IBOutlet UILabel *NoResultLabel;
    __weak IBOutlet UIView *NoResultView;
    __weak IBOutlet UILabel *TitleLabel;
    __weak IBOutlet UIButton *BackButton;
    __weak IBOutlet UITableView *BranchesTableView;
}
@property(assign)id<SearchResultsDelegate>delegate;
- (IBAction)BackButtonAction:(UIButton *)sender;
@property(nonatomic,retain)NSString* SearchedText;
@end
