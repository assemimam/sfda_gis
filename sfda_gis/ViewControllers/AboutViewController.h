//
//  AboutViewController.h
//  SFDA_GIS
//
//  Created by Assem Imam on 8/21/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"

@interface AboutViewController : MasterViewController
{
    __weak IBOutlet UITextView *AboutTextView;
    
    __weak IBOutlet UILabel *AddressLabel;
    __weak IBOutlet UILabel *WebsiteTitleLabel;
    __weak IBOutlet UILabel *PhoneTitleLabel;
    __weak IBOutlet UILabel *EmailTitleLabel;
    __weak IBOutlet UIButton *BackButton;
    __weak IBOutlet UILabel *ContctsLabel;
    __weak IBOutlet UILabel *AboutLabel;
    __weak IBOutlet UILabel *SfdaAppsLabel;
    __weak IBOutlet UILabel *SfdaAppLabel;
    __weak IBOutlet UIScrollView *ContainerScrollView;
    __weak IBOutlet UILabel *TitleLabel;
}
- (IBAction)SfdaLinkAction:(UIButton *)sender;
- (IBAction)CloseAction:(UIButton *)sender;
@end
