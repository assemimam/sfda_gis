//
//  RoutingViewController.h
//  SFDA_GIS
//
//  Created by assem on 9/2/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "DWBubbleMenuButton.h"

@interface RoutingViewController : UIViewController<GMSMapViewDelegate>
{
    __weak IBOutlet UIView *LoadingActivityControl;
    __weak IBOutlet UIView *LoadingView;
    __weak IBOutlet UILabel *LoadingLabel;
    __weak IBOutlet UIView *HeaderView;
    __weak IBOutlet UIView *MapViewContainer;
     GMSMapView *MapView;
    __weak IBOutlet UILabel *TitleLabel;
    __weak IBOutlet UIButton *BackButton;
    __weak IBOutlet UIButton *NavigatiorButton;
    __weak IBOutlet UIButton *OpenDropMenuButton;
    __weak IBOutlet UIView *ModalView;
    __weak IBOutlet UIView *ButtonsContainerView;
    __weak IBOutlet UIButton *OpenAppleButton;
    __weak IBOutlet UIButton *OpenGoogleButton;
    
}
@property(nonatomic,retain)id SelectedBranch;
- (IBAction)BackAction:(UIButton *)sender;
- (IBAction)AnimateDropMenu:(id)sender;
- (IBAction)GoToNavigatorAction:(UIButton *)sender;
-(IBAction)dismissPopUpMenuAction:(id)sender;

-(IBAction)openAppleMapButtonAction:(id)sender;
-(IBAction)openGoogleMapButtonAction:(id)sender;


@end
