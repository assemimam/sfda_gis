//
//  ViewController.h
//  SFDA_GIS
//
//  Created by Assem Imam on 8/5/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"
#import <MapKit/MapKit.h>
#import "OCMapView.h"

@interface ViewController : MasterViewController<MKMapViewDelegate,UITextFieldDelegate>
{
    
    __weak IBOutlet UIButton *SearchButton;
    __weak IBOutlet UIButton *FilterButton;
    __weak IBOutlet UIView *LoadingActivityControl;
    __weak IBOutlet UILabel *LoadingLabel;
    __weak IBOutlet UIView *LoadingView;
    __weak IBOutlet UIImageView *LogoImageView;
    __weak IBOutlet UITextField *SearchTextField;
    __weak IBOutlet UIButton *CancelSearchButton;
    __weak IBOutlet UIView *SearchHeaderView;
    __weak IBOutlet UIView *HeaderView;
    __weak IBOutlet OCMapView *MapView;
}
- (IBAction)MapButtonAction:(UIButton *)sender;
- (IBAction)CancelSearchAction:(UIButton *)sender;
- (IBAction)SearchAction:(UIButton *)sender;
- (IBAction)FilterButtonAction:(UIButton *)sender;
- (IBAction)MyLocationButtonAction:(UIButton *)sender;
@end
