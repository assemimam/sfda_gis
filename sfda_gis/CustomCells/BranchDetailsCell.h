//
//  BranchDetailsCell.h
//  SFDA_GIS
//
//  Created by Assem Imam on 8/26/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BranchDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *BranchTitleLabel;
@property (weak, nonatomic) IBOutlet UITextView *BranchSubTitleTextView;
@property (weak, nonatomic) IBOutlet UILabel *BranchSubTitleLabel;

@end
