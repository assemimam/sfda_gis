//
//  MenuCell.h
//  SFDA_GIS
//
//  Created by Assem Imam on 8/5/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *MenuOptionImageView;
@property (weak, nonatomic) IBOutlet UILabel *MenuOptionLabel;

@end
