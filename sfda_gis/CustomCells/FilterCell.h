//
//  FilterCell.h
//  SFDA_GIS
//
//  Created by Assem Imam on 8/21/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *FilterItemNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *FilterImageView;
@property (weak, nonatomic) IBOutlet UISwitch *FilterStateSwitchControl;

@end
