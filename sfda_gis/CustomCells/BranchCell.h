//
//  BranchCell.h
//  SFDA_GIS
//
//  Created by Assem Imam on 8/20/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BranchCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *BranchNameLabel;

@end
