//
//  RouteCell.h
//  sfda_gis
//
//  Created by Assem Imam on 9/8/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTLabel.h"

@interface RouteCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *DirectionStateImageView;
@property (weak, nonatomic) IBOutlet UILabel *RouteSuggestionLabel;
@property (weak, nonatomic) IBOutlet UILabel *DistanceLabel;

@end
