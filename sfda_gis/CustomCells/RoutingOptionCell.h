//
//  RoutingOptionCell.h
//  sfda_gis
//
//  Created by iOS Developer on 12/31/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoutingOptionCell : UITableViewCell

@property (nonatomic ,weak) IBOutlet UILabel *routingOptionLabel;

+(instancetype)setUpRoutingOptionCell;

@end
