//
//  SearchCell.h
//  sfda_gis
//
//  Created by Assem Imam on 9/17/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *BranchNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *BranchTypeLabel;

@end
