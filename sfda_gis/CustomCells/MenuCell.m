//
//  MenuCell.m
//  SFDA_GIS
//
//  Created by Assem Imam on 8/5/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
