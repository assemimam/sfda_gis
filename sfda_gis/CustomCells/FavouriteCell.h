//
//  FavouriteCell.h
//  sfda_gis
//
//  Created by Assem Imam on 9/18/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface FavouriteCell  : SWTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *BranchImageView;
@property (weak, nonatomic) IBOutlet UILabel *BranchNameLabel;

@end
