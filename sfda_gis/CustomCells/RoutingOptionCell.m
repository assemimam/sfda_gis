//
//  RoutingOptionCell.m
//  sfda_gis
//
//  Created by iOS Developer on 12/31/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "RoutingOptionCell.h"

@implementation RoutingOptionCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


+(instancetype)setUpRoutingOptionCell
{
    RoutingOptionCell *cell = [[NSBundle mainBundle]loadNibNamed:@"RoutingOptionCell"
                                                          owner:nil
                                                        options:nil][0];
    return cell;
    
}

@end
