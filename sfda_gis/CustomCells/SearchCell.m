//
//  SearchCell.m
//  sfda_gis
//
//  Created by Assem Imam on 9/17/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "SearchCell.h"

@implementation SearchCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
