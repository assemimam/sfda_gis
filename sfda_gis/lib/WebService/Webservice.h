//
//  Webservice.h
//  KFCA
//
//  Created by Assem Imam on 9/4/13.
//  Copyright (c) 2013 assem. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PRODUCTION_VERSION

#ifdef PRODUCTION_VERSION
#define BASE_URL @"http://sfdagisbeta.azurewebsites.net/api/"
#else
#define BASE_URL @"http://sfdagisbeta.azurewebsites.net/api/"
#endif

@interface Webservice : NSObject
{
    
}
+(id)GetBranches;
+(id)GetMainBranches;
+(id)GetBranchByID:(NSString*)branch_id;
@end
