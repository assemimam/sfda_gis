
//
//  Webservice.m
//  KFCA
//
//  Created by Assem Imam on 9/4/13.
//  Copyright (c) 2013 assem. All rights reserved.
//

#import "Webservice.h"


@implementation Webservice

+(id)GetBranches
{
    @try {
        id result;
        @try {
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@Branch", BASE_URL]];
            NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                            cachePolicy:NSURLCacheStorageAllowed
                                                        timeoutInterval:60];
            NSURLResponse * response = nil;
            NSError * error = nil;
            NSData * data = [NSURLConnection sendSynchronousRequest:request
                                                  returningResponse:&response
                                                              error:&error];
            if (error == nil)
            {
                result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
            }
            else{
                result=nil;
            }
        }
        @catch (NSException *exception) {
            
        }
        return result;
    }
    @catch (NSException *exception) {
        
    }
    
 
}
+(id)GetMainBranches
{
    @try {
        id result;
        @try {
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/MainBranch", BASE_URL]];
            NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                            cachePolicy:NSURLCacheStorageAllowed
                                                        timeoutInterval:60];
            NSURLResponse * response = nil;
            NSError * error = nil;
            NSData * data = [NSURLConnection sendSynchronousRequest:request
                                                  returningResponse:&response
                                                              error:&error];
            if (error == nil)
            {
                result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
            }
            else{
                result=nil;
            }
        }
        @catch (NSException *exception) {
            
        }
        return result;
    }
    @catch (NSException *exception) {
        
    }

}
+(id)GetBranchByID:(NSString*)branch_id
{
    @try {
        id result;
        @try {
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/Branch/%@", BASE_URL,branch_id]];
            NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                            cachePolicy:NSURLCacheStorageAllowed
                                                        timeoutInterval:60];
            NSURLResponse * response = nil;
            NSError * error = nil;
            NSData * data = [NSURLConnection sendSynchronousRequest:request
                                                  returningResponse:&response
                                                              error:&error];
            if (error == nil)
            {
                result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
            }
            else{
                result=nil;
            }
        }
        @catch (NSException *exception) {
            
        }
        return result;
    }
    @catch (NSException *exception) {
        
    }
}
@end
