//
//  CustomAnnotationView.h
//  SFDA_GIS
//
//  Created by Assem Imam on 8/12/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface CustomAnnotationView : MKAnnotationView<MKAnnotation>
{
      UILabel *label;
     
}
- (void) setClusterText:(NSString *)text;
- (int) getClusterText;
@property(nonatomic,retain)id DeatailsInformation;
@end
