//
//  CustomAnnotationView.m
//  SFDA_GIS
//
//  Created by Assem Imam on 8/12/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "CustomAnnotationView.h"

@implementation CustomAnnotationView
@synthesize DeatailsInformation;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)drawRect:(CGRect)rect
{

}

- (id) initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if ( self )
    {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            label = [[UILabel alloc] initWithFrame:CGRectMake(0, 3, 26, 26)];
            label.font = [UIFont boldSystemFontOfSize:15];
        }
        else{
            label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 26, 26)];
            label.font = [UIFont boldSystemFontOfSize:11];
        }
        
        [self addSubview:label];
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor clearColor];
        
        label.textAlignment = NSTextAlignmentCenter;
        label.shadowColor = [UIColor blackColor];
        label.shadowOffset = CGSizeMake(0,-1);
    }
    return self;
}

- (void) setClusterText:(NSString *)text
{
    label.text = text;
}
- (int) getClusterText
{
    return  [label.text intValue];
}

@end
