//
//  MKMapView+ZoomLevel.h
//  sfda_gis
//
//  Created by Assem Imam on 9/8/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (ZoomLevel)
- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel
                   animated:(BOOL)animated;
@end
