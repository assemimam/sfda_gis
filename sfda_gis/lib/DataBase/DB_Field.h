//
//  DB_Field.h
//  TeacherNote
//
//  Created by Tawasol on 9/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum {
    FIELD_DATA_TYPE_NUMBER = 0,
    FIELD_DATA_TYPE_TEXT = 1,
    FIELD_DATA_TYPE_DATE = 2,
    FIELD_DATA_TYPE_IMAGE=3,
    FIELD_DATA_TYPE_HTML_FILE=4,
    FIELD_DATA_TYPE_PDF_FILE=5,
}DB_FIELD_DATA_TYPE;
@interface DB_Field : NSObject
@property(nonatomic,retain)NSString*FieldName,*FieldValue,*UpdatedValue,*FieldAlias;
@property(nonatomic)  DB_FIELD_DATA_TYPE FieldDataType;
@property(nonatomic) BOOL IS_PRIMARY_KEY;
@end
