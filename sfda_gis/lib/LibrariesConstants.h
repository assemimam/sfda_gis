//
//  LibrariesConstants.h
//  MoRe
//
//  Created by Ahmed Aly on 10/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


#import "Language.h"
#import "AppDelegate.h"
#import "WebService.h"
#import "CommonMethods.h"
#import "ImageCache.h"
#import "Database.h"
#import "SlideNavigationController.h"

typedef enum
{
    FOODS_SECTOR=2,
    DRUGS_SECTOR=3,
    MEDICAL_SECTOR=4,
    SFDA_SECTOR=1,
}
SECTOR_TYPE;

typedef enum
{
    BRANCH=2,
    LABORATORY=3,
    OUTLET=4
}
BRANCH_TYPE;


#define SIDE_MENU_VIEW_TAG 1000000
// iOS 7 Support
#define IS_DEVICE_RUNNING_IOS_7_AND_ABOVE ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending)
// NetworkService
#define NETWORK_TIMEOUT 5
#define SET_BACKGROUND [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];\
#define SHOW_LOADING [ (AppDelegate*)[[UIApplication sharedApplication]delegate] ShowLoading:YES];\
#define HIDE_LOADING [ (AppDelegate*)[[UIApplication sharedApplication]delegate] ShowLoading:NO];\
#define SHOW_ALERT(alertText, okButton) {\
UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertText message:nil delegate:self cancelButtonTitle:okButton otherButtonTitles:nil];\
[alert show];\
}

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)