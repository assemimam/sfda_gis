//
//  RoutingOptionPopoverView.m
//  sfda_gis
//
//  Created by iOS Developer on 12/31/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "RoutingOptionPopoverView.h"
#import "RoutingOptionCell.h"
@interface RoutingOptionPopoverView ()
{
    NSMutableArray *tableViewDataSource;
}
@end

@implementation RoutingOptionPopoverView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    tableViewDataSource = [NSMutableArray arrayWithObjects:@"الإنتقال بواسطة خرائط آبل",@"الإنتقال بواسطة خرائط جوجل", nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    @try {
        return tableViewDataSource.count;
        
    }
    @catch (NSException *exception) {
        
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        
        static NSString *cellIdentifier = @"cell";
        RoutingOptionCell *cell = [tableView  dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell)
        {
            cell = [RoutingOptionCell setUpRoutingOptionCell];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.routingOptionLabel.text = tableViewDataSource[indexPath.row];
        return cell;
        
    }
    @catch (NSException *exception) {
        
    }
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        if ([self.delegate respondsToSelector:@selector(routingOptionSeletedIndex:)])
        {
            [self.delegate routingOptionSeletedIndex:indexPath.row];
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
}
@end
