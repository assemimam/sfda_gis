//
//  BranchDetailsView.m
//  SFDA_GIS
//
//  Created by Assem Imam on 8/26/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
#import "Webservice.h"
#import "BranchDetailsView.h"
#import "BranchDetailsCell.h"
#import "YLActivityIndicatorView.h"
#import "LibrariesHeader.h"
#import <Accounts/Accounts.h>

static NSString *const LABORATORIES = @"3";
static NSString *const OUTLETES = @"1";
static NSString *const BRANCHES = @"2";
@interface BranchDetailsView()
{
    YLActivityIndicatorView* LoadingIndicator;
    NSMutableArray *BranchItemsList;
    id BranchDetailsDic;
    BOOL finishLoading;
    UIPopoverController *sharingPickerPopover;
    BranchDetailsCell*currentBranchCell;
}
@end
@implementation BranchDetailsView
-(id)GetCashedBranchDetails
{
    NSMutableArray* result = [[NSMutableArray alloc]init];
    [[Cashing getObject] setDatabase:@"SFDA_GIS"];
    @try {
        DB_Field  *fldID = [[DB_Field alloc]init];
        fldID.FieldName=@"id";
        fldID.FieldAlias=@"Id";
        
        DB_Field  *fldNameAr = [[DB_Field alloc]init];
        fldNameAr.FieldName=@"NameAr";
        
        DB_Field  *fldNameEn = [[DB_Field alloc]init];
        fldNameEn.FieldName=@"NameEn";
        
        DB_Field  *fldSectionId = [[DB_Field alloc]init];
        fldSectionId.FieldName=@"SectionId";
        
        DB_Field  *fldMainBranchId = [[DB_Field alloc]init];
        fldMainBranchId.FieldName=@"MainBranchId";
        
        DB_Field  *fldBranchTypeId = [[DB_Field alloc]init];
        fldBranchTypeId.FieldName=@"BranchTypeId";
        
        DB_Field  *fldLatitude = [[DB_Field alloc]init];
        fldLatitude.FieldName=@"Latitude";
        
        DB_Field  *fldLongitude= [[DB_Field alloc]init];
        fldLongitude.FieldName=@"Longitude";
        
        DB_Field  *fldEmail = [[DB_Field alloc]init];
        fldEmail.FieldName=@"Email";
        
        DB_Field  *fldAddress = [[DB_Field alloc]init];
        fldAddress.FieldName=@"Address";
        
        DB_Field  *fldFax = [[DB_Field alloc]init];
        fldFax.FieldName=@"Fax";
        
        DB_Field  *fldFirstPhone = [[DB_Field alloc]init];
        fldFirstPhone.FieldName=@"FirstPhone";
        
        DB_Field  *fldSecondPhone = [[DB_Field alloc]init];
        fldSecondPhone.FieldName=@"SecondPhone";
        
        DB_Field  *fldHasFood = [[DB_Field alloc]init];
        fldHasFood.FieldName=@"HasFood";
        
        DB_Field  *fldHasDrug = [[DB_Field alloc]init];
        fldHasDrug.FieldName=@"HasDrug";
        
        DB_Field  *fldHasMedical = [[DB_Field alloc]init];
        fldHasMedical.FieldName=@"HasMedical";
        
        id BranchesList =[[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldID,fldNameAr,fldNameEn,fldAddress,fldFax,fldEmail,fldFirstPhone,fldSecondPhone,fldMainBranchId,fldSectionId,fldLatitude,fldLongitude,fldHasMedical,fldHasFood,fldHasDrug,fldBranchTypeId, nil] Tables:[NSArray arrayWithObject:@"branches"] Where:[NSString stringWithFormat:@"id=%i",[self.BranchID intValue]] FromIndex:nil ToIndex:nil OrderByField:nil AscendingOrder:YES];
        if (BranchesList) {
            if ([BranchesList count]>0) {
                for (id branch in BranchesList) {
                    NSMutableDictionary *branchDic = [branch mutableCopy];
                    
                    DB_Field  *fldDetailsId = [[DB_Field alloc]init];
                    fldDetailsId.FieldName=@"Id";
                    
                    DB_Field  *fldKey = [[DB_Field alloc]init];
                    fldKey.FieldName=@"Key";
                    
                    DB_Field  *fldValue = [[DB_Field alloc]init];
                    fldValue.FieldName=@"Value";
                    
                    DB_Field  *fldOrder = [[DB_Field alloc]init];
                    fldOrder.FieldName=@"key_order";
                    
                    DB_Field  *fldType = [[DB_Field alloc]init];
                    fldType.FieldName=@"Type";
                    
                    id BranchDetails =[[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldDetailsId,fldKey,fldValue,fldOrder,fldType, nil] Tables:[NSArray arrayWithObject:@"branch_details"] Where:[NSString stringWithFormat:@"branch_id=%i",[[branch objectForKey:@"Id"]intValue]]  FromIndex:nil ToIndex:nil OrderByField:nil AscendingOrder:YES];
                    if (BranchDetails) {
                        [branchDic setObject:BranchDetails forKey:@"BranchDetails"];
                    }
                    [result addObject:branchDic];
                }
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
    return result;
}

- (IBAction)ShareAction:(UIButton*)sender {
    @try {
        id sortedBranchDetails ;
        NSString *branchDetailsString=@"";
        branchDetailsString=[branchDetailsString stringByAppendingFormat:@"%@ \n",[NSNull null]!=[BranchDetailsDic objectForKey:@"NameAr"]?[BranchDetailsDic objectForKey:@"NameAr"]:@""] ;

        if ([BranchDetailsDic objectForKey:@"BranchDetails"]) {
            NSSortDescriptor *distanceDescriptor =
           
            [[NSSortDescriptor alloc] initWithKey:@"key_order"
                                        ascending:YES];
            NSArray *descriptors = [NSArray arrayWithObjects:distanceDescriptor, nil];
            sortedBranchDetails =[NSMutableArray arrayWithArray: [[BranchDetailsDic objectForKey:@"BranchDetails"] sortedArrayUsingDescriptors:descriptors]];
            for (id item in sortedBranchDetails) {
                branchDetailsString=[branchDetailsString stringByAppendingFormat:@"%@ : %@ \n",[item objectForKey:@"Key"],[item objectForKey:@"Value"]];
            }
        }

        NSURL *url = [NSURL URLWithString:@"http://www.sfda.gov.sa"];
        UIActivityViewController *controller =
        [[UIActivityViewController alloc]
        initWithActivityItems:@[branchDetailsString, url]
        applicationActivities:nil];
        controller.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                             UIActivityTypePrint,
                                             UIActivityTypeCopyToPasteboard,
                                             UIActivityTypeAssignToContact,
                                             UIActivityTypeSaveToCameraRoll,
                                             UIActivityTypeAddToReadingList,
                                             UIActivityTypePostToTencentWeibo,
                                             UIActivityTypeAirDrop];
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            sharingPickerPopover = [[UIPopoverController alloc] initWithContentViewController:controller];
            
            [sharingPickerPopover  presentPopoverFromRect:sender.frame inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
        else{
            [self.ContainerViewController presentViewController:controller animated:YES completion:nil];
        }

    }
    @catch (NSException *exception) {
        
    }

}
@synthesize BranchID,ContainerViewController;
- (IBAction)SaveToFavouriteButtonAction:(UIButton *)sender {
    @try {
        if (finishLoading==NO) {
            return;
        }
         [[Cashing getObject] setDatabase:@"SFDA_GIS"];
        if (sender.tag==1) {
            [[Cashing getObject]DeleteDataFromTable:@"favourite_branches" WhereCondition:[NSString stringWithFormat:@"id=%i",[[BranchDetailsDic objectForKey:@"Id"]intValue]]];
            Savelabel.textColor = [UIColor colorWithRed:244/255.0f green:147/255.0f blue:29/255.0f alpha:1];
            [sender setImage:[UIImage imageNamed:@"savebtn"] forState:UIControlStateNormal];
            [sender setTag:0];
        }
        else{
       
        DB_Field  *fldID = [[DB_Field alloc]init];
        fldID.FieldName=@"id";
        fldID.FieldDataType=FIELD_DATA_TYPE_NUMBER;
        fldID.IS_PRIMARY_KEY=YES;
        fldID.FieldValue=[NSString stringWithFormat:@"%i",[[BranchDetailsDic objectForKey:@"Id"]intValue]];
        
        DB_Field  *fldNameAr = [[DB_Field alloc]init];
        fldNameAr.FieldName=@"NameAr";
        fldNameAr.FieldDataType=FIELD_DATA_TYPE_TEXT;
        fldNameAr.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"NameAr"]?[BranchDetailsDic objectForKey:@"NameAr"]:@"";
        
        DB_Field  *fldNameEn = [[DB_Field alloc]init];
        fldNameEn.FieldName=@"NameEn";
        fldNameEn.FieldDataType=FIELD_DATA_TYPE_TEXT;
        fldNameEn.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"NameEn"]?[BranchDetailsDic objectForKey:@"NameEn"]:@"";
        
        
        DB_Field  *fldSectionId = [[DB_Field alloc]init];
        fldSectionId.FieldName=@"SectionId";
        fldSectionId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
        fldSectionId.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"SectionId"]?[NSString stringWithFormat:@"%i",[[BranchDetailsDic objectForKey:@"SectionId"]intValue]]:@"";
        
        DB_Field  *fldMainBranchId = [[DB_Field alloc]init];
        fldMainBranchId.FieldName=@"MainBranchId";
        fldMainBranchId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
        fldMainBranchId.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"MainBranchId"]?[NSString stringWithFormat:@"%i",[[BranchDetailsDic objectForKey:@"MainBranchId"]intValue]]:@"";
        
        DB_Field  *fldBranchTypeId = [[DB_Field alloc]init];
        fldBranchTypeId.FieldName=@"BranchTypeId";
        fldBranchTypeId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
        fldBranchTypeId.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"BranchTypeId"]?[BranchDetailsDic objectForKey:@"BranchTypeId"]:@"";
        
        DB_Field  *fldLatitude = [[DB_Field alloc]init];
        fldLatitude.FieldName=@"Latitude";
        fldLatitude.FieldDataType=FIELD_DATA_TYPE_TEXT;
        fldLatitude.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"Latitude"]?[NSString stringWithFormat:@"%f",[[BranchDetailsDic objectForKey:@"Latitude"]floatValue]]:@"";
        
        DB_Field  *fldLongitude= [[DB_Field alloc]init];
        fldLongitude.FieldName=@"Longitude";
        fldLongitude.FieldDataType=FIELD_DATA_TYPE_TEXT;
        fldLongitude.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"Longitude"]?[NSString stringWithFormat:@"%f",[[BranchDetailsDic objectForKey:@"Longitude"]floatValue]]:@"";
        
        DB_Field  *fldEmail = [[DB_Field alloc]init];
        fldEmail.FieldName=@"Email";
        fldEmail.FieldDataType=FIELD_DATA_TYPE_TEXT;
        fldEmail.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"Email"]?[BranchDetailsDic objectForKey:@"Email"]:@"";
        
        DB_Field  *fldAddress = [[DB_Field alloc]init];
        fldAddress.FieldName=@"Address";
        fldAddress.FieldDataType=FIELD_DATA_TYPE_TEXT;
        fldAddress.FieldValue=[NSNull null]!=[BranchDetailsDic objectForKey:@"Address"]?[BranchDetailsDic objectForKey:@"Address"]:@"";
        
       
        
            NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"MM/dd/yyyy hh:mm:ss"];
            [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            
            DB_Field  *fldRecentDate = [[DB_Field alloc]init];
            fldRecentDate.FieldName=@"RecentDate";
            fldRecentDate.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldRecentDate.FieldValue=[formatter stringFromDate:[NSDate date]];
            

            
        
        DB_Recored *addedRecored = [[DB_Recored alloc]init];
        addedRecored.Fields = [NSMutableArray  arrayWithObjects:fldID,fldNameAr,fldNameEn,fldSectionId,fldMainBranchId,fldBranchTypeId,fldLatitude,fldLongitude,fldEmail,fldAddress,fldRecentDate, nil];
        
        int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:addedRecored] inTable:@"favourite_branches"];
        Savelabel.textColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1];
        [sender setImage:[UIImage imageNamed:@"savebtnhover"] forState:UIControlStateNormal];
        [sender setTag:1];
        }
    }
    @catch (NSException *exception) {
        
    }
   
}

-(void)CashRecentBransheData:(id)BranchDetails
{
    @try {

        [[Cashing getObject] setDatabase:@"SFDA_GIS"];
        
        DB_Field  *fldID = [[DB_Field alloc]init];
        fldID.FieldName=@"id";
        fldID.FieldDataType=FIELD_DATA_TYPE_NUMBER;
        fldID.IS_PRIMARY_KEY=YES;
        fldID.FieldValue=[NSString stringWithFormat:@"%i",[[BranchDetails objectForKey:@"Id"]intValue]];
       
        DB_Field  *fldNameAr = [[DB_Field alloc]init];
        fldNameAr.FieldName=@"NameAr";
        fldNameAr.FieldDataType=FIELD_DATA_TYPE_TEXT;
        fldNameAr.FieldValue=[NSNull null]!=[BranchDetails objectForKey:@"NameAr"]?[BranchDetails objectForKey:@"NameAr"]:@"";
        
        DB_Field  *fldNameEn = [[DB_Field alloc]init];
        fldNameEn.FieldName=@"NameEn";
        fldNameEn.FieldDataType=FIELD_DATA_TYPE_TEXT;
        fldNameEn.FieldValue=[NSNull null]!=[BranchDetails objectForKey:@"NameEn"]?[BranchDetails objectForKey:@"NameEn"]:@"";
        
        DB_Field  *fldManagerId = [[DB_Field alloc]init];
        fldManagerId.FieldName=@"ManagerId";
        fldManagerId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
        fldManagerId.FieldValue=[NSNull null]!=[BranchDetails objectForKey:@"ManagerId"]?[BranchDetails objectForKey:@"ManagerId"]:@"";
        

        DB_Field  *fldSectionId = [[DB_Field alloc]init];
        fldSectionId.FieldName=@"SectionId";
        fldSectionId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
        fldSectionId.FieldValue=[NSNull null]!=[BranchDetails objectForKey:@"SectionId"]?[NSString stringWithFormat:@"%i",[[BranchDetails objectForKey:@"SectionId"]intValue]]:@"";
        
  
        
        DB_Field  *fldMainBranchId = [[DB_Field alloc]init];
        fldMainBranchId.FieldName=@"MainBranchId";
        fldMainBranchId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
        fldMainBranchId.FieldValue=[NSNull null]!=[BranchDetails objectForKey:@"MainBranchId"]?[NSString stringWithFormat:@"%i",[[BranchDetails objectForKey:@"MainBranchId"]intValue]]:@"";
        
    
        
        DB_Field  *fldBranchTypeId = [[DB_Field alloc]init];
        fldBranchTypeId.FieldName=@"BranchTypeId";
        fldBranchTypeId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
        fldBranchTypeId.FieldValue=[NSNull null]!=[BranchDetails objectForKey:@"BranchTypeId"]?[BranchDetails objectForKey:@"BranchTypeId"]:@"";
        
        
        DB_Field  *fldLatitude = [[DB_Field alloc]init];
        fldLatitude.FieldName=@"Latitude";
        fldLatitude.FieldDataType=FIELD_DATA_TYPE_TEXT;
        fldLatitude.FieldValue=[NSNull null]!=[BranchDetails objectForKey:@"Latitude"]?[NSString stringWithFormat:@"%f",[[BranchDetails objectForKey:@"Latitude"]floatValue]]:@"";
        
        DB_Field  *fldLongitude= [[DB_Field alloc]init];
        fldLongitude.FieldName=@"Longitude";
        fldLongitude.FieldDataType=FIELD_DATA_TYPE_TEXT;
        fldLongitude.FieldValue=[NSNull null]!=[BranchDetails objectForKey:@"Longitude"]?[NSString stringWithFormat:@"%f",[[BranchDetails objectForKey:@"Longitude"]floatValue]]:@"";
        
        DB_Field  *fldEmail = [[DB_Field alloc]init];
        fldEmail.FieldName=@"Email";
        fldEmail.FieldDataType=FIELD_DATA_TYPE_TEXT;
        fldEmail.FieldValue=[NSNull null]!=[BranchDetails objectForKey:@"Email"]?[BranchDetails objectForKey:@"Email"]:@"";
        
        DB_Field  *fldAddress = [[DB_Field alloc]init];
        fldAddress.FieldName=@"Address";
        fldAddress.FieldDataType=FIELD_DATA_TYPE_TEXT;
        fldAddress.FieldValue=[NSNull null]!=[BranchDetails objectForKey:@"Address"]?[BranchDetails objectForKey:@"Address"]:@"";
        
       
        NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"MM/dd/yyyy hh:mm:ss"];
        [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        
        DB_Field  *fldRecentDate = [[DB_Field alloc]init];
        fldRecentDate.FieldName=@"RecentDate";
        fldRecentDate.FieldDataType=FIELD_DATA_TYPE_TEXT;
        fldRecentDate.FieldValue=[formatter stringFromDate:[NSDate date]];

        
        DB_Recored *addedRecored = [[DB_Recored alloc]init];
        addedRecored.Fields = [NSMutableArray  arrayWithObjects:fldID,fldNameAr,fldNameEn,fldManagerId,fldMainBranchId,fldBranchTypeId,fldLatitude,fldLongitude,fldEmail,fldAddress,fldRecentDate, nil];
        
       int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:addedRecored] inTable:@"recent_branches"];
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)HandleGetBranchDetailsResult:(id)BranchDetails
{
    @try {

        [self CheckBranchInFavourite:[BranchDetails objectForKey:@"Id"]];
       
        BranchDetailsDic =BranchDetails;
        [self performSelectorInBackground:@selector(CashRecentBransheData:) withObject:BranchDetails];
        
        [BranchItemsList removeAllObjects];
         BranchNameLabel.text = [BranchDetails objectForKey:@"NameAr"];
        
        if ([[BranchDetails objectForKey:@"HasDrug"]intValue]==1) {
            [DrugImageView setHidden:NO];
        }
        else{
            [DrugImageView setHidden:YES];
        }
        
        if ([[BranchDetails objectForKey:@"HasFood"]intValue]==1) {
            [FoodImageView setHidden:NO];
        }
        else{
            [FoodImageView setHidden:YES];
        }
        
        if ([[BranchDetails objectForKey:@"HasMedical"]intValue]==1) {
            [MedicalImageView setHidden:NO];
        }
        else{
            [MedicalImageView setHidden:YES];
        }
        
        if ([[BranchDetails objectForKey:@"BranchTypeId"]intValue] == [BRANCHES intValue]) {
            BranchIconImageView.image=[UIImage imageNamed:@"filterstoreic.png"];
        }
        else if ([[BranchDetails objectForKey:@"BranchTypeId"]intValue] == [OUTLETES intValue])
        {
            BranchIconImageView.image=[UIImage imageNamed:@"filterbranchic.png"];
        }
        else if ([[BranchDetails objectForKey:@"BranchTypeId"]intValue] == [LABORATORIES intValue])
        {
            BranchIconImageView.image=[UIImage imageNamed:@"filterlabic.png"];
        }

        
        NSSortDescriptor *distanceDescriptor =
        [[NSSortDescriptor alloc] initWithKey:@"key_order"
                                    ascending:YES];
        
        NSArray *descriptors = [NSArray arrayWithObjects:distanceDescriptor, nil];
        id sortedBranchDetails =[NSMutableArray arrayWithArray: [[BranchDetails objectForKey:@"BranchDetails"] sortedArrayUsingDescriptors:descriptors]];


        for (id branchDetails in sortedBranchDetails) {
            
            NSMutableDictionary *branchDic=[[NSMutableDictionary alloc]init];

             NSString *emailExpression = @".*?@.*?(?>\\..{2,3})+";
             NSString *phoneExpression = @"[235689][0-9]{6}([0-9]{3})?";
             NSString *websiteExpression = @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
            
            NSString *subtitle =[NSNull null]!=[branchDetails objectForKey:@"Value"]?[branchDetails objectForKey:@"Value"]:@"غير معرف";
           
            NSRegularExpression *regexEmail = [NSRegularExpression regularExpressionWithPattern:emailExpression
                                                                                   options:NSRegularExpressionCaseInsensitive
                                                                                     error:nil];
            NSUInteger numberOfEmailMatches = [regexEmail numberOfMatchesInString:subtitle
                                                                options:0
                                                                  range:NSMakeRange(0, [subtitle length])];
            
            NSRegularExpression *regexPhone = [NSRegularExpression regularExpressionWithPattern:phoneExpression
                                                                                        options:NSRegularExpressionCaseInsensitive
                                                                                          error:nil];
            NSUInteger numberOfPhoneMatches = [regexPhone numberOfMatchesInString:subtitle
                                                                          options:0
                                                                            range:NSMakeRange(0, [subtitle length])];
           
            NSRegularExpression *regexWebSite = [NSRegularExpression regularExpressionWithPattern:websiteExpression
                                                                                        options:NSRegularExpressionCaseInsensitive
                                                                                          error:nil];
            NSUInteger numberOfWebSiteMatches = [regexWebSite numberOfMatchesInString:subtitle
                                                                          options:0
                                                                            range:NSMakeRange(0, [subtitle length])];
            
            if (numberOfEmailMatches>0 || numberOfPhoneMatches>0 || numberOfWebSiteMatches>0) {
                 [branchDic setValue:@"1" forKey:@"can_call"];
            }
            else{
                 [branchDic setValue:@"0" forKey:@"can_call"];
            }
            
            [branchDic setValue:[branchDetails objectForKey:@"Key"]?[branchDetails objectForKey:@"Key"]:@"غير معرف"  forKey:@"option"];
            [branchDic setValue:subtitle  forKey:@"option_subtitle"];
            [BranchItemsList addObject:branchDic];
        }
        
        [BranchDetailsTableView setHidden:NO];
        [LoadingView setHidden:YES];
        [LoadingIndicator stopAnimating];
        [BranchDetailsTableView reloadData];
        finishLoading =YES;
     
    }
    @catch (NSException *exception) {
        
    } 
}
-(void)CheckBranchInFavourite:(NSString*)branch_id
{
    @try {
        DB_Field  *fldID = [[DB_Field alloc]init];
        fldID.FieldName=@"id";
        
        id result = [[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldID, nil] Tables:[NSArray arrayWithObject:@"favourite_branches"] Where:[NSString stringWithFormat:@"id=%i",[branch_id intValue]] FromIndex:nil ToIndex:nil OrderByField:@"NameAr" AscendingOrder:YES];
        if (result) {
            if ( [result count ]>0) {
                Savelabel.textColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1];
                [SaveButton setImage:[UIImage imageNamed:@"savebtnhover"] forState:UIControlStateNormal];
                [SaveButton setTag:1];
            }
            else{
                [SaveButton setImage:[UIImage imageNamed:@"savebtn"] forState:UIControlStateNormal];
                Savelabel.textColor = [UIColor colorWithRed:244/255.0f green:147/255.0f blue:29/255.0f alpha:1];
                [SaveButton setTag:0];
            }
        }
        else{
            [SaveButton setImage:[UIImage imageNamed:@"savebtn"] forState:UIControlStateNormal];
            Savelabel.textColor = [UIColor colorWithRed:244/255.0f green:147/255.0f blue:29/255.0f alpha:1];
            [SaveButton setTag:0];
        }
    }
    @catch (NSException *exception) {
        
    }
}
-(void)GetBranchDetails
{
    @try {
        id result = [Webservice GetBranchByID:self.BranchID];
        [self performSelectorOnMainThread:@selector(HandleGetBranchDetailsResult:) withObject:result waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)ReloadBranchData
{
    @try {
        finishLoading =NO;
        LoadingLabel.text =[textConverter convertArabic:LoadingLabel.text];
        BranchNameLabel.text =[textConverter convertArabic:BranchNameLabel.text];
        Savelabel.text =[textConverter convertArabic:Savelabel.text];
        ShareLabel.text =[textConverter convertArabic:ShareLabel.text];
        DirectionsLabel.text =[textConverter convertArabic:DirectionsLabel.text];
        [BranchDetailsTableView setHidden:YES];
        [LoadingView setHidden:NO];
        [LoadingIndicator startAnimating];
        BranchNameLabel.text=@"";
        [FoodImageView setHidden:YES];
        [DrugImageView setHidden:YES];
        [MedicalImageView setHidden:YES];

          id result=  [self GetCashedBranchDetails];
          if (result) {
            if ([result count]>0) {
                    [self HandleGetBranchDetailsResult:[result objectAtIndex:0]];
            }
            else{
                   //handle no data result
                [BranchDetailsTableView setHidden:NO];
                [LoadingView setHidden:YES];
                [LoadingIndicator stopAnimating];
                [BranchDetailsTableView reloadData];
                finishLoading =YES;

            }
        }
       
    }
    @catch (NSException *exception) {
        
    }
    
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)awakeFromNib{
    @try {
        currentBranchCell= (BranchDetailsCell*)[[[NSBundle mainBundle]loadNibNamed:@"BranchDetailsCell" owner:nil options:nil]objectAtIndex:0];
        BranchItemsList = [[NSMutableArray alloc]init];
        DirectionsLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:DirectionsLabel.font.pointSize];
        ShareLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:ShareLabel.font.pointSize];
        Savelabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:Savelabel.font.pointSize];
        BranchNameLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:BranchNameLabel.font.pointSize];
        LoadingLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:LoadingLabel.font.pointSize];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            DrugImageView.image = [UIImage imageNamed:@"drugsicpopup~ipad@2x.png"];
            FoodImageView.image = [UIImage imageNamed:@"foodicpopup~ipad@2x.png"];
            MedicalImageView.image = [UIImage imageNamed:@"medicalicpopupic~ipad@2x.png"];
        }

        [BranchDetailsTableView setHidden:YES];
        [LoadingView setHidden:NO];
        LoadingIndicator = [[YLActivityIndicatorView alloc] initWithFrame:LoadingActivityIndicatorView.frame];
        LoadingIndicator.duration = .8f;
        [LoadingView addSubview:LoadingIndicator];
        
    }
    @catch (NSException *exception) {
        
    }
   
}
#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BranchDetailsCell *cell;
    @try {
        cell=(BranchDetailsCell*)[tableView dequeueReusableCellWithIdentifier:@"cleBranch"];
        if (cell==nil) {
            cell = (BranchDetailsCell*)[[[NSBundle mainBundle]loadNibNamed:@"BranchDetailsCell" owner:nil options:nil]objectAtIndex:0];
        }
        NSString* OptionTitle=[[BranchItemsList objectAtIndex:indexPath.row]objectForKey:@"option"];
        NSString* OptionSubTitle=[[BranchItemsList objectAtIndex:indexPath.row]objectForKey:@"option_subtitle"];
       
        cell.BranchTitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:cell.BranchTitleLabel.font.pointSize];
        cell.BranchTitleLabel.text = [textConverter convertArabic:OptionTitle];
        cell.BranchSubTitleTextView.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:cell.BranchSubTitleTextView.font.pointSize];
        cell.BranchSubTitleTextView.text =[textConverter convertArabic: OptionSubTitle];
        cell.BranchSubTitleLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:cell.BranchSubTitleLabel.font.pointSize];
        cell.BranchSubTitleLabel.text =[textConverter convertArabic: OptionSubTitle];
        if ([[[BranchItemsList objectAtIndex:indexPath.row]objectForKey:@"can_call"]intValue]==0 ) {
            [cell.BranchSubTitleTextView  setHidden:YES];
            [cell.BranchSubTitleLabel  setHidden:NO];
            
        }
        else
        {
             [cell.BranchSubTitleLabel  setHidden:YES];
             [cell.BranchSubTitleTextView  setHidden:NO];
             [cell.BranchSubTitleTextView setDataDetectorTypes:UIDataDetectorTypePhoneNumber|UIDataDetectorTypeLink|UIDataDetectorTypeAddress];
        }
        
        
    }
    @catch (NSException *exception) {
        
    }
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger noOfRows=0;
    
    @try {
        noOfRows = [BranchItemsList count];
    }
    @catch (NSException *exception) {
        
    }
    
    return noOfRows;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *Title= [textConverter convertArabic:[[BranchItemsList objectAtIndex:indexPath.row]objectForKey:@"option_subtitle"]];
    
    CGSize QuestionSize = [Title sizeWithFont:[UIFont fontWithName:@"GESSTwoBold-Bold" size:currentBranchCell.BranchSubTitleTextView.font.pointSize] constrainedToSize:CGSizeMake(tableView.frame.size.width , 2000000000000.0f)lineBreakMode:NSLineBreakByWordWrapping];
    
    return  MAX( QuestionSize.height,currentBranchCell.BranchTitleLabel.frame.size.height);
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
