//
//  NoInternetMessageView.h
//  sfda_gis
//
//  Created by Assem Imam on 10/9/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoInternetMessageView : UIView
{
    
    __weak IBOutlet UIImageView *InternetImageView;
    __weak IBOutlet UILabel *MessageLabel;
}
@property (weak, nonatomic) IBOutlet UIButton *HideButton;
@property (weak, nonatomic) IBOutlet UIView *ButtonsContainer;
@property (weak, nonatomic) IBOutlet UIButton *CancelButton;
@property (weak, nonatomic) IBOutlet UIButton *RetryButton;
@end
