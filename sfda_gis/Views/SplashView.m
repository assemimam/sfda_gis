//
//  SplashView.m
//  SFDA_GIS
//
//  Created by Assem Imam on 8/14/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "SplashView.h"

@implementation SplashView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)awakeFromNib{
    @try {
        self.StartApplicationLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:self.StartApplicationLabel.font.pointSize];
        self.StartApplicationLabel.text =[textConverter convertArabic:self.StartApplicationLabel.text];
    }
    @catch (NSException *exception) {
        
    }
   
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
