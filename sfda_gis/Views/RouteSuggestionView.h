//
//  RouteSuggestionView.h
//  sfda_gis
//
//  Created by Assem Imam on 9/8/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RouteSuggestionView : UIView<UITableViewDataSource,UITableViewDelegate>
{

    __weak IBOutlet UITableView *SuggestionsTableView;
    __weak IBOutlet UILabel *EndLabel;
    __weak IBOutlet UILabel *StartLabel;
    __weak IBOutlet UILabel *EndLocationLabel;
    __weak IBOutlet UILabel *StartLocationLabel;
   
    CGRect StartLocationFrame,EndLocationFrame;
}
@property (weak ,nonatomic) IBOutlet UILabel *TotalDistanceLabel;
@property (weak, nonatomic) IBOutlet UIButton *HideArrowButton;
@property (nonatomic,retain)  IBOutlet UIView *HeaderView;
@property (nonatomic,retain) NSString*StartLocation,*EndLocation;
@property (weak, nonatomic) IBOutlet UIButton *HideButton;
@property (weak, nonatomic) IBOutlet UIButton *ReverseDirectionsButton;
@property(nonatomic,retain) NSArray *SuggestionList;
-(void)RefreshData:(BOOL)reverse;
@end
