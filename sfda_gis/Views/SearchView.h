//
//  SearchView.h
//  sfda_gis
//
//  Created by assem on 9/16/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol SearchDelegate <NSObject>

@optional
-(void)didSelectRecentItem:(id)item;
-(void)didSelectNearByItem:(id)item;

@end

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "LibrariesHeader.h"

@interface SearchView : UIView<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    
    __weak IBOutlet UITableView *branchesTableView;
}
-(void)reloadData;
@property(nonatomic)CLLocation*currentLocation;
@property(assign)id<SearchDelegate>delegate;
@end
