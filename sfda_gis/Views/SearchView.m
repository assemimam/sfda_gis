//
//  SearchView.m
//  sfda_gis
//
//  Created by assem on 9/16/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//


#import "SearchView.h"
#import <MapKit/MapKit.h>
#import "SearchCell.h"
#import "TableHeaderView.h"

@interface SearchView()
{
    NSMutableArray *nearByBranchesList;
    NSMutableArray *lastVistedBranchesList;
    NSMutableArray *allLastVistedBranchesList;
    int lastAddedIndex;
    BOOL showMoreButton;
}
@end
@implementation SearchView
@synthesize delegate,currentLocation;
-(void)awakeFromNib{
       textConverter  = [[ArabicConverter alloc] init];
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(id)GetRecentBranches
{
    @try {
        
        [[Cashing getObject] setDatabase:@"SFDA_GIS"];
        
        DB_Field  *fldID = [[DB_Field alloc]init];
        fldID.FieldName=@"id";
        fldID.FieldAlias=@"Id";
        
        DB_Field  *fldNameAr = [[DB_Field alloc]init];
        fldNameAr.FieldName=@"NameAr";
        
        DB_Field  *fldNameEn = [[DB_Field alloc]init];
        fldNameEn.FieldName=@"NameEn";
        
        DB_Field  *fldManagerId = [[DB_Field alloc]init];
        fldManagerId.FieldName=@"ManagerId";
        
        DB_Field  *fldManagerName = [[DB_Field alloc]init];
        fldManagerName.FieldName=@"ManagerName";
        
        DB_Field  *fldSectionId = [[DB_Field alloc]init];
        fldSectionId.FieldName=@"SectionId";
        
        DB_Field  *fldSectionName = [[DB_Field alloc]init];
        fldSectionName.FieldName=@"SectionName";
        
        DB_Field  *fldMainBranchId = [[DB_Field alloc]init];
        fldMainBranchId.FieldName=@"MainBranchId";
        
        DB_Field  *fldMainBranchName = [[DB_Field alloc]init];
        fldMainBranchName.FieldName=@"MainBranchName";
        
        DB_Field  *fldBranchTypeId = [[DB_Field alloc]init];
        fldBranchTypeId.FieldName=@"BranchTypeId";
        
        DB_Field  *fldBranchTypeName = [[DB_Field alloc]init];
        fldBranchTypeName.FieldName=@"BranchTypeName";
        
        DB_Field  *fldLatitude = [[DB_Field alloc]init];
        fldLatitude.FieldName=@"Latitude";
        
        DB_Field  *fldLongitude= [[DB_Field alloc]init];
        fldLongitude.FieldName=@"Longitude";
        
        DB_Field  *fldEmail = [[DB_Field alloc]init];
        fldEmail.FieldName=@"Email";
        
        DB_Field  *fldAddress = [[DB_Field alloc]init];
        fldAddress.FieldName=@"Address";
        
        DB_Field  *fldFax = [[DB_Field alloc]init];
        fldFax.FieldName=@"Fax";
        
        DB_Field  *fldFirstPhone = [[DB_Field alloc]init];
        fldFirstPhone.FieldName=@"FirstPhone";
        
        DB_Field  *fldSecondPhone = [[DB_Field alloc]init];
        fldSecondPhone.FieldName=@"SecondPhone";
        
        NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"MM/dd/yyyy hh:mm:ss"];
        [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        
        DB_Field  *fldRecentDate = [[DB_Field alloc]init];
        fldRecentDate.FieldName=@"RecentDate";
        
        id result = [[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldID,fldNameAr,fldNameEn,fldManagerId,fldManagerName,fldSectionId,fldSectionName,fldMainBranchId,fldMainBranchName,fldBranchTypeId,fldBranchTypeName,fldLatitude,fldLongitude,fldEmail,fldAddress,fldFax,fldFirstPhone,fldSecondPhone,fldRecentDate, nil] Tables:[NSArray arrayWithObject:@"recent_branches"] Where:nil FromIndex:nil ToIndex:nil OrderByField:@"RecentDate" AscendingOrder:NO];
        
        
        return  result;
    
    }
    @catch (NSException *exception) {
        
    }
}
-(void)reloadData
{
    @try {
        showMoreButton=YES;
        lastAddedIndex=0;
        nearByBranchesList  = [[NSMutableArray alloc]init];
        lastVistedBranchesList  = [[NSMutableArray alloc]init];
        allLastVistedBranchesList  = [[NSMutableArray alloc]init];
        
        allLastVistedBranchesList =  [self GetRecentBranches];
        if ([allLastVistedBranchesList count]>3) {
            showMoreButton= YES;
            for (int i=lastAddedIndex ; i<3;i++) {
                [lastVistedBranchesList addObject:[allLastVistedBranchesList objectAtIndex:i]];
                 lastAddedIndex+=1;
            }
          
        }
        else{
            for (id branch in allLastVistedBranchesList) {
                [lastVistedBranchesList addObject:branch];
                lastAddedIndex+=1;
            }
          
        }
        NSMutableArray* UnsortedArrayOfDictionaries =[[NSMutableArray alloc]init];;
        for (id branch in BranchesList) {
            CLLocation *branchLocation = [[CLLocation alloc]initWithLatitude:[[branch objectForKey:@"Latitude"]doubleValue] longitude:[[branch objectForKey:@"Longitude"]doubleValue]];
            float distance = [self.currentLocation  distanceFromLocation:branchLocation];
            NSMutableDictionary *addedBranch = [[NSMutableDictionary alloc]init];
            [addedBranch setObject:branch forKey:@"branch"];
            [addedBranch setObject:[NSString stringWithFormat:@"%f",distance] forKey:@"distance"];
            [UnsortedArrayOfDictionaries addObject:addedBranch];
        }
        NSSortDescriptor *distanceDescriptor =
        [[NSSortDescriptor alloc] initWithKey:@"distance"
                                    ascending:YES];
        
        NSArray *descriptors = [NSArray arrayWithObjects:distanceDescriptor, nil];
        nearByBranchesList =[NSMutableArray arrayWithArray: [UnsortedArrayOfDictionaries sortedArrayUsingDescriptors:descriptors]];
        [branchesTableView reloadData];
        
    }
    @catch (NSException *exception) {
        
    }
 
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    id cell;
 
    @try {
        
        if (indexPath.section==0) {
            
            cell=(UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"cleBranch"];
            if (cell==nil) {
                cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cleBranch"];
            }
             ((UITableViewCell*)cell).textLabel.textAlignment = NSTextAlignmentRight;
             ((UITableViewCell*)cell).textLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:13];
             ((UITableViewCell*)cell).textLabel.numberOfLines = 0;
             ((UITableViewCell*)cell).textLabel.lineBreakMode = NSLineBreakByWordWrapping;
             ((UITableViewCell*)cell).backgroundColor = [UIColor clearColor];
            
            if (indexPath.row < [lastVistedBranchesList count]) {
                 ((UITableViewCell*)cell).textLabel.textColor = [UIColor blackColor];
                ((UITableViewCell*)cell).textLabel.text = [textConverter convertArabic:[[lastVistedBranchesList objectAtIndex:indexPath.row]objectForKey:@"NameAr"]];
                
            }
            else{
                ((UITableViewCell*)cell).textLabel.textColor = [UIColor redColor];
                ((UITableViewCell*)cell).textLabel.textAlignment = NSTextAlignmentCenter;
                ((UITableViewCell*)cell).textLabel.text =[textConverter convertArabic: @"المزيد"];
                
            }
        }
        else if (indexPath.section==1){
            
            cell = (SearchCell*)[[[NSBundle mainBundle]loadNibNamed:@"SearchCell" owner:nil options:nil]objectAtIndex:0];
            ((SearchCell*)cell).BranchNameLabel.text =[textConverter convertArabic:[[[nearByBranchesList objectAtIndex:indexPath.row]objectForKey:@"branch"]objectForKey:@"NameAr"]];
            ((SearchCell*)cell).backgroundColor = [UIColor clearColor];
            ((SearchCell*)cell).BranchNameLabel.font = [UIFont fontWithName:@"GESSTwoBold-Bold" size:((SearchCell*)cell).BranchNameLabel.font.pointSize];
            ((SearchCell*)cell).BranchTypeLabel.font = [UIFont fontWithName:@"GESSTwoBold-Bold" size:((SearchCell*)cell).BranchTypeLabel.font.pointSize];
           
          
            
            if ([[[[nearByBranchesList objectAtIndex:indexPath.row]objectForKey:@"branch"] valueForKey:@"BranchTypeId"]intValue] ==1) {
               
                ((SearchCell*)cell).BranchTypeLabel.text =[textConverter convertArabic:[[Language getObject]getStringWithKey:@"outlet"]];
            }
            else if ([[[[nearByBranchesList objectAtIndex:indexPath.row]objectForKey:@"branch"] valueForKey:@"BranchTypeId"]intValue] ==2){
               
                ((SearchCell*)cell).BranchTypeLabel.text =[textConverter convertArabic:[[Language getObject]getStringWithKey:@"branch"]];
            }
            else if ([[[[nearByBranchesList objectAtIndex:indexPath.row]objectForKey:@"branch"] valueForKey:@"BranchTypeId"]intValue] ==3){
               
                ((SearchCell*)cell).BranchTypeLabel.text =[textConverter convertArabic:[[Language getObject]getStringWithKey:@"laboratory"]];
            }

        }
        
    }
    @catch (NSException *exception) {
        
    }
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowCount=0;
    @try {
        if (section==0) {
            if ([allLastVistedBranchesList count]>3) {
                if (showMoreButton) {
                     rowCount= [lastVistedBranchesList count]+1;
                }
                else{
                     rowCount= [lastVistedBranchesList count];
                }
             
            }
            else{
                rowCount= [lastVistedBranchesList count];
            }
          
        }
        if (section==1) {
            return 10;
        }
    }
    @catch (NSException *exception) {
        
    }
    return rowCount;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
#pragma mark uitablevide delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 35;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        if (self.delegate==nil) {
            return;
        }
        
        if (indexPath.section==0) {
            if (indexPath.row <= [lastVistedBranchesList count]-1) {
               [self.delegate didSelectRecentItem:[lastVistedBranchesList objectAtIndex:indexPath.row]];
            }
            else{
                if ([allLastVistedBranchesList count]<=3) {
                    return;
                }
                if (lastAddedIndex<[allLastVistedBranchesList count]) {
                    if (lastAddedIndex+3<[allLastVistedBranchesList count]) {
                        for (int i=lastAddedIndex ; i<lastAddedIndex+3;i++) {
                            
                            [lastVistedBranchesList addObject:[allLastVistedBranchesList objectAtIndex:i]];
                        }
                        lastAddedIndex+=3;
                         showMoreButton =YES;
                    }
                    else{
                        for (int i=lastAddedIndex ; i<[allLastVistedBranchesList count];i++) {
                            [lastVistedBranchesList addObject:[allLastVistedBranchesList objectAtIndex:i]];
                        }
                        lastAddedIndex=[allLastVistedBranchesList count];
                        showMoreButton =NO;
                    }
                   
                }
                else{
                    
                }
                [branchesTableView reloadData];
            }
        }
        else if (indexPath.section==1){
            [self.delegate didSelectNearByItem:[[nearByBranchesList objectAtIndex:indexPath.row]objectForKey:@"branch"]];
        }
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    @catch (NSException *exception) {
        
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 35;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView;
    float headerWidth = tableView.frame.size.width;
    float padding = 10.0f;

    @try {
        UIImageView *headerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(headerWidth - 15, 10, 10, 15)];
        headerImageView.contentMode = UIViewContentModeScaleAspectFit;
        NSString *headerText ;
        if (section==0) {
            headerText=@"المسودات الأخيرة";
            headerImageView.image = [UIImage imageNamed:@"histroy.png"];
        }
        else  if (section==1) {
            headerText=@"الاقرب";
            headerImageView.image = [UIImage imageNamed:@"nearest.png"];
        }
            headerText =[textConverter convertArabic:headerText];
            headerView = [[UIView alloc] initWithFrame:CGRectMake(300, 0.0f, headerWidth, 35)];
            headerView.autoresizesSubviews = YES;
            headerView.backgroundColor = [UIColor colorWithRed:242/255.0f  green:242/255.0f blue:242/255.0f alpha:1];
            // create the label centered in the container, then set the appropriate autoresize mask
            UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,headerWidth, 35)];
            headerLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
            headerLabel.textAlignment = NSTextAlignmentRight;
            headerLabel.text = headerText;
            headerLabel.textColor = [UIColor blackColor];
            headerLabel.backgroundColor = [UIColor clearColor];
            headerLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
            CGSize textSize = [headerText sizeWithFont:headerLabel.font constrainedToSize:headerLabel.frame.size lineBreakMode:NSLineBreakByWordWrapping];
            headerLabel.frame = CGRectMake(headerWidth - (textSize.width + padding+15), headerLabel.frame.origin.y, textSize.width, headerView.frame.size.height);
            [headerView addSubview:headerLabel];
            [headerView addSubview:headerImageView];
        
    }
    @catch (NSException *exception) {
        
    }
    
    
    return headerView;
}

@end
