//
//  SplashView.h
//  SFDA_GIS
//
//  Created by Assem Imam on 8/14/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *SplashImageView;
@property (weak, nonatomic) IBOutlet UIButton *StartApplicationButton;
@property (weak, nonatomic) IBOutlet UILabel *StartApplicationLabel;

@end
