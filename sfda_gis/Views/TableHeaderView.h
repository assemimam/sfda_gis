//
//  TableHeaderView.h
//  SFDA_GIS
//
//  Created by Assem Imam on 8/20/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableHeaderView : UIView
@property (weak, nonatomic) IBOutlet UILabel *HeaderTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *RowsCountLabel;

@end
