//
//  FilterView.m
//  SFDA_GIS
//
//  Created by Assem Imam on 8/21/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "FilterView.h"
#import "LibrariesHeader.h"
#import "FilterCell.h"

#define OFFICES_SECTION 0
#define SECTORS_SECTION 1
#define BRANCHES 0
#define LABS 1
#define OUTLETS 2
#define FOOD_SECTOR 0
#define DRUGS_SECTOR 1
#define MEDICAL_SECTOR 2
@interface FilterView ()
{
    __weak IBOutlet UITableView *FilterTableView;
    NSArray *SfdaOficesList;
    NSArray *SfdaSectorsList;
    NSArray *MenuHeadersList;
    NSMutableArray*FilterOptions;
}
@end
@implementation FilterView
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
       
    }
    return self;
}
-(void)awakeFromNib{
    @try {
   
        NSMutableArray* options = [[[Language getObject]getArrayWithKey:@"FilterOptions"]mutableCopy];
        FilterOptions =[[NSMutableArray alloc]init];
        int section=0;
        for (id fiterOption in options) {
            NSMutableDictionary *ParentItem = [[NSMutableDictionary alloc]init];
            NSMutableArray *ChildItems = [[NSMutableArray alloc]init];
            [ParentItem setObject:[fiterOption objectForKey:@"title"] forKey:@"title"];
            for (id item in [fiterOption objectForKey:@"items"]) {
                NSMutableDictionary *ChildItem=[[NSMutableDictionary alloc]init];
                [ChildItem setObject:[item objectForKey:@"option"] forKey:@"option"];
                [ChildItem setObject:[item objectForKey:@"image"] forKey:@"image"];
                [ChildItem setObject:[item objectForKey:@"filter_key_value"] forKey:@"filter_key_value"];
                [ChildItem setObject:[item objectForKey:@"fiter_key_string"] forKey:@"fiter_key_string"];
                [ChildItem setObject:[item objectForKey:@"state"] forKey:@"state"];
                 [ChildItem setObject:[item objectForKey:@"section"] forKey:@"section"];
                [ChildItems addObject:ChildItem];
            }
            section++;
           [ParentItem setObject:ChildItems forKey:@"items"];
            [FilterOptions addObject:ParentItem];
        }
        
       
       
    }
    @catch (NSException *exception) {
        
    }
}
- (void)drawRect:(CGRect)rect
{
    @try {
       
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FilterCell *cell;
    @try {
        cell=(FilterCell*)[tableView dequeueReusableCellWithIdentifier:@"cleFilter"];
        if (cell==nil) {
            cell = (FilterCell*)[[[NSBundle mainBundle]loadNibNamed:@"FilterCell" owner:nil options:nil]objectAtIndex:0];
        }
        NSString*OptionName=@"";
        NSString*Imagename=@"";
        OptionName=[[[[FilterOptions objectAtIndex:indexPath.section]objectForKey:@"items"]objectAtIndex:indexPath.row]objectForKey:@"option"];
        Imagename =[[[[FilterOptions objectAtIndex:indexPath.section]objectForKey:@"items"]objectAtIndex:indexPath.row]objectForKey:@"image"];
        cell.FilterStateSwitchControl.accessibilityLabel=[NSString stringWithFormat:@"%i",indexPath.section];
        cell.FilterStateSwitchControl.tag = indexPath.row;
        BOOL ISON = [[[[[FilterOptions objectAtIndex:indexPath.section]objectForKey:@"items"]objectAtIndex:indexPath.row]objectForKey:@"state"] boolValue];
        [cell.FilterStateSwitchControl setOn:ISON animated:NO];
        [cell.FilterStateSwitchControl addTarget:self action:@selector(FilterStateSwitchControlAction:) forControlEvents:UIControlEventValueChanged];
        cell.FilterItemNameLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:12];
        cell.FilterItemNameLabel.text =[textConverter convertArabic: OptionName];
        cell.FilterImageView.image = [UIImage imageNamed:Imagename];
        
    }
    @catch (NSException *exception) {
        
    }
    return cell;
}
-(void)FilterStateSwitchControlAction:(UISwitch*)sender
{
    @try {
        NSPredicate*predicate;

        predicate =[NSPredicate predicateWithFormat:@"state == %@ ",@"1"];
        id  firstSectionOptions = [[[FilterOptions objectAtIndex:0] objectForKey:@"items"] filteredArrayUsingPredicate:predicate];
        if ([firstSectionOptions count ]==1) {
            if ([sender.accessibilityLabel intValue]==0) {
                  [sender setOn:YES animated:NO];
                
            }
        }

        id  seconedSectionOptions =[[[FilterOptions objectAtIndex:1] objectForKey:@"items"] filteredArrayUsingPredicate:predicate];
        if ([seconedSectionOptions count ]==1) {
            if ([sender.accessibilityLabel intValue]==1) {
                [sender  setOn:YES animated:NO];
                
            }
        }
        

        
        int Section = [sender.accessibilityLabel intValue];
        int CurrentRow = sender.tag;
        if (sender.isOn ) {
           [[[[FilterOptions objectAtIndex:Section]objectForKey:@"items"]objectAtIndex:CurrentRow] setObject:@"1" forKey:@"state"];
        }
        else{
             [[[[FilterOptions objectAtIndex:Section]objectForKey:@"items"]objectAtIndex:CurrentRow] setObject:@"0" forKey:@"state"];
        }
        NSMutableArray *CurrentChangedOptions = [[NSMutableArray alloc]init];
        for (id fiterOption in FilterOptions) {
            for (id item in [fiterOption objectForKey:@"items"]) {
                if ([[item objectForKey:@"state"]intValue]==1) {
                    [CurrentChangedOptions addObject:item];
                }
            }
        }
        [FilterTableView reloadData];
        if (self.delegate) {
            [self.delegate didChangeFilterOptions:CurrentChangedOptions];
        }
        
    }
    @catch (NSException *exception) {
        
    }
   
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger noOfRows=0;
    
    @try {
             noOfRows = [[[FilterOptions objectAtIndex:section]objectForKey:@"items"]count];
         }
    @catch (NSException *exception) {
        
    }
    
    return noOfRows;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [FilterOptions count];
}
#pragma -mark uitableview delegate methods
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 35;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView;
    @try {
        if(FilterOptions!=nil && [FilterOptions count] > 0){
            
            NSString *headerText =[textConverter convertArabic: [[FilterOptions objectAtIndex:section]objectForKey:@"title"]];
            float headerWidth = 320.0f;
            float padding = 10.0f;
            headerView = [[UIView alloc] initWithFrame:CGRectMake(300, 0.0f, headerWidth, 35)];
            headerView.autoresizesSubviews = YES;
            headerView.backgroundColor = [UIColor colorWithRed:242/255.0f  green:242/255.0f blue:242/255.0f alpha:1];
            // create the label centered in the container, then set the appropriate autoresize mask
            UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, headerWidth, 35)];
            headerLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
            headerLabel.textAlignment = NSTextAlignmentRight;
            headerLabel.text = headerText;
            headerLabel.textColor = [UIColor colorWithRed:176/255.0f  green:176/255.0f blue:176/255.0f alpha:1];
            headerLabel.backgroundColor = [UIColor clearColor];
            headerLabel.font=[UIFont fontWithName:@"GESSTwoBold-Bold" size:12];
            CGSize textSize = [headerText sizeWithFont:headerLabel.font constrainedToSize:headerLabel.frame.size lineBreakMode:NSLineBreakByWordWrapping];
            headerLabel.frame = CGRectMake(headerWidth - (textSize.width + padding), headerLabel.frame.origin.y, textSize.width, headerLabel.frame.size.height);
            [headerView addSubview:headerLabel];
            
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
    
    return headerView;
}
@end
