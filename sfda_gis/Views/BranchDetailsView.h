//
//  BranchDetailsView.h
//  SFDA_GIS
//
//  Created by Assem Imam on 8/26/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BranchDetailsView : UIView<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UIButton *SaveButton;
    __weak IBOutlet UILabel *LoadingLabel;
    __weak IBOutlet UIView *LoadingActivityIndicatorView;
    __weak IBOutlet UIView *LoadingView;
    __weak IBOutlet UIImageView *MedicalImageView;
    __weak IBOutlet UIImageView *DrugImageView;
    __weak IBOutlet UIImageView *FoodImageView;
    __weak IBOutlet UILabel *ShareLabel;
    __weak IBOutlet UILabel *Savelabel;
    __weak IBOutlet UILabel *DirectionsLabel;
    __weak IBOutlet UILabel *BranchNameLabel;
    __weak IBOutlet UIImageView *BranchIconImageView;
    __weak IBOutlet UITableView *BranchDetailsTableView;
}
-(void)ReloadBranchData;
@property(nonatomic,retain)UIViewController *ContainerViewController;
@property(nonatomic,retain) NSString*BranchID;
@property (weak, nonatomic) IBOutlet UIButton *ShareButton;
@property (weak, nonatomic) IBOutlet UIButton *DirectionsButton;
@property (weak, nonatomic) IBOutlet UIButton *ArrowButton;
@end
