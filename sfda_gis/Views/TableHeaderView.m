//
//  TableHeaderView.m
//  SFDA_GIS
//
//  Created by Assem Imam on 8/20/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "TableHeaderView.h"

@implementation TableHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
