//
//  RouteSuggestionView.m
//  sfda_gis
//
//  Created by Assem Imam on 9/8/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "RouteSuggestionView.h"
#import "RouteCell.h"
#import "LibrariesHeader.h"

@implementation RouteSuggestionView
@synthesize SuggestionList,StartLocation,EndLocation,TotalDistanceLabel;
-(void)awakeFromNib
{
    @try {
        StartLocationLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:StartLocationLabel.font.pointSize];
        EndLocationLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:EndLocationLabel.font.pointSize];
        StartLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:StartLabel.font.pointSize];
        EndLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:EndLabel.font.pointSize];
        EndLabel.text=[textConverter convertArabic: EndLabel.text];
        StartLocationLabel.text =[textConverter convertArabic:StartLocationLabel.text];
        StartLocationFrame =StartLocationLabel.frame  ;
        EndLocationFrame =EndLocationLabel.frame  ;
        
        //TotalDistanceLabel.font = [UIFont fontWithName:@"GESSTwoBold-Bold" size:StartLocationLabel.font.pointSize];
        TotalDistanceLabel.text = [textConverter convertArabic:TotalDistanceLabel.text];
        
    }
    @catch (NSException *exception) {
        
    }
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)RefreshData:(BOOL)reverse{
    @try {
        StartLocationLabel.text = StartLocation;
        EndLocationLabel.text = EndLocation;
        [SuggestionsTableView reloadData];
        [UIView animateWithDuration:0.3 animations:^{
            StartLocationLabel.frame = StartLocationFrame;
            EndLocationLabel.frame =EndLocationFrame ;
        }];
        if (reverse) {
            [UIView animateWithDuration:0.3 animations:^{
                StartLocationLabel.frame = CGRectMake(StartLocationLabel.frame.origin.x, EndLocationFrame.origin.y, StartLocationLabel.frame.size.width, StartLocationLabel.frame.size.height) ;
                EndLocationLabel.frame =CGRectMake(EndLocationLabel.frame.origin.x, StartLocationFrame.origin.y, EndLocationLabel.frame.size.width, EndLocationLabel.frame.size.height)  ;
            }];

        }
        
        
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [SuggestionList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RouteCell *cell =(RouteCell*)[tableView dequeueReusableCellWithIdentifier:@"cleRoute"];
    @try {
        if (cell==nil) {
            cell =(RouteCell*)[[[NSBundle mainBundle]loadNibNamed:@"RouteCell" owner:nil options:nil]objectAtIndex:0];
        }
        
       NSString *Suggestion= [CommonMethods GetHTML_Text:[[[SuggestionList objectAtIndex:indexPath.row] objectForKey:@"suggestion"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        cell.DistanceLabel.text =[[SuggestionList objectAtIndex:indexPath.row] objectForKey:@"distance"] ;
        cell.RouteSuggestionLabel.textAlignment= NSTextAlignmentRight;
        cell.RouteSuggestionLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:cell.RouteSuggestionLabel.font.pointSize];
        cell.RouteSuggestionLabel.text = [textConverter convertArabic: Suggestion];
        cell.DistanceLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:cell.DistanceLabel.font.pointSize];
        cell.RouteSuggestionLabel.textColor = [UIColor colorWithRed:109/255.0f green:109/255.0f  blue:109/255.0f  alpha:1];
        cell.DirectionStateImageView.image = [UIImage imageNamed:[[SuggestionList objectAtIndex:indexPath.row] objectForKey:@"maneuver"]];
        
    }
    @catch (NSException *exception) {
        
    }
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize QuestionSize;
  
    @try {
        NSString *Suggestion=[textConverter convertArabic: [CommonMethods GetHTML_Text:[[[SuggestionList objectAtIndex:indexPath.row] objectForKey:@"suggestion"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]]];
        //NSLog(@"title %@at index %i",Title,indexPath.row);
         if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
         QuestionSize = [Suggestion sizeWithFont:[UIFont fontWithName:@"GESSTwoBold-Bold" size:15] constrainedToSize:CGSizeMake(225 , 2000000000000.0f)];
         }
         else{
           QuestionSize = [Suggestion sizeWithFont:[UIFont fontWithName:@"GESSTwoBold-Bold" size:17] constrainedToSize:CGSizeMake(600 , 2000000000000.0f)];
         }
    NSLog(@"text  %@",Suggestion);
    }
    @catch (NSException *exception) {
        
    }
    NSLog(@"height %f",QuestionSize.height+10);
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        return  MAX( QuestionSize.height+10,40);
    }
    else{
        return  MAX( QuestionSize.height+10,60);
    }
    
    
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
