//
//  RoutingOptionPopoverView.h
//  sfda_gis
//
//  Created by iOS Developer on 12/31/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RoutingOptionPopoverView;

@protocol RoutingOptionDelegate <NSObject>

-(void)routingOptionSeletedIndex:(NSUInteger)index;

@end


@interface RoutingOptionPopoverView : UIViewController <UITableViewDataSource , UITableViewDelegate>

{
    __weak IBOutlet UITableView *routingOptionTableView;
}

@property (nonatomic ,weak) id<RoutingOptionDelegate>delegate;


@end
