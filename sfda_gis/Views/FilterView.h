//
//  FilterView.h
//  SFDA_GIS
//
//  Created by Assem Imam on 8/21/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
#import "LibrariesHeader.h"

@protocol FilterDelegate <NSObject>

@optional
-(void)didChangeFilterOptions:(id)options;
@end
#import <UIKit/UIKit.h>
@interface FilterView : UIView<UITableViewDelegate,UITableViewDataSource>
@property(assign)id<FilterDelegate>delegate;
@end
