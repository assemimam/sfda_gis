//
//  NoInternetMessageView.m
//  sfda_gis
//
//  Created by Assem Imam on 10/9/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "NoInternetMessageView.h"

@implementation NoInternetMessageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)awakeFromNib{
    @try {
        
      MessageLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:MessageLabel.font.pointSize];
      self.RetryButton.titleLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:self.RetryButton.titleLabel.font.pointSize];
      self.CancelButton.titleLabel.font =[UIFont fontWithName:@"GESSTwoBold-Bold" size:self.CancelButton.titleLabel.font.pointSize];
                          
    }
    @catch (NSException *exception) {
        
    }
  
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
