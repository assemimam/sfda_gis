//
//  AppDelegate.m
//  SFDA_GIS
//
//  Created by Assem Imam on 8/5/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "AppDelegate.h"
#import "SlideNavigationController.h"
#import "RightmenuViewController.h"
#import "ViewController.h"
#import "Language.h"
#import "SplashViewController.h"
#import "Reachability.h"
#import <GoogleMaps/GoogleMaps.h>
#define MAIN_BRANCHES_KEY @"main_branches"

@interface AppDelegate()
{
    
}
@end
@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    @try {
        
        textConverter = [[ArabicConverter alloc]init];
        [[SlideNavigationController sharedInstance] setNavigationBarHidden:YES];
        [GMSServices provideAPIKey:@"AIzaSyDkvSa02-bkM48Vsdw2XpemrKsUifaNvgg"];
        
        [[Language getObject]setLanguage:languageArabic];
        UIStoryboard *countryStoryboard;
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad"
                                                          bundle:nil];
        }
        else{
            countryStoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone"
                                                          bundle:nil];
        }
        
        ViewController *vc_main =[countryStoryboard  instantiateViewControllerWithIdentifier:@"ViewController"];
        SplashViewController *vc_splash =[countryStoryboard  instantiateViewControllerWithIdentifier:@"SplashViewController"];
        RightmenuViewController *vc_menu =[countryStoryboard  instantiateViewControllerWithIdentifier:@"RightmenuViewController"];
        
        if ([[NSUserDefaults standardUserDefaults]objectForKey:@"splash"]) {
            [SlideNavigationController sharedInstance].viewControllers = [NSArray  arrayWithObject:vc_main];
        }
        else{
            [SlideNavigationController sharedInstance].viewControllers = [NSArray  arrayWithObject:vc_splash];
        }
        
        [SlideNavigationController sharedInstance].rightMenu = vc_menu;
        
        [self.window makeKeyAndVisible];
    }
    @catch (NSException *exception) {
        
    }
    
    return YES;
}


- (BOOL)prefersStatusBarHidden
{
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Reachability

#pragma -mark uialertview delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    @try {
        if (buttonIndex==1) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs://"]];
            
        }
    }
    @catch (NSException *exception) {
        
    }
    
}

//-(BOOL)shouldAutorotate
//{
//    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
//    {
//        return YES;
//
//    }
//    else{
//        return YES;
//
//    }
//
//}

//-(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
//{
//    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
//    {
//        return UIInterfaceOrientationMaskLandscapeLeft;
//        
//    }
//}

@end
